/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
 var coinChange = function(coins, amount) {
    if(amount == 0) return 0;
    
    // let ansArray = new Array(amount+1);
    let curr = [amount];
    let count = 0;
    
    while(curr.length>0){
        console.log(curr)
        count++;
        
        let _curr = [];
        
        for(let currAmount of curr){
            for(let c of coins){
                if(currAmount-c>0){
                    _curr.push(currAmount-c);
                }else if(currAmount-c == 0){
                    return count;
                }
            }
        }
        curr = _curr;
         
    }
    return -1;
};

console.log(coinChange([1,2,5], 100))
