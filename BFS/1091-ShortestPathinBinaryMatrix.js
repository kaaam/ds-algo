/**
 * @param {number[][]} grid
 * @return {number}
 */

// var changeMark = function(x, y, newX, newY, grid, _curr){
//     if(grid[newX][newY] == 0){
//                     grid[newX][newY] = grid[x][y]+1;
//                     _curr.push([newX, newY]);
//                 }else if(grid[newX][newY]>1 && grid[newX][newY] > grid[x][y]+1){
//                     grid[newX][newY] = grid[x][y]+1;
//                     // _curr.push([x+1, y]);
//                 }
// }


// var shortestPathBinaryMatrix = function(grid) {
//     if(grid.length == 0) return -1;
//     if(grid[0][0] == 1) return -1;
//     if(grid[0][0] == 0 && grid.length==1) return 1;
    
//     let curr = [[0, 0]];
//     grid[0][0] = 1;
    
//     while(curr.length){
//         let _curr = [];
//         for(let [x, y] of curr){

//             if(grid[x+1]!=undefined && grid[x+1][y]!=undefined){
//                 changeMark(x, y, x+1, y, grid, _curr);
//             }

//             if(grid[x][y+1]!=undefined){
//                 changeMark(x, y, x, y+1, grid, _curr);
//             }
            
//             if(grid[x+1]!=undefined && grid[x+1][y+1]!=undefined){
//                 changeMark(x, y, x+1, y+1, grid, _curr);
//             }
            
            
            
//             if(grid[x-1]!=undefined && grid[x-1][y]!=undefined){
//                 changeMark(x, y, x-1, y, grid, _curr);
//             }

//             if(grid[x][y-1]!=undefined){
//                 changeMark(x, y, x, y-1, grid, _curr);
//             }
            
//             if(grid[x-1]!=undefined && grid[x-1][y+1]!=undefined){
//                 changeMark(x, y, x-1, y+1, grid, _curr);
//             }
            
//             if(grid[x-1]!=undefined && grid[x-1][y-1]!=undefined){
//                 changeMark(x, y, x-1, y-1, grid, _curr);
//             }
            
//             if(grid[x+1]!=undefined && grid[x+1][y-1]!=undefined){
//                 changeMark(x, y, x+1, y-1, grid, _curr);
//             }
            
            

//         }
//         // console.log(curr)
//         // console.log(grid)
//         // console.log('\n\n')
//         curr = _curr;

//     }
    
//     return grid[grid.length-1][grid.length-1]<=1?-1:grid[grid.length-1][grid.length-1];
    
// };




var recurse = function(grid, x, y, count){
    console.log(x, y)
    let t = [1, -1, 0];
    
    // if(grid[x]==undefined || grid[x][y] == undefined) return;
    // if(grid[x][y] == 1) return;
    // if(grid[x][y] <= count && grid[x][y]!=0) return;
    
    grid[x][y] = count;
    // console.log(x, y, grid[x][y])
    
    
    for(let s1 of t){
        for(let s2 of t){
            if(s1 == s2 && s1 == 0){ continue;}
            // if(grid[x+s1]!=undefined && grid[x+s1][y+s2]!=undefined){
            //     console.log(x+s1, y+s2, grid[x+s1][y+s2])
            // }
            if(grid[x+s1]!=undefined && grid[x+s1][y+s2]!=undefined && grid[x+s1][y+s2]!=1 && (grid[x+s1][y+s2] > count+1 || grid[x+s1][y+s2]==0)){
                recurse(grid, x+s1, y+s2, count+1);
            }
        }
    }
}


// DFS
var shortestPathBinaryMatrix = function(grid) {
    if(grid.length == 0) return -1;
    if(grid[0][0] == 1) return -1;
    if(grid[0][0] == 0 && grid.length==1) return 1;
    
    // grid[0][0] = 1;
    recurse(grid, 0, 0, 1);
    
    // console.log(grid)
    return grid[grid.length-1][grid.length-1]<=1?-1:grid[grid.length-1][grid.length-1];
    
    
    
}

var input = require('./1091-ShortestPathinBinaryMatrix.json')

console.log(shortestPathBinaryMatrix(input))

// console.log(shortestPathBinaryMatrix([[0,1],[1,0]]))



