/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
 var levelOrder = function(root) {
    
    let ans = [];
    let curr = [];
    if (root!=null) curr = [root];
    
    while(curr.length>0){
        let _next = [];
        let _curr = [];
        for(let v of curr){
            _curr.push(v.val);
            if(v.left!=null) _next.push(v.left);
            if(v.right!=null) _next.push(v.right);
        }
        ans.push(_curr);
        curr = _next;
    }
    
    return ans;
    
};