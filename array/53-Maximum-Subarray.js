// var maxSubArrayMine = function (nums) {


//     let maxSum = 0;
//     let maxLeft = -1;
//     let maxRight = -1;

//     let _sum = 0;
//     let _left = -1;
//     let _right = -1;

//     let maxValue = Number.NEGATIVE_INFINITY;

//     for (let i = 0; i < nums.length; i++) {
//         if (nums[i] > maxValue) maxValue = nums[i];

//         if ((_sum + nums[i] > _sum) || (_sum + nums[i] >= 0)) {
//             _sum = _sum + nums[i];
//             if (_left == -1) _left = i;
//             _right = i;
//         } else {
//             _sum = 0;
//             _left = -1;
//             _right = -1;
//         }

//         if (_sum > maxSum) {
//             maxLeft = _left;
//             maxRight = _right;
//             maxSum = _sum;
//         }

//         // console.log(_sum, maxSum, "_sum, maxSum     ---at ", nums[i])
//         // console.log(_left, _right, " _left, _right ")
//         // console.log(maxLeft, maxRight, " maxLeft, maxRight ")
//         // console.log('\n')
//     }

//     // console.log(maxSum, maxLeft, maxRight, " maxSum, left, right")
//     if (maxLeft == maxRight && maxRight == -1) {
//         return maxValue;
//     }
//     return maxSum;

// };

// // result = maxSubArray([-10, 1, 2, -1, 10]);
// // result = maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);
// // result = maxSubArray([-10, -1, -20]);
// // result = maxSubArray([-10, -1, 1, -20, 1000]);
// // result = maxSubArrayMine([10, 1, -2, 5]);
// // console.log(result, " result ")



// function maxSubArray(A) {
//     var prev = 0;
//     var max = -Number.MAX_VALUE;

//     for (var i = 0; i < A.length; i++) {
//       prev = Math.max(prev + A[i], A[i]);
//       max = Math.max(max, prev);
//       console.log(prev, max, " prev, max")
//     }
//     return max;
//   }

//   result = maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);
//   console.log(result, " result ")





// // *************************************************** Trying without looking after coupe of days
// function maxSubArray(nums) {
//     let maxSum = Number.MIN_SAFE_INTEGER;
//     let biggestNumber = Number.MIN_SAFE_INTEGER;

//     let currSum = 0;
//     for(let _num of nums){
//         if(currSum<0) currSum=0;
//         currSum = currSum + _num;
//         if(currSum > maxSum) maxSum = currSum;
//     }
//     return maxSum;
// }

// // result = maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);
// result = maxSubArray([-2]);
// console.log(result, " result ")






// // *************************************************** DP
// function maxSubArray(nums) {
//     let dpArray = []
//     for(let i=0;i<nums.length;i++){
//         dpArray.push(new Array(nums.length).fill(0));
//     }

//     let maxSum = Number.MIN_SAFE_INTEGER;

//     for(let i=0;i<nums.length;i++){
//         for(let j=i;j<nums.length;j++){
//             if(i == j){
//                 dpArray[i][j] = nums[i];
//             }else{
//                 dpArray[i][j] = dpArray[i][j-1] + nums[j];
//             }
//             if(dpArray[i][j] > maxSum) maxSum = dpArray[i][j];
//         }
//     }
//     return maxSum;
// }

// result = maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);
// // result = maxSubArray([-2]);
// console.log(result, " result ")









// ***************************************************optimising 2d array with dict
function maxSubArray(nums) {
    let dpDict = {};
    for (let i = 0; i < nums.length; i++) {
        for (let j = i; j < nums.length; j++) {
            if (i in dpDict) {
                dpDict[i][j] = 0;
            } else {
                dpDict[i] = { [j]: 0 };
            }
        }
    }

    let maxSum = Number.MIN_SAFE_INTEGER;

    for (let i = 0; i < nums.length; i++) {
        for (let j = i; j < nums.length; j++) {
            if (i == j) {
                dpDict[i][j] = nums[i];
            } else {
                dpDict[i][j] = dpDict[i][j - 1] + nums[j];
            }
            if (dpDict[i][j] > maxSum) maxSum = dpDict[i][j];
        }
    }
    return maxSum;
}

result = maxSubArray([-2, 1, -3, 4, -1, 2, 1, -5, 4]);
// result = maxSubArray([-2]);
console.log(result, " result ")