
var recursionBruteForce = function (nums, i) {

    console.log(" at ", i, "     with value ", (i <= (nums.length - 1)) ? nums[i] : "beyond array")
    if (i >= nums.length - 1) { return true; }
    if (nums[i] == 0) { return false; }

    // let allJumps = [];

    for (let step = 1; step <= nums[i]; step++) {
        // allJumps.push(recursion(nums, i+step));
        let isEndReached = recursionBruteForce(nums, i + step);
        if (isEndReached) {
            return true;
        }
    }


    // for(let jump of allJumps){
    //     if(jump == true){
    //         return true;
    //     }
    // }

    return false;
};

var canJumpBruteForce = function (nums) {
    return recursionBruteForce(nums, 0);
};


// result = canJump([2,3,1,1,4]);
// result = canJumpBruteForce([2,0,1,1,4]);



var canJump = function (nums) {

    if(nums.length == 1) return true;

    let target = 1;
    for(let i=nums.length-2;i>=0;i--){
        if(nums[i]>=target && target!=1){
            target = 1;
        }
        if(nums[i]<target){
            target = target + 1;
        }
    }
    if(target>1) return false
    return true
};

// result = canJump([5,0,0,0,0,2,0,4]);
// result = canJump([1,0]);
result = canJump([10, 1, 2, 2, 0, 0, 0]);
console.log(result, "result")