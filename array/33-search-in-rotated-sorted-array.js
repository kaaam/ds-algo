// https://leetcode.com/problems/search-in-rotated-sorted-array/


var bs_algo = function (nums, start, end, target) {
    while (start <= end) {
        let _mid = (start + end) >> 1;

        if (nums[_mid] == target) return _mid;
        else if (nums[_mid] < target) start = _mid + 1;
        else end = _mid - 1;
    }
    return -1;
}


var bs_special_algo = function (nums, start, end, target) {

    // return {start, end} if either result found or we ended with plain sorted subbarrau

    let _mid = (start + end) >> 1;
    while (1) {

        _mid = (start + end) >> 1;
        
        if(start > end){
            return { start, end };
        }

        // console.log(nums[start] <= nums[_mid] && nums[_mid] <= nums[end], "---", start, _mid, end, '------', nums[start], nums[_mid], nums[end])
        if(nums[start] <= nums[_mid] && nums[_mid] <= nums[end]){
            return { start, end };
        }


        // console.log(" IN SPECIAL ALGO")
        // console.log(start, _mid, end, " start, _mid, end ")
        // console.log(nums[start], nums[_mid], nums[end], " nums[start], nums[_mid], nums[end] ")
        // console.log('\n')

        if (nums[_mid] == target) return { start, end };
        else if (target > nums[_mid]) {
            if (nums[_mid] == Math.min(nums[start], nums[_mid], nums[end])) {
                // if (target <= nums[end]) {
                //     start = _mid + 1;
                //     return { start, end };
                // } else {
                    end = _mid - 1;
                // }
            } else {
                start = _mid + 1;
            }
        } else {
            if (nums[_mid] == Math.min(nums[start], nums[_mid], nums[end])) {
                end = _mid - 1;
            } else {
                // if (target >= nums[start]) {
                //     end = _mid - 1;
                //     return { start, end };
                // } else {
                    start = _mid + 1;
                // }
            }

        }
    }
    return { start, end };
}


function main(nums, target) {
    // console.log('\n', nums.join(", "), '\n')
    // console.log('\n', target, '\n')
    let start = 0; let end = nums.length - 1;

    ({ start, end } = bs_special_algo(nums, start, end, target));
    // console.log(start, end, " start, end ")
    // console.log(nums[start], nums[end], " nums[start], nums[end] ")
    let _mid = (start + end) >> 1;
    if (nums[_mid] == target) return _mid;
    return bs_algo(nums, start, end, target);
}


// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 25) == -1);
// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 20) != -1);
// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 19) != -1);
// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 12) != -1);
// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 0) != -1);
// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 1) != -1);
// console.log(main([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20], 3) != -1);
// (function () {
//     let result = main([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0], 0); 
//     console.log(result, result != -1);
//   })();
  


// console.log(main([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0], 1) != -1);
// console.log(main([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0], 5) != -1);
// console.log(main([1], 5) == -1);
// console.log(main([5], 5) != -1);
// console.log(main([5, 1], 5) != -1);
// console.log(main([5, 1], 1) != -1);
// console.log(main([12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 7) != -1);

// (function () {
//     let result = main([17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 18); 
//     console.log(result, result != -1);
//   })();
console.log(main([1], 0) == -1);