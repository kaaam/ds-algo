/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */

 var swap = function(nums, i, j){
    let temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
    return nums;
}


var nextPermutation = function(nums) {
    
    if(nums.length ==1) return nums;
    
    let firstDescIndex;
    let toSwapIndex;
    let ans = [];
    
    for(let i=nums.length-2;i>=0;i--){
        if(nums[i]<nums[i+1]){
            firstDescIndex = i;
            break;
        }
    }
    
    if(firstDescIndex == undefined){
        for(let i=0;i<nums.length>>1;i++){
            swap(nums, i, nums.length-1-i);
        }
        return;

    }
    
    for(let i=nums.length-1;i>=0;i--){
        if(nums[i]>nums[firstDescIndex]){
            toSwapIndex = i;
            break;
        }
    }
    
    // console.log(nums)
    
    // let temp = nums[toSwapIndex];
    // nums[toSwapIndex] = nums[firstDescIndex];
    // nums[firstDescIndex] = temp;
    swap(nums, toSwapIndex, firstDescIndex);
    // console.log(nums, toSwapIndex, firstDescIndex)
    
    // for(let i=0;i<=firstDescIndex;i++){
    //     ans.push(nums[i]);
    // }

    // console.log(firstDescIndex+1, firstDescIndex+((nums.length-1-firstDescIndex)>>1), " ---- ")
    for(let i=firstDescIndex+1;i<=(firstDescIndex+((nums.length-1-firstDescIndex)>>1));i++){
        // console.log(i, nums.length-i+firstDescIndex)
        swap(nums, i, nums.length-i+firstDescIndex);
    }
    
    
    
    
};