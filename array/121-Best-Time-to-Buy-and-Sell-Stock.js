
var maxProfit = function (prices) {
    let maxProfit = 0;
    let lastBuy = prices[0];

    for (let i = 1; i < prices.length; i++) {
        if (prices[i] < lastBuy) lastBuy = prices[i];
        if (prices[i] - lastBuy > maxProfit) maxProfit = prices[i] - lastBuy;
    }
    return maxProfit;
};

result = maxProfit([7, 6, 5, 4, 3, 10, 2, 9, 1, 5]);
// result = maxProfit([1, 1, 1, 1]);
console.log(result, " result ")