let debugg = 0;


function backtrack(elements, target, currentSum, result, currentElementsList, currentIndex) {

    console.log(' AFTER CALL ')
    console.log(currentSum, "currentSum");
    // console.log(result, "result");
    console.log(result, "result2");
    console.log(currentElementsList, "currentElementsList");
    console.log(currentIndex, "currentIndex");
    console.log(debugg, " debugg ")
    console.log('\n\n');

    debugg = debugg + 1;
    if(debugg>100) return ;


    if (currentSum == target) {
        // console.log(" PUSHING ", currentElementsList)
        result.push(currentElementsList);
    } else if (currentSum > target) {
    } else {
        for (let i = currentIndex; i < elements.length; i++) {

            let newCurrentElementsList = [...currentElementsList, elements[i]];

            console.log(' BEFORE CALL ', elements[i])
            console.log(currentSum+elements[i], "currentSum");
            // console.log(result, "result");
            console.log(result, "result2");
            console.log(newCurrentElementsList, "currentElementsList");
            console.log(i, "currentIndex");
            console.log(debugg, " debugg ")
            console.log('\n\n');

            backtrack(elements, target, currentSum + elements[i], result, newCurrentElementsList, i+1);
            newCurrentElementsList = currentElementsList;


            // ******************* explanantion of this is at the end of the file *******************
            while( elements[i+1] == elements[i]){
                console.log(currentElementsList, "currentElementsList    ", elements, "    ", i, " out of ", elements.length-1);
                if(i == elements.length-1){
                    break;
                }
                i = i + 1;
            }

        }
    }
}


function combinationSum(elements, target) {
    // elements = new Set(elements);
    // elements = [...elements];
    elements.sort((a, b) => a - b);
    // console.log(elements, "elements elements")
    let result = [];

    if((elements.length && elements[0] > target) || elements.length == 0) return [];
    backtrack(elements, target, 0, result, [], 0);
    // console.log(" FINAL ", result)
    return result;

}


// // 2, 3, 5
// result = combinationSum([5, 3, 2, 5, 3, 2, 3], 5)
// console.log(result, "result")


// result = combinationSum([], 5)
// console.log(result, "result")

// result = combinationSum([15], 5)
// console.log(result, "result")

// result = combinationSum([2], 1)
// console.log(result, "result")

// result = combinationSum([2,3,5,6,7], 7)
// console.log(result, "result")

// result = combinationSum([7, 7, 7,7], 7)
// console.log(result, "result")


// combinationSumBruteForce([7, 3, 6, 2,3,6,7], 7)


// // [10,1,2,7,6,1,5]
// result = combinationSum([10,1,2,7,6,1,5], 8)
// console.log(result, "result")


// [1, 1, 1, 1, 1, 2, 2, 2, 5 ]
result = combinationSum([2,5,2,1,2, 1, 1, 1, 1], 3)
console.log(result, "result")






// ************************* PARTIAL Explanation of while condition at the end of for loop *************************
// let's take the example of [1, 1, 1, 1, 1, 2, 2, 2, 5]
// 
// ******* first iteration
// 1(1st 1)
// 
// 1(1st 1) 1(2nd 1)
// 
// 1(1st 1) 1(2nd 1) 1(3rd 1)
// 
// 1(1st 1) 1(2nd 1) 1(3rd 1) 1(4th 1)   ------------------|
// 1 again can't come after this                           |
//                                                         |
// 1(1st 1) 1(2nd 1) 1(3rd 1) 1(4th 1) 1(5th 1)    ---|    |
// while loop NOT hit, 2 reached                   ---|    |
//                                                         |
// 1(1st 1) 1(2nd 1) 1(3rd 1) 1(4th 1) 2                   |
// while loop hit, 2 reached ------------------------------|
// 
// 
// 
// 
// after every step of recusion, same number can't come again. That's why while loop is put after recursion call
// 
// also notice how there is no [1(1st one) 1(3rd one)] or [1(1st one) 1(4th one)], All ones can come only once and in a sequnce.
// If [1(1st one) 1(3rd one)] would have come, it would have created duplicates
// so after 1(1st one), no other 3rd, 4th, 5th 1 can come without 2nd 1.
// 
// 
// ************** same is true at next level of iteration with multiple 2s