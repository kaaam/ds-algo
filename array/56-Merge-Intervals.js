
var mergeMine = function (intervals) {
    intervals.sort((a, b) => {
        let diff = a[0] - b[0];
        if (diff == 0) {
            return b[1] - a[1];
        }
        return diff;
    });
    let answer = [intervals[0]];
    // console.log(intervals)
    let index = 0;

    if (intervals.length == 0 || intervals.length == 1) return answer;

    for (let i = 1; i < intervals.length; i++) {
        if (intervals[i][0] > answer[index][0] && intervals[i][0] <= answer[index][1]) {
            if (intervals[i][1] > answer[index][1]) {
                let x = answer[index][0];
                answer.pop();
                answer.push([x, intervals[i][1]])
                // index = index + 1;
            } else {
                // pass
            }
        } else if (intervals[i][0] == answer[index][0]) {
            //   pass  
        } else {
            answer.push(intervals[i])
            index = index + 1;
        }
    }
    // console.log(answer, " answer ")
    return answer

    // while(index <= answer.length){
    //     if(intervals[index][0] > intervals[0][0] && intervals[index][0] < intervals[0][1]){
    //         if(intervals[index][1] > intervals[index][1]){
    //             // answer.push()
    //         }else{
    //             // pass
    //         }
    //     }
    // }
};

// result = merge([[1, 3], [15, 18], [2, 6], [8, 10]]);
// result = merge([[1, 3], [15, 18], [2, 6], [8, 10]]);
// result = merge([[1, 4], [4, 5]]);
// result = mergeMine([[1,4],[1,5]]);
// result = merge([
//     [1, 20],
//     [1, 8],
//     [2, 8],
//     [3, 10],
//     [4, 5],
//     [2, 20]
// ]);
// result = merge([[1, 3]]);
// console.log(result, " result ")



function merge(intervals) {
    if (!intervals.length) return intervals
    intervals.sort((a, b) => a[0] !== b[0] ? a[0] - b[0] : a[1] - b[1])
    console.log(intervals, " intervals intervals ")
    var prev = intervals[0]
    var res = [prev]
    for (var curr of intervals) {
        if (curr[0] <= prev[1]) {
            prev[1] = Math.max(prev[1], curr[1])
        } else {
            res.push(curr)
            prev = curr
        }
    }
    return res
}

// result = merge([[1, 3], [15, 18], [2, 6], [8, 10]]);
// result = merge([[1, 3], [15, 18], [2, 6], [8, 10]]);
// result = merge([[1, 4], [4, 5]]);
// result = mergeMine([[1,4],[1,5]]);
result = merge([
    [4, 5],
    [1, 8],
    [1, 20],
    [2, 8],
    [3, 10],
    [2, 20]
]);
// result = merge([[1, 3]]);
// console.log(result, " result ")