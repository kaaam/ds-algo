// https://leetcode.com/problems/permutations/

var permuteRecursive = function (nums, freezeIndex, result) {
    // if(index == nums.length-1) result.push
    // result.push([...nums]);
    if (nums[0] == 1) {
        // console.log(nums, freezeIndex, result, "nums, freezeIndex, result")
    }

    for (let i = freezeIndex + 1; i < nums.length; i++) {
        let newNums = [...nums];
        let temp = newNums[freezeIndex + 1];
        newNums[freezeIndex + 1] = newNums[i];
        newNums[i] = temp;
        if (i > freezeIndex + 1) { result.push([...newNums]); }
        permuteRecursive(newNums, freezeIndex + 1, result)
    }
}

var permute = function (nums) {
    if (nums.length == 1) return [nums];

    let result = [nums];
    permuteRecursive(nums, -1, result);
    return result;
};

result = permute([1, 2, 3, 4, 5]);
// result = permute([1, 2, 3]);
// result = permute([1]);
console.log(result, "result", result.length)