let debugg = 0;


function backtrack(elements, target, currentSum, result, currentElementsList, currentIndex) {

    console.log(' AFTER CALL ')
    console.log(currentSum, "currentSum");
    // console.log(result, "result");
    console.log(result, "result2");
    console.log(currentElementsList, "currentElementsList");
    console.log(currentIndex, "currentIndex");
    console.log(debugg, " debugg ")
    console.log('\n\n');

    debugg = debugg + 1;
    if(debugg>50) return ;


    if (currentSum == target) {
        // console.log(" PUSHING ", currentElementsList)
        result.push(currentElementsList);
    } else if (currentSum > target) {
    } else {
        for (let i = currentIndex; i < elements.length; i++) {
            
            // optimization
            if(elements[i] > target) return;

            let newCurrentElementsList = [...currentElementsList, elements[i]];

            console.log(' BEFORE CALL ', elements[i])
            console.log(currentSum+elements[i], "currentSum");
            // console.log(result, "result");
            console.log(result, "result2");
            console.log(newCurrentElementsList, "currentElementsList");
            console.log(i, "currentIndex");
            console.log(debugg, " debugg ")
            console.log('\n\n');

            backtrack(elements, target, currentSum + elements[i], result, newCurrentElementsList, i);
            newCurrentElementsList = currentElementsList;
        }
    }
}


function combinationSum(elements, target) {
    elements = new Set(elements);
    elements = [...elements];
    elements.sort((a, b) => a - b);
    // console.log(elements, "elements elements")
    let result = [];

    if((elements.length && elements[0] > target) || elements.length == 0) return [];
    backtrack(elements, target, 0, result, [], 0);
    // console.log(" FINAL ", result)
    return result;

}


// 2, 3, 5
// result = combinationSum([5, 3, 2, 5, 3, 2, 3], 5)
// console.log(result, "result")


// result = combinationSum([], 5)
// console.log(result, "result")

// result = combinationSum([15], 5)
// console.log(result, "result")

// result = combinationSum([2], 1)
// console.log(result, "result")

// result = combinationSum([2,3,6,7], 7)
// console.log(result, "result")

// result = combinationSum([7, 7, 7,7], 7)
// console.log(result, "result")


result = combinationSum([1, 10, 11, 12, 13, 14, 15], 2)
console.log(result, "result")




// **************************************** TIME COMPLEXITY ****************************************

                                                
//                                                                             (2, 3, 5)
//                         /                                                         |                                           \
// 
//                     2                                                           3
//             /                   \                                       /               \
//         2, 2                     2, 3                                3,3                3, 5
//     /                   /                    \
// 2, 2, 2              2, 3, 3                2, 3, 5



// ***************** DP ******************* (image attached)
