

function recursive(sortedList, target, start = 0, end = sortedList.length - 1) {

    let mid = (start + end) >> 1;
    // console.log(sortedList, "sortedList")
    // console.log(start, sortedList[start])
    // console.log(mid, sortedList[mid])
    // console.log(end, sortedList[end])
    // console.log('\n')
    if(start > end) return -1;
    if (sortedList[mid] == target) return mid;
    else if (sortedList[mid] < target) return recursive(sortedList, target, mid + 1, end);
    else return recursive(sortedList, target, start, mid - 1);

}


function iterative(sortedList, target) {
    let start = 0; let end = sortedList.length - 1;
    // let mid = Math.floor((start + end) / 2);

    while (end >= start) {
        let mid = (start + end) >> 1;
        console.log(start, mid, end)
        // console.log(sortedList[start], sortedList[mid], sortedList[end])

        if (sortedList[mid] == target) return mid;
        else if (sortedList[mid] < target) start = mid + 1;
        else end = mid - 1;

    }
    return -1;
    

}

function main(sortedList, target) {
    // return iterative(sortedList, target);
    return recursive(sortedList, target);

}

let result;
// result = main([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], 8);
// result = main([1, 2, 3, 4, 5, 6, 7, 8, 16, 17, 18, 19, 20, 21, 25, 26, 27, 29, 30], 9);
// result = main([1], 9);
// console.log(result, result == -1, " result result")
// console.log('\n\n');

// result = main([1], 1);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2], 9);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2], 1);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2], 2);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3], 9);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3], 1);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3], 2);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3], 3);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 4], 9);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 4], 1);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 4], 2);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 4], 3);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 4], 4);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 5], 9);
// console.log(result, " result result")
// console.log('\n\n');

// result = main([1, 2, 3, 6], 9);
// console.log(result, " result result")
// console.log('\n\n');




result = main([-1,0,3,5,9,12], 2);
console.log(result, " result result")
console.log('\n\n');