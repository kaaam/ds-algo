

var recurse = function (grid, visitedGrid, i, j){
    if(grid[i] && grid[i][j] == "1" && visitedGrid[i] && visitedGrid[i][j] == "0"){
        visitedGrid[i][j] = "1";
        recurse(grid, visitedGrid, i+1, j);
        recurse(grid, visitedGrid, i-1, j);
        recurse(grid, visitedGrid, i, j+1);
        recurse(grid, visitedGrid, i, j-1);
    }else{
        return;
    }
}


var numIslands = function (grid) {
    let totalInslands = 0;

    let visitedGrid = JSON.parse(JSON.stringify(grid));

    for (let i = 0; i < visitedGrid.length; i++) {
        for (let j = 0; j < visitedGrid[0].length; j++) {
            visitedGrid[i][j] = visitedGrid[i][j] == "1" ? "0" : "1";
        }
    }

    // console.log(grid, "grid grid")
    // console.log(visitedGrid, "visitedGrid visitedGrid")

    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[0].length; j++) {

            // console.log(grid[i][j], visitedGrid[i][j])
            if(grid[i][j] == "1" && visitedGrid[i][j] == "0"){
                // console.log(" INSIDE ")
                recurse(grid, visitedGrid, i, j);
                totalInslands = totalInslands + 1;
            }
        }
    }

    // console.log(visitedGrid, "visitedGrid visitedGrid")
    // console.log(totalInslands, " totalInslands totalInslands")
    return totalInslands;
};

// result = numIslands([
//     ["1", "1", "1", "1", "0"],
//     ["1", "1", "0", "1", "0"],
//     ["1", "1", "0", "0", "0"],
//     ["0", "0", "0", "0", "0"]
// ]);

// result = numIslands([
//     ["1","1","0","0","0"],
//     ["1","1","0","0","0"],
//     ["0","0","1","0","0"],
//     ["0","0","0","1","1"]
// ]);


// result = numIslands([
//     ["1"]
// ]);

// result = numIslands([
//     ["0"]
// ]);


// result = numIslands([
//     ["0","0","0","0","0"],
//     ["0","0","0","0","0"],
//     ["0","0","0","0","0"],
//     ["0","0","0","0","0"]
// ]);


// result = numIslands([
//     ["1","0","1","0","1"],
//     ["0","1","0","1","0"],
//     ["1","0","1","0","1"],
//     ["0","1","0","1","0"]
// ]);


// result = numIslands([
//     ["0","0","0","0","0"],
//     ["0","0","0","0","0"],
//     ["0","0","0","0","0"],
//     ["0","0","0","0","1"]
// ]);

console.log(result, ' result ')