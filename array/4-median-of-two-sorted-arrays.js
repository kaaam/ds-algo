https://leetcode.com/problems/median-of-two-sorted-arrays/
// https://leetcode.com/problems/median-of-two-sorted-arrays/discuss/278326/javascript-100

var answer;


var findMedianSortedArrays = function(nums1, nums2) {
    if(nums1.length > nums2.length) return findMedianSortedArrays(nums2, nums1)
    let x = nums1.length
    let y = nums2.length
    let lowX = 0, highX = x
    while(lowX <= highX) {
        console.log(lowX, highX, "lowX, highX")
        const partitionXIndex = (highX + lowX) >> 1
        const partitionYIndex = ((x + y + 1) >> 1) - partitionXIndex
        console.log(partitionXIndex, partitionYIndex, " partitionXIndex, partitionYIndex ")
        
        const maxX = partitionXIndex == 0 ? Number.NEGATIVE_INFINITY : nums1[partitionXIndex - 1]
        const maxY = partitionYIndex == 0 ? Number.NEGATIVE_INFINITY : nums2[partitionYIndex - 1]
        
        const minX = partitionXIndex == nums1.length ? Number.POSITIVE_INFINITY : nums1[partitionXIndex]
        const minY = partitionYIndex == nums2.length ? Number.POSITIVE_INFINITY : nums2[partitionYIndex ]

        console.log(maxX, minX, "maxX, minX")
        console.log(maxY, minY, "maxY, minY")
        
        if(maxX <= minY && maxY <= minX) {
            const lowMax = Math.max(maxX, maxY)
            if( (x + y) % 2 == 1)
                return lowMax
            return (lowMax + Math.min(minX, minY)) / 2
        } else if(maxX < minY) {
            lowX = partitionXIndex + 1
        } else 
            highX = partitionXIndex - 1
        console.log("\n")
    }
    
};

// Number.NEGATIVE_INFINITY
// Number.POSITIVE_INFINITY
var findMedianSortedArraysTriedWithoutLooking = function (nums1, nums2) {
    if (nums1.length > nums2.length) return findMedianSortedArraysTriedWithoutLooking(nums2, nums1);

    let firstLength = nums1.length;
    let secondLength = nums2.length;

    let firstLeftLimit = 0;
    let firstRightLimit = firstLength;

    while (firstLeftLimit <= firstRightLimit) {
        // console.log(firstRightLimit, firstLeftLimit, " firstRightLimit, firstLeftLimit ")

        // calculate Index
        let firstCurrentIndex = (firstRightLimit + firstLeftLimit) >> 1;
        let firstSecondIndex = ((firstLength + secondLength + 1) >> 1) - firstCurrentIndex;
        // console.log(firstCurrentIndex, firstSecondIndex, "   firstCurrentIndex, firstSecondIndex   ")

        // calculate Values
        let firstLeftValue = nums1[firstCurrentIndex - 1] != undefined ? nums1[firstCurrentIndex - 1] : Number.NEGATIVE_INFINITY;
        let firstRightValue = nums1[firstCurrentIndex] != undefined ? nums1[firstCurrentIndex] : Number.POSITIVE_INFINITY;

        let secondLeftValue = nums2[firstSecondIndex - 1] != undefined ? nums2[firstSecondIndex - 1] : Number.NEGATIVE_INFINITY;
        let secondRightValue = nums2[firstSecondIndex] != undefined ? nums2[firstSecondIndex] : Number.POSITIVE_INFINITY;

        // console.log(firstLeftValue, firstRightValue, secondLeftValue, secondRightValue, "firstLeftValue, firstRightValue, secondLeftValue, secondRightValue")


        if (firstLeftValue <= secondRightValue && firstRightValue >= secondLeftValue) {
            console.log('YO', Math.max(firstLeftValue, secondLeftValue), Math.min(firstRightValue, secondRightValue))
            if ((firstLength + secondLength) % 2 == 1) return Math.max(firstLeftValue, secondLeftValue);
            return (Math.max(firstLeftValue, secondLeftValue) + Math.min(firstRightValue, secondRightValue)) / 2;
        } else if (firstLeftValue > secondRightValue) {
            firstRightLimit = firstCurrentIndex - 1;
        } else {
            firstLeftLimit = firstCurrentIndex + 1;
        }
        // console.log('\n\n')

    }

};
// *************** example 1 ********************
// 1, (2), 3                    -> 1 (0, 3)
// 4, 5, 6, 7, (8), 9           -> 4
// 1, 2, 3, 4, (5), 6, 7, 8, 9

// 1, 2, (3)                    -> 2 (2, 3)
// 4, 5, (6), 7, 8, 9           -> 2
// 1, 2, 3, 4, (5), 6, 7, 8, 9

// 1, 2, 3 ()                    -> 3 (2, 2)
// 4, (5), 6, 7, 8, 9            -> 1
// 1, 2, 3, 4, (5), 6, 7, 8, 9
// *************** example 1 ********************



// *************** example 2 ********************
// 14, 15, 50, 70, (90), 100, 110, 120                                                    -> 4 (0, 8)
// 1, 2, 3, 4, 12, 13, (16), 60, 80, 130, 140, 150                                        -> 6 (0, 12)
// 1, 2, 3, 4, 12, 13, 14, 15, 16, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150

// 14, (15), 50, 70, 90, 100, 110, 120                                                    -> 1 (0, 3)
// 1, 2, 3, 4, 12, 13, 16, 60, 80, (130), 140, 150                                        -> 9 (9, 12)
// 1, 2, 3, 4, 12, 13, 14, 15, 16, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150

// 14, 15, (50), 70, 90, 100, 110, 120                                                    -> 2 (2, 3)
// 1, 2, 3, 4, 12, 13, 16, 60, (80), 130, 140, 150                                        -> 8 (8, 9)
// 1, 2, 3, 4, 12, 13, 14, 15, 16, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150

// 14, 15, 50, (70), 90, 100, 110, 120                                                    -> 3 (3, 3)
// 1, 2, 3, 4, 12, 13, 16, (60), 80, 130, 140, 150                                        -> 7 (8, 8)
// 1, 2, 3, 4, 12, 13, 14, 15, 16, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150
// *************** example 2 ********************



// *************** example 3 ********************
// (1), 3                                     -> 0 (0, 1)
// 2,__, ()                                    -> 2 (0, 0)
// 1, 2, 3

// 1, (3)                                     -> 0 (1, 1)
// 2, (),__                                    -> 1 (0, 0)
// 1, 2, 3
// *************** example 3 ********************




// findMedianSortedArrays(
//     [5, 6, 7, 8, 9, 20, 21],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );

// findMedianSortedArrays(
//     [5],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );

// findMedianSortedArrays(
//     [500],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );

// findMedianSortedArrays(
//     [500, 600, 700],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );

// findMedianSortedArraysTriedWithoutLooking(
//     [500, 600, 700],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );

// findMedianSortedArrays(
//     [500, 600, 700, 800],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );


// 4+9 -> 13 total elememts
// 1, 2, 3, 4, 12, 13, (14), 15, 16, 500, 600, 700, 800
answer = findMedianSortedArraysTriedWithoutLooking(
    [500, 600, 700, 800],
    [1, 2, 3, 4, 12, 13, 14, 15, 16]
);


// answer = findMedianSortedArrays(
//     [500, 600, 700, 800],
//     [1, 2, 3, 4, 12, 13, 14, 15, 16]
// );

// answer = findMedianSortedArrays(
//     // [14, 15, 16, 50, 90, 100, 110, 120],
//     [14, 15, 50, 70, 90, 100, 110, 120],
//     [1, 2, 3, 4, 12, 13, 16, 60, 80, 130, 140, 150]
// );

// answer = findMedianSortedArrays(
//     // [14, 15, 16, 50, 90, 100, 110, 120],
//     [1, 2, 3],
//     [4, 5, 6, 7, 8]
// );

// answer = findMedianSortedArrays(
//     [],
//     [4, 5, 6, 7, 8]
// );


// answer = findMedianSortedArraysTriedWithoutLooking(
//     // [14, 15, 16, 50, 90, 100, 110, 120],
//     [14, 15, 50, 70, 90, 100, 110, 120],
//     [1, 2, 3, 4, 12, 13, 16, 60, 80, 130, 140, 150]
// );

// answer = findMedianSortedArraysTriedWithoutLooking(
//     [1, 3],
//     [2]
// );

// answer = findMedianSortedArrays(
//     [1, 3],
//     [2]
// );


// answer = findMedianSortedArraysTriedWithoutLooking(
//     [0, 0],
//     [0, 0]
// );

// answer = findMedianSortedArrays(
//     [0, 0],
//     [0, 0]
// );

console.log(answer, " answer answer ")