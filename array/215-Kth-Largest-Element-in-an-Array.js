/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */


var recurse = function (nums, k, leftLimit, rightLimit) {
    // console.log('\n\n')
    // console.log(leftLimit, rightLimit, " leftLimit, rightLimit ", nums[k], k, nums)
    if (rightLimit < leftLimit) return -1;
    let pivotIndex = (leftLimit + rightLimit) >> 1;
    let pivot = nums[pivotIndex];
    let leftOfPivot = [];
    let rightOfPivot = [];

    for (let i = leftLimit; i <= rightLimit; i++) {
        // console.log(nums[i], "++++nums[i]")
        if (i != pivotIndex) {
            nums[i] <= pivot ? leftOfPivot.push(nums[i]) : rightOfPivot.push(nums[i]);
        }

    }


    let numsPart = [...leftOfPivot, pivot, ...rightOfPivot];
    // console.log(leftOfPivot, " leftOfPivot ");
    // console.log(rightOfPivot, " rightOfPivot ");
    // console.log(numsPart, "  numsPart  ")
    // console.log(pivot, ' pivot ')
    for (let i = 0; i < numsPart.length; i++) {
        nums[leftLimit + i] = numsPart[i];
    }

    let pivotNewIndex = leftLimit + leftOfPivot.length;

    // console.log(nums, " before call ")
    // console.log(pivotNewIndex, " indexOfPivot ")
    // console.log(nums.length-k,  pivotNewIndex, "  -----   ")
    // console.log('\n\n')
    if (nums.length - k == pivotNewIndex) {
        // return recurse(nums, k, indexOfPivot, indexOfPivot);
        return nums[pivotNewIndex];
    } else if (nums.length - k < pivotNewIndex) {
        return recurse(nums, k, leftLimit, pivotNewIndex - 1);
    } else {
        return recurse(nums, k, pivotNewIndex + 1, rightLimit);
    }

    // if (k <= rightOfPivot.length) return recurse(nums, k, leftLimit + leftOfPivot.length + 1, rightLimit);
    // else if (k == leftOfPivot.length + 1) return recurse(nums, k, leftLimit + leftOfPivot.length + 1, leftLimit + leftOfPivot.length + 1);
    // else return recurse(nums, k, leftLimit, leftOfPivot.length - 1);

}

var findKthLargest = function (nums, k) {
    // console.log(nums)
    return recurse(nums, k, 0, nums.length - 1);

};

console.log(findKthLargest([3, 2, 3, 1, 2, 4, 5, 5, 6], 4))
// console.log(findKthLargest([3, 2, 3, 1, 2, 4, 5, 5, 6], 9))


// 3, 2, 3, 1, 2, 4, 5, 5, 6
// 1, 2, 2, 3, 3, 4, 5, 5, 6
// 0, 1, 2, 3, 4, 5, 6, 7, 8



// 0 1 2 3
// 4 3 2 1


var swap = function (nums, index1, index2) {
    let temp = nums[index1];
    nums[index1] = nums[index2];
    nums[index2] = temp;
    return nums;

}


var sortKelemntsIterative = function (nums, k) {
    let left = 0;
    let right = nums.length - 1;

    let currentPivotIndex;
    let targetIndex = nums.length - k;

    //  && (nums.length-k)!=
    while (left < right) {
        console.log(left, right, " left, right ")
        let i = left;
        let j = right;
        currentPivotIndex = (left + right) >> 1;
        let pivot = nums[currentPivotIndex];
        console.log(nums, "   nums ")
        console.log(pivot, "   pivot ")
        console.log(currentPivotIndex, " currentPivotIndex ")

        swap(nums, currentPivotIndex, right);
        j--;

        while (i < j) {
            if (nums[i] <= pivot && nums[j] > pivot) {
                i++; j--;
            } else if (nums[i] <= pivot) {
                i++;
            } else if (nums[j] > pivot) {
                j--;
            } else {
                console.log(nums, " nums before swap", i, j)
                swap(nums, i, j);
            }
        }

        console.log(i, j, " i, j ")
        if(i == j){
            if(nums[i]>pivot){
                currentPivotIndex = i;
                swap(nums, i, right);
            }else{
                currentPivotIndex = i+1;
                swap(nums, i+1, right);
            }
        }else{
            currentPivotIndex = i;
            swap(nums, i, right);
        }
        console.log(nums, "   nums AFTER ")


        console.log(targetIndex == currentPivotIndex, targetIndex, currentPivotIndex)
        if (targetIndex == currentPivotIndex) {
            return nums[targetIndex];
        } else if (targetIndex > currentPivotIndex) {
            left = currentPivotIndex + 1;
        } else {
            right = currentPivotIndex - 1;
        }

        console.log(left, right, " END ")

    }

    return nums[left];


};

// console.log(findKthLargestIterative([3, 2, 3, 1, 2, 4, 5, 5, 6], 4))
// console.log(findKthLargestIterative([3,2,1,5,6,4], 2))
// console.log(findKthLargestIterative([1], 1))
// console.log(findKthLargestIterative([3,1,2,4], 2))
// console.log(findKthLargestIterative([7,6,5,4,3,2,1], 2))

console.log(sortKelemntsIterative([-1,2,0], 2))
