
// var jump = function (nums) {

//     if (nums.length == 1) return 0;

//     let left = 0; let right = 0;
//     let totalJumpCount = 0;

//     while (right < nums.length - 1) {
//         let maxJumpIndex = right;
//         for (let i = left; i <= right; i++) {
//             if ((nums[i] + i) > maxJumpIndex) maxJumpIndex = nums[i] + i;
//         }
//         left = right + 1;
//         right = maxJumpIndex;
//         totalJumpCount = totalJumpCount + 1;
//     }

//     return totalJumpCount;


// };

// // result = jump([5,0,0,0,0,2,0,4]);
// // result = jump([1,0]);
// // result = jump([10, 1, 2, 2, 0, 0, 0]);
// result = jump([5, 1, 3, 1, 0, 5, 0]);
// console.log(result, "result")






// // ************************************ trying again without looking after couple of days

// var jump = function (nums) {
//     let left = 0; let right = 0;
//     let jumpCount = 0;

//     while (right < nums.length - 1) {
//         let newLeft = right + 1;
//         let newRight = newLeft;
//         for (let i = left; i <= right; i++) {
//             if (i + nums[i] > newRight) newRight = i + nums[i];
//         }
//         left = newLeft;
//         right = newRight;
//         jumpCount++;
//     }
//     return jumpCount;

// };

// // result = jump([5,0,0,0,0,2,0,4]);
// // result = jump([1,0]);
// // result = jump([10, 1, 2, 2, 0, 0, 0]);
// // result = jump([5, 1, 3, 1, 0, 5, 0]);
// result = jump([1, 1, 3, 1, 0, 5, 0]);
// console.log(result, "result")




// // ************************************ trying again recursive

// var recurse = function(nums, [left, right], currJump){

//     let max = -1;
//     for(let i=left;i<=right;i++){
//         if(i >= nums.length-1) return currJump;
//         else{
//             if (i + nums[i] > max) max = i + nums[i];
//         }
//     }
//     return recurse(nums, [right+1, max], currJump+1);
// }

// var jump = function (nums) {
//     return recurse(nums, [0, 0], 0);
// };

// // result = jump([5,0,0,0,0,2,0,4]);
// // result = jump([1,0]);
// // result = jump([10, 1, 2, 2, 0, 0, 0]);
// // result = jump([5, 1, 3, 1, 0, 5, 0]);
// // result = jump([1, 1, 3, 1, 0, 5, 0]);
// result = jump([2,3,1,1,4]);

// console.log(result, "result")














// // ************************************ n^2 (brute force) without DP


// var jump = function (nums) {
//     let minJumps = new Array(nums.length).fill(Number.MAX_SAFE_INTEGER);
//     minJumps[0] = 0;

//     for(let i=0;i<nums.length;i++){
//         let fuel = nums[i];
//         let j = i;
//         while(fuel>=0){
//             if(minJumps[j]>minJumps[i]+1) minJumps[j] = minJumps[i]+1;
//             j++;
//             fuel--;
//         }
//     }
//     return minJumps[minJumps.length-1];

// };

// // result = jump([5,0,0,0,0,2,0,4]);
// // result = jump([1,0]);
// // result = jump([10, 1, 2, 2, 0, 0, 0]);
// result = jump([5, 1, 3, 1, 0, 5, 0]);
// // result = jump([1, 1, 3, 1, 0, 5, 0]);
// // result = jump([2,3,1,1,4]);

// console.log(result, "result")




// ************************************ n^2 DP (gives out of memeory)
// incomplete

var jump = function (nums) {
    let dpArray = [];
    for (let i = 0; i < nums.length; i++) {
        dpArray.push(new Array(nums.length).fill(Number.MAX_SAFE_INTEGER));
    }

    for (let i = 0; i < nums.length-1; i++) {
        let fuel = nums[i];
        for (let j = i; j < nums.length; j++) {
            if (i == j) {dpArray[i][j] = 0; continue;}
            if (i == 0) {
                if (fuel <= 0) continue;
                dpArray[i][j] = Math.min(dpArray[i][j], dpArray[i][i]+1)
            } else {
                // if(i == 1) console.log(i, j, fuel, dpArray[i][j], dpArray[i-1][j], dpArray[i-1][i]+1);
                if (fuel <= 0){
                    dpArray[i][j] = Math.min(dpArray[i-1][j], dpArray[i][j]);
                }else{
                    dpArray[i][j] = Math.min(dpArray[i-1][j], dpArray[i-1][i]+1, dpArray[i][j]);
                }
            }
            fuel--;
        }
    }
    // console.log(dpArray, " dpArray ")
    return dpArray[nums.length-2][nums.length-1];


};

// result = jump([5,0,0,0,0,2,0,4]);
// result = jump([1,0]);
// result = jump([10, 1, 2, 2, 0, 0, 0]);
// result = jump([5, 1, 3, 1, 0, 5, 0]);
result = jump([1, 1, 3, 1, 0, 5, 0]);
// result = jump([2,3,1,1,4]);

console.log(result, "result")