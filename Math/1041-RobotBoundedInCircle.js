/**
 * @param {string} instructions
 * @return {boolean}
 */
 var isRobotBounded = function(instructions) {
    
    let currPosition = [0, 0];
    let currDirection = [0, 1];
    
    for(let ins of instructions){
        if(ins == 'G'){
            currPosition[0] += 1*currDirection[0];
            currPosition[1] += 1*currDirection[1];
        }else if(ins == 'L'){
            
            if(currDirection[0] == 0){
                if(currDirection[1] == 1){
                    currDirection = [-1, 0];
                    
                }else if(currDirection[1] == -1){
                    currDirection = [1, 0];
                }
                

            }else if(currDirection[0] == 1){
                currDirection = [0, 1];

            }else if(currDirection[0] == -1){
                currDirection = [0, -1];

            }

            
        }else{

            if(currDirection[0] == 0){
                if(currDirection[1] == 1){
                    currDirection = [1, 0];
                    
                }else if(currDirection[1] == -1){
                    currDirection = [-1, 0];
                }
                

            }else if(currDirection[0] == 1){
                currDirection = [0, -1];

            }else if(currDirection[0] == -1){
                currDirection = [0, 1];

            }
        }
    }
    
    
    // console.log(currDirection, " currDirection ")
    // console.log(currPosition, " currPosition ")
    
    // if(currPosition[0] == 0 && currPosition[1] == 0){
    //     return true;
    // }
    
    if(currDirection[0] == 0 && currDirection[1] == 1 && (currPosition[0] != 0 || currPosition[1] != 0)){
        return false;
    }
    
    return true;
    
};