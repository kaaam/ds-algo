
// *********************************** incomplete O(N^2) .Bbut there are easier O(N^2) algos

// var stackObject = function(initialValue){
//     this.stack = initialValue;
//     this.addingAllowed = true;
//     this.matchedIndex = 1; // more than the length initially. Index till it's matched
//     this.freezedStack = false;
//     // this.pushToStack = function(char){this.stack.push(char)}
// }


// var longestPalindrome = function(s) {
//     let allStackObjects = [];
//     // let mainStack = new stackObject(s[0]);
//     allStackObjects[0] = new stackObject([s[0]]);

//     for(let i=1;i<s.length;i++){
//         // iterate string

//         for(let _stackObject of allStackObjects){
//             // iterate all stacks

//             if(_stackObject.addingAllowed == true && this.freezedStack == false){

//                 if(_stackObject.stack[_stackObject.stack.length-1] == s[i]
//                     || _stackObject.stack[_stackObject.stack.length-2] == s[i]){
//                         let temp = new stackObject([..._stackObject.stack]);
//                         allStackObjects.push(temp);
//                 }

//             }else{

//             }


//         }
//     }

//     // among all allStackObjects, return one with highest matchedIndex
// };

// result = longestPalindrome("abacabaabacaba");
// console.log(result, " result ")




// // let first = new stackObject(1);
// // let second = new stackObject(2);
// // console.log(first, second, " first, second ")
// // second.addingAllowed = 273883;
// // console.log(first, second, " first, second ")








// *********************************** complete (expanding around center)


// var longestPalindrome = function (s) {

//     let left; let right;

//     let currentLength = 0;
//     let currentString = ``;

//     let maxLength = 0;
//     let maxString = ``;

//     for (let i = 0; i < s.length; i = i + 0.5) {
//         left = Math.floor(i);
//         right = Math.ceil(i);
//         currentLength = 0;
//         currentString = ``

//         // console.log(left, right, i, " left, right, i before while")
//         while (left >= 0 && right <= s.length - 1 && s[left] == s[right]) {
//             // console.log(left, right, i, " left, right, i ")
//             if (left == right) currentString = `${s[left]}`;
//             else { currentString = `${s[left]}${currentString}${s[right]}` }
//             // currentLength = currentLength + 1;
//             left = left - 1;
//             right = right + 1;
//         }
//         currentLength = right - 1 - left + 1 + 1;

//         // console.log("\n")

//         if (currentLength > maxLength) {
//             maxLength = currentLength;
//             maxString = currentString;
//         }
//     }
//     return maxString;

// };



// result = longestPalindrome("a");
// result = longestPalindrome("babad");
// result = longestPalindrome("cbbd");
// result = longestPalindrome("abccbazabccbaaaabcdeffedcbaa");
// result = longestPalindrome("qqqqqqqqq");

// console.log(result, " result ")




// *********************************** complete (DP) using 2d array

// var longestPalindrome = function (s) {
//     // let dpArray = new Array(s.length).fill(new Array(s.length).fill(0));

//     let biggestLength = -1;
//     let biggestStartEnd;
//     // let biggestString;


//     let dpArray = [];
//     for(let i=0;i<s.length;i++){
//         let temp = [];
//         for(let j=0;j<s.length;j++){
//             temp.push(0);
//         }
//         dpArray.push(temp);
//     }
//     // console.log(dpArray, " dpArray ")


//     // x is bigger | x is column no
//     // i is bigger | i is column no
//     let x=0; let y=0;
//     while(x<s.length){
//         let i=x; let j=y;
//         while(i<s.length && j<s.length){
//             if(s[i] == s[j]){
//                 if(i-j>1){
//                     if(dpArray[j+1][i-1] == 1){
//                         if(i-j>biggestLength) {
//                             biggestLength = i-j;
//                             biggestStartEnd = [j, i];
//                         }
//                         dpArray[j][i]=1;
//                     }
//                 }else{
//                     dpArray[j][i]=1;
//                     if(i-j>biggestLength) {
//                         biggestLength = i-j;
//                         biggestStartEnd = [j, i];
//                     }
//                 }
//             }
//             i++;j++
//         }
//         x++;
//     }

//     // console.log(JSON.stringify(dpArray))
//     // console.log(biggestStartEnd, "   biggestStartEnd    ")
//     return s.substring(biggestStartEnd[1]+1, biggestStartEnd[0])

// };

// console.log(longestPalindrome("babad"));
// console.log(longestPalindrome("abccbadabccba"));







// *********************************** complete (DP) using dict

var longestPalindrome = function (s) {

    let biggestLength = -1;
    let biggestStartEnd;
    // let biggestString;


    let dpDict = {};
    for(let i=0;i<s.length;i++){
        dpDict[i] = {};
        for(let j=i;j<s.length;j++){
            dpDict[i][j] = 0;
        }
    }
    // console.log(dpDict, " dpDict ")
    // return ;


    // x is bigger | x is column no
    // i is bigger | i is column no
    let x=0; let y=0;
    while(x<s.length){
        let i=x; let j=y;
        while(i<s.length && j<s.length){
            if(s[i] == s[j]){
                if(i-j>1){
                    if(dpDict[j+1][i-1] == 1){
                        if(i-j>biggestLength) {
                            biggestLength = i-j;
                            biggestStartEnd = [j, i];
                        }
                        dpDict[j][i]=1;
                    }
                }else{
                    dpDict[j][i]=1;
                    if(i-j>biggestLength) {
                        biggestLength = i-j;
                        biggestStartEnd = [j, i];
                    }
                }
            }
            i++;j++
        }
        x++;
    }

    // console.log(dpDict)
    // console.log(biggestStartEnd, "   biggestStartEnd    ")
    return s.substring(biggestStartEnd[1]+1, biggestStartEnd[0])

};

// console.log(longestPalindrome("abccbadabccba"));
console.log(longestPalindrome("a"));