
var recurse =  function(map, digits, index){

    let currentDigit = digits[index];
    if(index==digits.length-1) return map[currentDigit];

    let listOfCombination=[];
    let returnList = recurse(map, digits, index+1);
    for(let i=0;i<map[currentDigit].length; i++){
        for(let j=0;j<returnList.length;j++){
            listOfCombination.push(`${map[currentDigit][i]}${returnList[j]}`);
        }
    }
    return listOfCombination;

}

var letterCombinations = function(digits) {
    let map = {
        // 0:[" "],
        // 1:[],
        2:["a", "b", "c"],
        3:["d", "e", "f"],
        4:["g", "h", "i"],
        5:["j", "k", "l"],
        6:["m", "n", "o"],
        7:["p", "q", "r", "s"],
        8:["t", "u", "v"],
        9:["w", "x", "y", "z"],
    };

    return recurse(map, digits, 0);
    
};

result = letterCombinations("23");
console.log(result, " result ")