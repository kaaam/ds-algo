
var UndergroundSystem = function () {
    this.avgTimeMapping = {};
    this.userMapping = {};

};

/** 
 * @param {number} id 
 * @param {string} stationName 
 * @param {number} t
 * @return {void}
 */
UndergroundSystem.prototype.checkIn = function (id, stationName, t) {
    this.userMapping[id] = { id, stationName, t };
};

/** 
 * @param {number} id 
 * @param {string} stationName 
 * @param {number} t
 * @return {void}
 */
UndergroundSystem.prototype.checkOut = function (id, stationName, t) {

    if (this.userMapping[id].stationName in this.avgTimeMapping) {

        if (stationName in this.avgTimeMapping[this.userMapping[id].stationName]) {
            this.avgTimeMapping[this.userMapping[id].stationName][stationName].count
                = this.avgTimeMapping[this.userMapping[id].stationName][stationName].count + 1;
            this.avgTimeMapping[this.userMapping[id].stationName][stationName].sum
                = this.avgTimeMapping[this.userMapping[id].stationName][stationName].sum + (t - this.userMapping[id].t);
        } else {
            this.avgTimeMapping[this.userMapping[id].stationName][stationName] = {
                count: 1,
                sum: (t - this.userMapping[id].t),
            }
        }

    } else {

        this.avgTimeMapping[this.userMapping[id].stationName] = {
            [stationName]: {
                count: 1,
                sum: (t - this.userMapping[id].t),
            }
        }
    }

    delete this.userMapping[id];
};

/** 
 * @param {string} startStation 
 * @param {string} endStation
 * @return {number}
 */
UndergroundSystem.prototype.getAverageTime = function (startStation, endStation) {
    return this.avgTimeMapping[startStation][endStation].sum/this.avgTimeMapping[startStation][endStation].count;
};



objectUnderGround = new UndergroundSystem();
objectUnderGround.checkIn(45,"Leyton",3);
objectUnderGround.checkIn(45,"Leyton",4);
objectUnderGround.checkOut(45,"Pa",8);
console.log(objectUnderGround.getAverageTime("Leyton","Pa"));