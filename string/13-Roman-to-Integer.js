var romanToInt = function (s) {
    let mapping = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000,
        "IV": 4,
        "IX": 9,
        "XL": 40,
        "XC": 90,
        "CD": 400,
        "CM": 900,
    }

    let sum = 0;
    let index = 0;

    while (index < s.length) {
        switch (`${s[index]}${s[index + 1]}`) {
            case "CM":
                sum = sum + mapping["CM"];
                index = index + 2;
                break;

            case "CD":
                sum = sum + mapping["CD"];
                index = index + 2;
                break;

            case "XC":
                sum = sum + mapping["XC"];
                index = index + 2;
                break;

            case "XL":
                sum = sum + mapping["XL"];
                index = index + 2;
                break;

            case "IX":
                sum = sum + mapping["IX"];
                index = index + 2;
                break;

            case "IV":
                sum = sum + mapping["IV"];
                index = index + 2;
                break;

            default: {
                switch (`${s[index]}`) {
                    case "I":
                        sum = sum + mapping["I"];
                        index = index + 1;
                        break;

                    case "V":
                        sum = sum + mapping["V"];
                        index = index + 1;
                        break;

                    case "X":
                        sum = sum + mapping["X"];
                        index = index + 1;
                        break;

                    case "L":
                        sum = sum + mapping["L"];
                        index = index + 1;
                        break;

                    case "C":
                        sum = sum + mapping["C"];
                        index = index + 1;
                        break;

                    case "D":
                        sum = sum + mapping["D"];
                        index = index + 1;
                        break;

                    case "M":
                        sum = sum + mapping["M"];
                        index = index + 1;
                        break;

                    default:
                    //   pass
                }
            }

        }
    }
    return sum;

};

// result = romanToInt("CXLIV");
// result = romanToInt("V");
// result = romanToInt("LVIII");
// result = romanToInt("CCXLIV");
result = romanToInt("CCXLI");

console.log(result, "  result  ")