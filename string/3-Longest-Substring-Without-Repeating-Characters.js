
var lengthOfLongestSubstring = function(s) {
    let maxCount = 0;
    
    let count = 0;
    let countStartIndex = 0;
    
  let letterLocation = {};

    for(let i=0;i<s.length;i++){
        
        if(s[i] in letterLocation && letterLocation[s[i]] >=countStartIndex){
            countStartIndex = letterLocation[s[i]] + 1;
            count = i-countStartIndex;
        }
        
        count++;
        letterLocation[s[i]] = i;
        if(count>maxCount){
            maxCount = count;
        }
    }
    return maxCount;
};