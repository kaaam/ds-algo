/**
 * @param {string} path
 * @return {string}
 */
 var simplifyPath = function(path) {
    let result = [];
    
    let curr = path.split("/");
    
    // console.log(curr)
    
    for(let x of curr){
        if(x == '..'){
            result.pop();
            result.pop();
        }else if(x == ''){
            continue;
        }else if(x == '.'){
            continue;
        }else{
            result.push('/')
            result.push(x)
        }
        // console.log(result)
    }
    if(result.length == 0) return "/";
    return result.join('');

    
    
};