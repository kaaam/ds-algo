var isValid = function (s) {
    let opening = new Set(["(", "{", "["])
    let closing = new Set([")", "}", "]"])
    let map = {
        ")": "(", "}": "{", "]": "["
    }
    let stack = [];

    for (let bracket of s) {
        // console.log(opening.has(bracket), bracket)
        if (opening.has(bracket)) {
            stack.push(bracket);
        } else {
            let _last = stack.pop();
            if (map[bracket] != _last) {
                return false;
            }
        }
    }
    if (stack.length) return false;
    return true;
};

// result = isValid('(())');
// result = isValid('(()}');
// result = isValid('[][]{{[{()}]}{()}');
// result = isValid('[][][][]()(){}{}');
console.log(result, " result ")