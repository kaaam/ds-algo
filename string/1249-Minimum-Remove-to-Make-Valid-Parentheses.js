
var minRemoveToMakeValid = function(s) {
    // ))(((((((((()

    let leftCount = 0;
    let rightCount = 0;
    let intermediateString=``;

    for(let letter of s){

        if(letter == '('){
            leftCount++;
        }else if(letter == ')'){
            if(rightCount >= leftCount){
                // 
                continue;
            }else{
                rightCount++;
            }
        }
        intermediateString = intermediateString + letter;
    }
    // (((((((((()


    let finalString=``;
    leftCount = 0;
    rightCount = 0;

    for(let i=intermediateString.length-1;i>=0;i--){

        if(intermediateString[i] == ')'){
            rightCount++;
        }else if(intermediateString[i] == '('){
            if(leftCount >= rightCount){
                // 
                continue;
            }else{
                leftCount++;
            }
        }
        // intermediateString = intermediateString + intermediateString[i];
        finalString = `${intermediateString[i]}${finalString}`;
    }


    return finalString;
};

// result = minRemoveToMakeValid('lee(t(c)o)de)');
// result = minRemoveToMakeValid('))((');
// result = minRemoveToMakeValid('))(((((((((()');
// result = minRemoveToMakeValid('a)b(c)d');
console.log("!",result, "result ")