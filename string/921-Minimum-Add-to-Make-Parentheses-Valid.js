var minAddToMakeValid = function(s) {
    
    let ans = 0;
    let count = 0;
    
    for(let char of s){
        if(char == ')'){
            if(count>0) {
                count--;
            }else{
                ans++;
            }
        }else{
            count++;
        }
    }

    return count+ans;
};


// result = minAddToMakeValid(")()(");
// result = minAddToMakeValid("()()(");
// result = minAddToMakeValid("))))(((((");
// result = minAddToMakeValid("()");
// result = minAddToMakeValid("(()(()())())");
console.log(result, " result ");