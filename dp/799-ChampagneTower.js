/**
 * @param {number} poured
 * @param {number} query_row
 * @param {number} query_glass
 * @return {number}
 */

var champagneTower = function (poured, query_row, query_glass) {
    let currRows = [poured];
    let left;
    let right;
    let sum = 0;

    for (let i = 1; i <= query_row; i++) {
        let tempArray = [];
        // console.log('\n')

        for (let j = 0; j <= i; j++) {
            if (j == 0) {
                left = currRows[0];
                if (left > 1) {
                    tempArray.push((left - 1) / 2);
                } else {
                    tempArray.push(0);
                }
                // console.log(((currRows[0] - 1) > 1 ? (currRows[0] - 1) : 0) / 2, " ---- ", (currRows[0] - 1), currRows)
                // tempArray.push(((currRows[0] - 1) > 1 ? (currRows[0] - 1) : 0) / 2);
            } else if (j == i) {
                left = currRows[0];
                if (left > 1) {
                    tempArray.push((left - 1) / 2);
                } else {
                    tempArray.push(0);
                }
            } else {
                sum = 0;
                left = currRows[j - 1];
                right = currRows[j];

                if (left > 1) {
                    sum += (left - 1) / 2;
                }

                if (right > 1) {
                    sum += (right - 1) / 2;
                }

                // let toAdd = (currRows[j - 1] > 1 ? ((currRows[j - 1] - 1) / 2) : 0) + (currRows[j] > 1 ? ((currRows[j] - 1) / 2) : 0);
                tempArray.push(sum);
            }
        }

        // console.log(tempArray)
        currRows = tempArray;
    }

    for (let i = 0; i < currRows.length; i++) {
        if (i == query_glass) {
            return currRows[query_glass] > 1 ? 1 : currRows[query_glass];
        }
    }

    // return currRows[query_glass];
};

// console.log(champagneTower(11, 5, 1))
// console.log(champagneTower(10, 0, 0))
// console.log(champagneTower(1000, 5, 1))
// console.log(champagneTower(1, 1, 0))