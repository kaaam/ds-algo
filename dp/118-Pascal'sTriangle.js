/**
 * @param {number} numRows
 * @return {number[][]}
 */


// **************************************************** iterartive
var generate = function(numRows) {
    let array = [[1], [1, 1]]
    if(numRows == 1) return [array[0]];
    if(numRows == 2) return array;

    for(let i=3;i<=numRows;i++){
        let curr = [1];
        let last = array[array.length-1];
        for(let i=0; i<last.length-1;i++){
            curr.push(last[i]+last[i+1]);
        }
        curr.push(1);
        array.push(curr);
    }

    return array;
    
};


console.log(generate(10));