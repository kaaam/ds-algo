/**
 * @param {number} numRows
 * @return {number[][]}
 */


// **************************************************** iterartive
var generate = function (numRows) {
    if (numRows == 1) return [1];
    if (numRows == 2) return [1, 1];
    let last = [1, 1];

    for (let i = 3; i <= numRows; i++) {
        let curr = [1];
        for (let i = 0; i < last.length - 1; i++) {
            curr.push(last[i] + last[i + 1]);
        }
        curr.push(1);
        last = curr;
    }

    return last;

};


console.log(generate(10));