/**
 * @param {number[][]} triangle
 * @return {number}
 */
var minimumTotal = function (triangle) {
    let min = triangle[0][0];
    for (let i = 1; i < triangle.length; i++) {
        min = Number.MAX_SAFE_INTEGER;
        for (let j = 0; j < triangle[i].length; j++) {
            if (j == 0) {
                triangle[i][j] = triangle[i][j] + triangle[i - 1][j];
            } else if (j == triangle[i].length - 1) {
                triangle[i][j] = triangle[i][j] + triangle[i - 1][j - 1];
            } else {
                triangle[i][j] = triangle[i][j] + Math.min(triangle[i - 1][j - 1], triangle[i - 1][j]);
            }
            if (triangle[i][j] < min) min = triangle[i][j];
        }
    }
    return min;
};


// console.log(minimumTotal([[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]]));
// console.log(minimumTotal([[2]]));
// console.log(minimumTotal([[2], [3, 4]]));