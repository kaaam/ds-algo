/**
 * @param {number} n
 * @return {number}
 */

// // ****************************** brute force (recursive)
// var recurse = function (leftIndex, rightIndex) {

//     if (leftIndex >= rightIndex) return 1;

//     if (leftIndex + 1 == rightIndex) {
//         return 2;
//     }


//     let count = 0;
//     for (let i = leftIndex; i <= rightIndex; i++) {
//         // console.log(leftIndex, i - 1, recurse(leftIndex, i - 1), " call 1 ")
//         let first = recurse(leftIndex, i - 1);
//         // console.log(i + 1, rightIndex, recurse(i + 1, rightIndex), " call 2 ")
//         let second = recurse(i + 1, rightIndex);
//         count += first * second;
//         // console.log(i, leftIndex, rightIndex, " for loop")
//     }
//     return count;

// }

// var numTrees = function (n) {
//     // let elements = [];
//     // for(let i=1;i<=n;i+=){
//     //     elements.push(i);
//     // }
//     return recurse(0, n - 1);
// };

// // console.log(numTrees(3));
// // console.log(numTrees(2));
// // console.log(numTrees(4));
// console.log(numTrees(19));






// ****************************** brute force (DP)
var recurse = function (leftIndex, rightIndex, cache = {}) {

    if (leftIndex >= rightIndex) return 1;

    if (leftIndex + 1 == rightIndex) {
        return 2;
    }


    let count = 0;
    if ((rightIndex - leftIndex) in cache) return cache[rightIndex - leftIndex];
    for (let i = leftIndex; i <= rightIndex; i++) {
        // console.log(leftIndex, i - 1, recurse(leftIndex, i - 1), " call 1 ")
        let first = recurse(leftIndex, i - 1, cache);
        // console.log(i + 1, rightIndex, recurse(i + 1, rightIndex), " call 2 ")
        let second = recurse(i + 1, rightIndex, cache);
        count += first * second;
        // console.log(i, leftIndex, rightIndex, " for loop")
    }
    cache[rightIndex - leftIndex] = count;
    return count;

}

var numTrees = function (n) {
    // let elements = [];
    // for(let i=1;i<=n;i+=){
    //     elements.push(i);
    // }
    return recurse(0, n - 1);
};

// console.log(numTrees(3));
// console.log(numTrees(2));
// console.log(numTrees(4));
console.log(numTrees(5));
// console.log(numTrees(19));