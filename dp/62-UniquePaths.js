/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
// var uniquePaths = function(m, n) {

//     let dpArray = [];
//     for(let i=0;i<m;i++){
//         dpArray.push(new Array(n).fill(0));
//     }

//     for(let i=0;i<m;i++){
//         for(let j=0;j<n;j++){
//             if(i==0||j==0){
//                 dpArray[i][j] = 1;
//             }else{
//                 dpArray[i][j] = dpArray[i-1][j]+dpArray[i][j-1]
//             }
//         }
//     }

//     return dpArray[m-1][n-1];
// };


var uniquePaths = function (m, n) {
    let newM = Math.min(m, n);
    let newN = Math.max(m, n);
    m  = newM;
    n  = newN;

    let dpArray = [];
    for (let i = 0; i < m; i++) {
        dpArray.push(new Array(n).fill(-1));
    }

    for (let i = 0; i < m; i++) {
        for (let j = i; j < n; j++) {
            if (i == 0 || j == 0) {
                dpArray[i][j] = 1;
            } else {
                dpArray[i][j] = dpArray[i - 1][j] + ((dpArray[i][j - 1] != -1) ? dpArray[i][j - 1] : dpArray[i - 1][j]);
            }
        }
    }

    // console.log(dpArray, " dpArray ")
    return dpArray[m - 1][n - 1];
};

// console.log(uniquePaths(3, 3));
// console.log(uniquePaths(3, 3));
// console.log(uniquePaths(3, 1));
// console.log(uniquePaths(3, 2));
// console.log(uniquePaths(2, 3));
// console.log(uniquePaths(5, 2));
console.log(uniquePaths(2, 5));
// console.log(uniquePaths(1, 1));