/**
 * @param {number} n
 * @return {number}
 */

// // ****************************** brute force (recursive)
// var recurse = function (leftIndex, rightIndex) {

//     if (leftIndex >= rightIndex) return 1;

//     if (leftIndex + 1 == rightIndex) {
//         return 2;
//     }


//     let count = 0;
//     for (let i = leftIndex; i <= rightIndex; i++) {
//         // console.log(leftIndex, i - 1, recurse(leftIndex, i - 1), " call 1 ")
//         let first = recurse(leftIndex, i - 1);
//         // console.log(i + 1, rightIndex, recurse(i + 1, rightIndex), " call 2 ")
//         let second = recurse(i + 1, rightIndex);
//         count += first * second;
//         // console.log(i, leftIndex, rightIndex, " for loop")
//     }
//     return count;

// }

// var numTrees = function (n) {
//     // let elements = [];
//     // for(let i=1;i<=n;i+=){
//     //     elements.push(i);
//     // }
//     return recurse(0, n - 1);
// };

// // console.log(numTrees(3));
// // console.log(numTrees(2));
// // console.log(numTrees(4));
// console.log(numTrees(19));






// ****************************** brute force (DP)
var recurse = function (leftIndex, rightIndex, cache = {}) {

    if (leftIndex > rightIndex) return [[null]];
    if (leftIndex == rightIndex) return [[leftIndex + 1]];

    if (leftIndex + 1 == rightIndex) {
        return [[leftIndex + 1, null, rightIndex + 1], [rightIndex + 1, leftIndex + 1, null]];
    }


    // let count = 0;
    let all = [];
    if ((rightIndex - leftIndex) in cache) {
        // console.log("!!!!!!!!!!!!!", rightIndex, leftIndex, " rightIndex, leftIndex, ", cache[rightIndex - leftIndex]);
        return cache[rightIndex - leftIndex];
    }
    for (let i = leftIndex; i <= rightIndex; i++) {
        // console.log(leftIndex, i - 1, recurse(leftIndex, i - 1), " call 1 ")
        let first = recurse(leftIndex, i - 1, cache);
        // console.log(i + 1, rightIndex, recurse(i + 1, rightIndex), " call 2 ")
        let second = recurse(i + 1, rightIndex, cache);
        // count += first * second;
        // console.log(first, second, " first, second, ")
        // console.log(i, leftIndex, rightIndex, " for loop")

        for (let _first of first) {
            for (let _second of second) {
                // if (_second[_second.length - 1] == null) _second.pop();
                console.log(_first, _second, " _first, _second ")
                // all.push([i + 1, ..._first, ..._second])
                // console.log(all, " all ")

                let temp = [i + 1, _first[0], _second[0]];
                for (let i = 1; i < Math.max(_first.length, _second.length); i = i + 2) {
                    temp.push(_first[i] ? _first[i] : null)
                    temp.push(_first[i + 1] ? _first[i + 1] : null)
                    temp.push(_second[i] ? _second[i] : null)
                    temp.push(_second[i + 1] ? _second[i + 1] : null)
                }
                console.log(temp, " temp ")
                all.push(temp)
            }
        }

    }
    cache[rightIndex - leftIndex] = all;
    return all;

}

var numTrees = function (n) {
    // let elements = [];
    // for(let i=1;i<=n;i+=){
    //     elements.push(i);
    // }
    return recurse(0, n - 1);
};

console.log(numTrees(3));
// console.log(numTrees(2));
// console.log(numTrees(4));
// console.log(numTrees(5));
// console.log(numTrees(19));