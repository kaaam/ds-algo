/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
var uniquePathsWithObstacles = function (obstacleGrid) {
    for (let i = 0; i < obstacleGrid.length; i++) {
        for (let j = 0; j < obstacleGrid[0].length; j++) {
            if (obstacleGrid[i][j] == 1) {
                obstacleGrid[i][j] = -1;
                continue;
            }
            if (i == 0 && j == 0) {
                obstacleGrid[i][j] = 1;
            } else {
                if (i != 0) {
                    obstacleGrid[i][j] = obstacleGrid[i][j] + (obstacleGrid[i - 1][j] != -1 ? obstacleGrid[i - 1][j] : 0);
                }
                if (j != 0) {
                    obstacleGrid[i][j] = obstacleGrid[i][j] + (obstacleGrid[i][j - 1] != -1 ? obstacleGrid[i][j - 1] : 0);
                }
            }
        }
    }
    // console.log(obstacleGrid, " obstacleGrid ")

    return obstacleGrid[obstacleGrid.length - 1][obstacleGrid[0].length - 1] == -1 ? 0 : obstacleGrid[obstacleGrid.length - 1][obstacleGrid[0].length - 1];
};



// console.log(uniquePathsWithObstacles([[0, 0, 0], [0, 1, 0], [0, 0, 0]]));
// console.log(uniquePathsWithObstacles([[0, 1, 0], [0, 1, 0], [0, 0, 0]]));
// console.log(uniquePathsWithObstacles([[1, 0, 0], [0, 1, 0], [0, 0, 0]]));
// console.log(uniquePathsWithObstacles([[0, 1, 0], [0, 1, 0], [0, 0, 1]]));
// console.log(uniquePathsWithObstacles([[1]]));
// console.log(uniquePathsWithObstacles([[0, 0, 0, 0, 0, 1, 0], [0, 1, 0, 0, 0, 0, 0]]));

