/**
 * @param {number} n
 * @return {number}
 */


// // ************************************************ brute force | recursion

// var recurse = function (currN, count, maxN) {
//     // console.log(currN, count, maxN, " currN, count, maxN opening")
//     // if (currN >= maxN) {
//     //     console.log(currN, count, maxN, " currN, count, maxN ending")
//     //     console.log('\n')
//     // }


//     // console.log('\n')
//     if (currN >= maxN) return count + 1;

//     if (currN + 1 <= maxN) { count = recurse(currN + 1, count, maxN); }
//     if (currN + 2 <= maxN) { count = recurse(currN + 2, count, maxN); }
//     // console.log(currN, count, maxN, " currN, count, maxN ending")
//     // console.log('\n')
//     return count;
// }

// var climbStairs = function (n) {
//     return recurse(0, 0, n);
// };

// console.log(climbStairs(4));




// // ************************************************ brute force | recursion (without passing count)

// var recurse = function (currN, maxN) {
//     // console.log(currN, count, maxN, " currN, count, maxN opening")
//     // if (currN >= maxN) {
//     //     console.log(currN, count, maxN, " currN, count, maxN ending")
//     //     console.log('\n')
//     // }


//     // console.log('\n')
//     let count = 0;
//     if (currN >= maxN) return 1;

//     if (currN + 1 <= maxN) { count += recurse(currN + 1, maxN); }
//     if (currN + 2 <= maxN) { count += recurse(currN + 2, maxN); }
//     // console.log(currN, count, maxN, " currN, count, maxN ending")
//     // console.log('\n')
//     return count;
// }

// var climbStairs = function (n) {
//     return recurse(0, n);
// };

// console.log(climbStairs(4));



// // ************************************************ DP | recursion(DFS) (without passing count)

// var recurse = function (currN, maxN, cache) {
//     // console.log(currN, count, maxN, " currN, count, maxN opening")
//     // if (currN >= maxN) {
//     //     console.log(currN, count, maxN, " currN, count, maxN ending")
//     //     console.log('\n')
//     // }


//     // console.log('\n')
//     if (currN in cache) return cache[currN];
//     let count = 0;
//     if (currN >= maxN) return 1;

//     if (currN + 1 <= maxN) { count += recurse(currN + 1, maxN, cache); }
//     if (currN + 2 <= maxN) { count += recurse(currN + 2, maxN, cache); }
//     cache[currN] = count;
//     // console.log(currN, count, maxN, " currN, count, maxN ending")
//     // console.log('\n')
//     return count;
// }

// var climbStairs = function (n) {
//     return recurse(0, n, {});
// };

// console.log(climbStairs(4));







// // ************************************************ DP (iterative)/fibonachi

// var climbStairs = function (n) {
//     let dpArray = new Array(n + 1).fill(0);

//     for (let i = 0; i < dpArray.length; i++) {
//         if (i == 0) { dpArray[0] = 1; continue; }
//         if (dpArray[i - 1] != null && dpArray[i - 1] != undefined) {
//             dpArray[i] += dpArray[i - 1];
//         }
//         if (dpArray[i - 2] != null && dpArray[i - 2] != undefined) {
//             dpArray[i] += dpArray[i - 2];
//         }
//     }
//     // console.log(dpArray, "   dpArray  ")
//     return dpArray[dpArray.length-1];
// };

// console.log(climbStairs(4));


