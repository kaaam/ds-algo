/**
 * @param {string} s1
 * @param {string} s2
 * @param {string} s3
 * @return {boolean}
 */


// // ************************************ brute force
// var recurse = function (s1, s2, s3, i1, i2, i3) {
//     // console.log(i1, i2, i3)

//     if (i3 == s3.length){
//         if (i2 == s2.length && i1 == s1.length) return true;
//         else return false;
//     }

//     let first = false;
//     let second = false;


//     if (s3[i3] == s1[i1]) {
//         first = recurse(s1, s2, s3, i1 + 1, i2, i3 + 1);
//     }
//     if (s3[i3] == s2[i2]) {
//         second = recurse(s1, s2, s3, i1, i2 + 1, i3 + 1);
//     }

//     return first || second;

// }

// var isInterleave = function (s1, s2, s3) {
//     return recurse(s1, s2, s3, 0, 0, 0)
// };


// console.log(isInterleave(
//     'aabcde'
//     , 'aazfgh'
//     , 'aabcaazfdegh'
// ))



var updateCache = function (i1, i2, i3, cache, value) {
    if (i1 in cache) {
        if (i2 in cache[i1]) {
            cache[i1][i2][i3] = value;
        } else {
            cache[i1][i2] = { [i3]: value };
        }
    } else {
        cache[i1] = { [i2]: { [i3]: value } }
    }
    return cache;
}



// ************************************ brute force (dp)
var recurse = function (s1, s2, s3, i1, i2, i3, cache) {
    // console.log(i3)
    // console.log(cache)

    if (i3 == s3.length) {
        if (i2 == s2.length && i1 == s1.length) return [true, cache];
        else return [false, cache];
    }

    if ((i1 in cache) && (i2 in cache[i1]) && (i3 in cache[i1][i2])) {
        // console.log('hit')
        return [cache[i1][i2][i3], cache];
    }

    let first = false;
    let second = false;


    if (s3[i3] == s1[i1]) {
        [first, cache] = recurse(s1, s2, s3, i1 + 1, i2, i3 + 1, cache);
        // console.log(cache, "  cache ", i1, i2, i3)
        cache = updateCache(i1 + 1, i2, i3 + 1, cache, first);
        // console.log(cache, "  cache ", i1, i2, i3)
    }

    if (s3[i3] == s2[i2]) {
        [second, cache] = recurse(s1, s2, s3, i1, i2 + 1, i3 + 1, cache);
        cache = updateCache(i1, i2 + 1, i3 + 1, cache, second);
    }

    return [first || second, cache];

}

var isInterleave = function (s1, s2, s3) {
    // new Array(s3.length).fill(false);
    let obj = {};
    let [result] = recurse(s1, s2, s3, 0, 0, 0, obj)
    // console.log(obj, " obj ")
    return result;
};


// console.log(isInterleave(
//     'aabcde'
//     , 'aazfgh'
//     , 'aabcaazfdegh'
// ))

// console.log(isInterleave(
//     'a'
//     , 'b'
//     , 'c'
// ))


console.log(isInterleave(
    "accbaabaaabbcbaacbababacaababbcbabaababcaabbbbbcacbaa"
    , "cabaabcbabcbaaaacababccbbccaaabaacbbaaabccacabaaccbbcbcb"
    , "accbcaaabbaabaaabbcbcbabacbacbababaacaaaaacbabaabbcbccbbabbccaaaaabaabcabbcaabaaabbcbcbbbcacabaaacccbbcbbaacb"
))