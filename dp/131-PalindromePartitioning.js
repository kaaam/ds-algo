/**
 * @param {string} s
 * @return {string[][]}
 */

var iterate = function (s, currStrArray, currIndex) {

    if(currIndex>=s.length) return currStrArray;
    let last = currStrArray.pop();
    if (last) {
        if (last[0] != last[last.length - 1]) {
            return false;
        }
    }else{
        last = '';
    }

    let combinations = [];
    currStrArray.push(last + s[currIndex]);
    console.log(currStrArray, " currStrArray first ", currIndex)
    let first = iterate(s, [...currStrArray], currIndex+1);
    if (first) combinations = [...combinations, ...first];
    console.log(first, " first ", currIndex)
    console.log(currStrArray, " currStrArray second ", currIndex, last, s[currIndex])
    currStrArray.pop();
    currStrArray.push(last);
    currStrArray.push(s[currIndex]);
    console.log(currStrArray, " currStrArray second ", currIndex, last, s[currIndex])
    let second = iterate(s, [...currStrArray], currIndex+1);
    if (second) combinations = [...combinations, ...second];
    console.log(second, " second ", currIndex)
    return combinations;
}

var partition = function (s) {
    return iterate(s, [s[0]], 1)
};


console.log(partition('aabcbcbaa'));