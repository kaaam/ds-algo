#########utility functions###########

def swap_two_numbers(a, b):
	x = a
	a = b
	b = x
	return a, b
#########utility functions###########






# Given an unsorted array A of size N of non-negative integers, find a continuous sub-array which adds to a given number S.
# https://practice.geeksforgeeks.org/problems/subarray-with-given-sum/0
def unsorted_subarray_sum(unsorted_list, sum):
	_sum = 0
	start = 0
	for i in range(len(unsorted_list)):
		_sum = _sum + unsorted_list[i]
		# print(start, i, _sum)
		if _sum > sum:
			while _sum >= sum:
				_sum = _sum - unsorted_list[start]
				start = start + 1
				if _sum == sum:
					return unsorted_list[start], unsorted_list[i]
		elif _sum == sum:
			return unsorted_list[start], unsorted_list[i]

print('unsorted_subarray_sum', unsorted_subarray_sum([1,2,3,7,5], 12))
print('unsorted_subarray_sum', unsorted_subarray_sum([3,2,1,5,20,7], 27))




# Given an array arr of N integers. Find the contiguous sub-array with maximum sum.
# Kadane's Algorithm
# https://practice.geeksforgeeks.org/problems/kadanes-algorithm/0#ExpectOP
def Kadane(unsorted_list_with_positive_and_negative_integers):
	_list = unsorted_list_with_positive_and_negative_integers
	_sum = 0
	max = -99999999

	for i in range(len(_list)):
		_sum = _sum + _list[i]
		if _sum <= 0:
			_sum = 0
		if _sum > max:
			max = _sum
	return max

print('Kadane', Kadane([1, 2, 3, -2, 5]))
print('Kadane *giving 0 should be -1*', Kadane([-1, -2, -3, -4])) # giving 0 should be -1


# Merge two sorted arrays Without Extra Space
# https://practice.geeksforgeeks.org/problems/merge-two-sorted-arrays/0/#ExpectOP
def merge_two_array_withoutextraspace(sorted_list_one, sorted_list_two):																																											
	pass


# https://practice.geeksforgeeks.org/problems/count-the-triplets/0
# Given an array of distinct integers. The task is to count all the triplets such that sum of two elements equals the third element.
def sum_of_two_is_third_method_one(unsorted_list):
	unsorted_list.sort()
	_integers = {}
	_count = 0
	for i in unsorted_list:
		_integers[i] = True
	for i in range(len(unsorted_list)):
		for j in range(i+1, len(unsorted_list)):
			if unsorted_list[i] + unsorted_list[j] in _integers:
				_count = _count + 1
	return _count

print('sum_of_two_is_third_method_one', sum_of_two_is_third_method_one([1, 5, 3, 2]))
print('sum_of_two_is_third_method_one', sum_of_two_is_third_method_one([3, 2, 7]))



def _merge_two_sorted_array_withoutextraspace(bigger_list, smaller_list):
	for i in range(len(bigger_list)):
		for x in range(i, len(bigger_list)):
			# if x-i > len(smaller_list) -1:
			# 	pass
			# else:
			try:
				if bigger_list[x] > smaller_list[x-i]:
					bigger_list[x], smaller_list[x-i] = swap_two_numbers(bigger_list[x], smaller_list[x-i])
			except IndexError:
				pass
	return bigger_list, smaller_list

def merge_two_sorted_array_withoutextraspace(sorted_list_one, sorted_list_two):
	if len(sorted_list_one) > len(sorted_list_two):
		return _merge_two_sorted_array_withoutextraspace(sorted_list_one, sorted_list_two)
	else:
		return _merge_two_sorted_array_withoutextraspace(sorted_list_two, sorted_list_one)

print('merge_two_sorted_array_withoutextraspace', merge_two_sorted_array_withoutextraspace([1, 5, 9, 10, 15, 20], [2, 3, 8, 13]))
print('merge_two_sorted_array_withoutextraspace', merge_two_sorted_array_withoutextraspace([10], [2, 3]))



def rearrange_sorted_array_alternately_withoutextraspace(sorted_list):
	bigest_integer_index = len(sorted_list)-1
	for i in range(1, len(sorted_list), 2):
		sorted_list[i-1], sorted_list[bigest_integer_index] = swap_two_numbers(sorted_list[i-1], sorted_list[bigest_integer_index])
		sorted_list[i], sorted_list[len(sorted_list)-1] = swap_two_numbers(sorted_list[i], sorted_list[len(sorted_list)-1])
		bigest_integer_index = bigest_integer_index - 1
	return sorted_list


print('rearrange_sorted_array_alternately_withoutextraspace', rearrange_sorted_array_alternately_withoutextraspace([1,3,5,7,9,11,13,15,17,19]))