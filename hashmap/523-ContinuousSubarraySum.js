/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
var checkSubarraySum = function (nums, k) {

    let reminderDict = {};
    let currSum = 0;
    for (let i = 0; i < nums.length; i++) {
        currSum = currSum + nums[i];

        let reminder = currSum % k;
        // console.log(nums[i], i, currSum, reminder, reminderDict)
        if (reminder == 0 && i >= 1) return true;

        if (reminder in reminderDict) {
            if (i - reminderDict[reminder] >= 2) return true;
        } else {
            reminderDict[reminder] = i;
        }


    }
    return false;
};


// console.log(checkSubarraySum([23,2,4,6,7], 6));
// console.log(checkSubarraySum([23,2,6,4,7], 6));
// console.log(checkSubarraySum([23,2,6,4,7], 13));
// console.log(checkSubarraySum([100, 1, 3, 2, 3, 1, 2, 3, 1, 2, 3, 1 ], 104));
// console.log(checkSubarraySum([100, 1, 3, 2, 3, 1, 2, 3, 1, 2, 3, 1 ], 1));
// console.log(checkSubarraySum([12], 13));
// console.log(checkSubarraySum([12], 11));
// console.log(checkSubarraySum([12], 12));
// console.log(checkSubarraySum([1, 2, 3, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0], 7));
// console.log(checkSubarraySum([1, 2, 3, 7], 7));
// console.log(checkSubarraySum([0, 0], 1));




// [23,2,4,6,7], 6
// [100, 1, 3, 2, 3, 1, 2, 3, 1, 2, 3, 1 ], 104
// [1, 1, 1, 1, 1] 3