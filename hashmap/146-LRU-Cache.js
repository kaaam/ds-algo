/**
 * @param {number} capacity
 */
var LRUCache = function (capacity) {
    this.capacity = capacity;

    // this.linkedListStart = {val: -1, next:-1, prev: -1};
    // this.linkedListEnd = linkedListStart;
    this.linkedListStart = null;
    this.linkedListEnd = null;

    // this.keyMapping = {[-1]: keyStart};
    this.keyMapping = {};
    this.currentLength = 0;

};

/** 
 * @param {number} key
 * @return {number}
 */
LRUCache.prototype.get = function (key) {
    let tempNode = this.keyMapping[key];


    if (key in this.keyMapping) {
        if (this.linkedListStart == this.linkedListEnd) {
            return tempNode.val;
        } else {

            if (!tempNode.next) {
                return tempNode.val;
            } else if (!tempNode.prev) {
                this.linkedListStart = tempNode.next;
                this.linkedListStart.prev = null;
                this.linkedListEnd.next = tempNode;
                tempNode.prev = this.linkedListEnd;
                this.linkedListEnd = tempNode;
                this.linkedListEnd.next = null;

            } else {
                tempNode.prev.next = tempNode.next;
                tempNode.next.prev = tempNode.prev;
                this.linkedListEnd.next = tempNode;
                tempNode.prev = this.linkedListEnd;
                this.linkedListEnd = tempNode;
                this.linkedListEnd.next = null;
            }
        }

    } else {
        return -1;
    }
    return tempNode.val;
};

/** 
 * @param {number} key 
 * @param {number} value
 * @return {void}
 */
LRUCache.prototype.put = function (key, value) {

    // adding at right end
    let tempNode;

    if (key in this.keyMapping) {
        // if key found

        tempNode = this.keyMapping[key];

        if (!tempNode.next) {
            tempNode.val = value;
        } else if (!tempNode.prev) {
            this.linkedListStart = tempNode.next;
            this.linkedListStart.prev = null;
            this.linkedListEnd.next = tempNode;
            tempNode.prev = this.linkedListEnd;
            tempNode.next = null;
            this.linkedListEnd = tempNode;
            this.linkedListEnd.val = value;

        } else {
            tempNode.prev.next = tempNode.next;
            tempNode.next.prev = tempNode.prev;
            this.linkedListEnd.next = tempNode;
            tempNode.next = null;
            tempNode.prev = this.linkedListEnd;
            this.linkedListEnd = tempNode;
            this.linkedListEnd.val = value;
        }

    } else {
        // key not found
        if (this.currentLength == this.capacity) {
            // delete, capacity reached
            delete this.keyMapping[this.linkedListStart.key];
            if (this.linkedListStart == this.linkedListEnd) {
                this.linkedListStart = null;
                this.linkedListEnd = null;
            } else {
                this.linkedListStart = this.linkedListStart.next;
                this.linkedListStart.prev = null;
            }
            this.currentLength--;
        }

        if (!this.linkedListStart) {
            tempNode = { val: value, next: null, prev: null, key };
            this.linkedListEnd = tempNode;
            this.linkedListStart = tempNode;
            this.keyMapping[key] = tempNode;
        } else {
            tempNode = { val: value, next: null, prev: this.linkedListEnd, key };
            this.linkedListEnd.next = tempNode;
            this.keyMapping[key] = tempNode;
            this.linkedListEnd = tempNode;
        }
        this.currentLength++;
    }

};

/** 
 * Your LRUCache object will be instantiated and called as such:
 * var obj = new LRUCache(capacity)
 * var param_1 = obj.get(key)
 * obj.put(key,value)
 */

// lru = new LRUCache(4);
// console.log(lru.keyMapping)
// lru.put(1, 1);
// console.log(lru.keyMapping)
// lru.get(1);
// console.log(lru.keyMapping)
// lru.put(2, 2);
// console.log(lru.keyMapping)
// console.log('\n')
// console.log(lru.linkedListStart)
// console.log('\n')
// console.log(lru.linkedListEnd)
// console.log('\n')
// lru.put(3, 3);
// console.log(lru.keyMapping)
// console.log('\n')
// console.log(lru.linkedListStart)
// console.log('\n')
// console.log(lru.linkedListEnd)
// console.log('\n')
// lru.get(3);
// console.log(lru.keyMapping)
// console.log('\n')
// console.log(lru.linkedListStart)
// console.log('\n')
// console.log(lru.linkedListEnd)
// console.log('\n')

// console.log(" --------- lru.get(2); ----------")
// lru.get(2);
// console.log(lru.keyMapping)
// console.log('\n')
// console.log(lru.linkedListStart)
// console.log('\n')
// console.log(lru.linkedListEnd)
// console.log('\n')


// console.log(" --------- lru.put(3, 3); ----------")
// lru.put(3, 3);
// console.log(lru.keyMapping)
// console.log('\n')
// console.log(lru.linkedListStart)
// console.log('\n')
// console.log(lru.linkedListEnd)
// console.log('\n')

// ["LRUCache","put","put","get","put","get","put","get","get","get"]
// ["put","put","get","put","get","put","get","get","get"]
// [[2],[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]

lru = new LRUCache(2);
lru.put(1, 1)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.put(2, 2)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.get(1)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.put(3, 3)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.get(2)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.put(4, 4)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.get(1)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.get(3)
console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')
console.log('\n')
console.log('\n')
lru.get(4)


console.log(lru.keyMapping)
console.log('\n')
console.log(lru.linkedListStart)
console.log('\n')
console.log(lru.linkedListEnd)
console.log('\n')