/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */



// ********* WITHOUT ACTAULLY CONSTRUCTING TREE***********
// ********* root.length now working in leetcode ***********
// ********* root is not array, it's literally top(root node). Got confused because of input shown in example ***********
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var verticalOrderWorksForArray = function (root) {
    if(root.length==0) return [];

    let diff = 1;
    let positionArray = [0];
    let ansDict = {};
    let min = Number.MAX_SAFE_INTEGER;
    let max = Number.MIN_SAFE_INTEGER;

    let currentPositionValue;
    for (let i = 0; i < root.length; i++) {

        currentPositionValue = positionArray[i];

        if (root[i] != null) {
            if (currentPositionValue in ansDict) {
                ansDict[currentPositionValue].push(root[i])
            } else {
                ansDict[currentPositionValue] = [root[i]];
            }

            if (currentPositionValue < min) min = currentPositionValue;
            if (currentPositionValue > max) max = currentPositionValue;

        }


        if (i + diff <= root.length) {
            positionArray.push(currentPositionValue - 1);
        }

        if (i + diff + 1 <= root.length) {
            positionArray.push(currentPositionValue + 1);
        }

        diff++;
    }

    console.log(ansDict)
    console.log(positionArray)
    console.log(min)
    console.log(max)

    let ansList = [];
    for (let i = min; i <= max; i++) {
        if (i in ansDict) {
            ansList.push(ansDict[i])
        }
    }
    // console.log(ansList)
    return ansList;


};

// console.log(verticalOrder([3, 9, 20, null, null, 15, 7]))
// console.log(verticalOrder([3,9,8,4,0,1,7]))
// console.log(verticalOrder([3,9,8,4,0,1,7,null,null,null,2,5]))
// console.log(verticalOrder([]))
// console.log(verticalOrder([1]))
// console.log(verticalOrder([1, 2, 3]))





/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */


 var traverse = function(node, nodePosition, ansDict={'min':Number.MAX_SAFE_INTEGER, 'max':Number.MIN_SAFE_INTEGER}){

    if(node == null || node.val == null) return ansDict;
    else{
        if(nodePosition in ansDict){
            ansDict[nodePosition].push(node.val);
        }else{
            ansDict[nodePosition] = [node.val];
        }
        if(nodePosition < ansDict.min) ansDict.min = nodePosition;
        if(nodePosition > ansDict.max) ansDict.max = nodePosition;
        ansDict = traverse(node.left, nodePosition-1, ansDict);
        ansDict = traverse(node.right, nodePosition+1, ansDict);
    }

    return ansDict;
}

// [3,9,8,4,0,1,7,null,null,null,2,5]  -  fails
var verticalOrderFails = function(root) {
    // console.log(root.val)
    let ansDict =  traverse(root, 0)
    let ansArray = [];
    // console.log(ansDict, " ansDict ");
    
    for(let i=ansDict.min;i<=ansDict.max;i++){
        if(i in ansDict){
            ansArray.push(ansDict[i]);
        }
    }
    // console.log(ansArray)
    return ansArray;
    
};

// console.log(verticalOrder([3, 9, 20, null, null, 15, 7]))
// console.log(verticalOrder([3,9,8,4,0,1,7]))
// console.log(verticalOrder([3,9,8,4,0,1,7,null,null,null,2,5]))
// console.log(verticalOrder([]))
// console.log(verticalOrder([1]))
// console.log(verticalOrder([1, 2, 3]))





/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
var verticalOrder = function(root) {
    
};