/**
 * @param {number[]} time
 * @return {number}
 */
var numPairsDivisibleBy60WRONG = function (time) {

    let divDictMore = {};
    let divDictLess = {};
    for (let _time of time) {
        let div = _time / 60;
        let _key  = (div % 1).toFixed(5);
        console.log(div, " div ")
        if (div >= 1) {
            _key in divDictMore ? divDictMore[_key].push(_time) : divDictMore[_key] = [_time];
        } else {
            _key  = (div % 1).toFixed(5);
            _key in divDictLess ? divDictLess[_key].push(_time) : divDictLess[_key] = [_time];
        }
    }
    console.log(divDictMore, divDictLess, " divDictMore, divDictLess ")

    let count = 0;
    for (const [key, value] of Object.entries(divDictLess)) {
        console.log(key, value);
        let remaining = 1 - key;
        if (remaining in divDictMore) {
            count = count + divDictLess[key].length * divDictLess[remaining].length;
            count = count + divDictLess[key].length * divDictMore[remaining].length;
            // delete divDictMore[key];
        } else {
            if (remaining in divDictLess) {
                count = count + divDictLess[remaining].length * divDictLess[key].length;
            } else {
                continue;
            }
        };
    }

    for (const [key, value] of Object.entries(divDictMore)) {
        console.log(key, value, " !!! ");
        let remaining = 1 - key;
        console.log(remaining, " remaining ")
        if (remaining in divDictMore) {
            console.log(divDictMore[remaining].length * divDictMore[key].length, "----")
            count = count + divDictMore[remaining].length * divDictMore[key].length;
        }else if(remaining == 1){
            console.log(divDictMore[key].length, "++++", count, divDictMore[key].length * divDictMore[key].length-1, divDictMore[key].length, divDictMore[key].length-1)
            count = count + ((divDictMore[key].length) * (divDictMore[key].length-1));
        }else {
            continue;
        }
    }

    console.log(count, " count ")
    return count;
};

var numPairsDivisibleBy60 = function (time) {
    countDict = {};
    for(let t of time){
        if(t<60){
            // console.log(t, 60-t);
            if((60-t) in countDict){
                countDict[60-t] = countDict[60-t] + 1;
            }else{
                countDict[60-t] = 1;
            }
        }else{
            // console.log(t, 60-(t%60), " gt ");
            if((60-(t%60)) in countDict){
                countDict[60-(t%60)] = countDict[60-(t%60)] + 1;
            }else{
                countDict[60-(t%60)] = 1;
            }
        }
        // console.log(countDict)
    }
    // console.log(countDict)
    

    let result = 0;
    let visited = new Set();
    for (let [key, value] of Object.entries(countDict)) {
        // console.log(visited, ' visited ', key)
        key = parseInt(key);
        if(visited.has(key)) continue;
        if(key == 0 || key == 30 || key == 60) {
            
            //TODO:  n!/((n-r)!×r!)
            // result  = result + ((value)*(value-1));
            for(let i=value-1;i>=0;i--) {result = result+i;}
        }else{
            if((60-key) in countDict){
                // console.log(value, (value)*(countDict[60-key]), ' value, (value)*(countDict[60-key]) ')
                // console.log(result, " result ", key)
                result  = result + ((value)*(countDict[60-key]));
                // console.log(result, " result ", key)
                visited.add(60-key);
                visited.add(key);
            }
        }
    }
    // console.log(result, " result")
    return result;
}


// console.log(numPairsDivisibleBy60([30, 20, 150, 100, 40, 60, 120]))
console.log(numPairsDivisibleBy60([60, 60, 60]))
// console.log(numPairsDivisibleBy60([50, 10, 110, 20, 170, 130, 60, 120, 190, 60]))
// numPairsDivisibleBy60([50, 10, 110, 20, 170, 130, 190])
// console.log(numPairsDivisibleBy60([30,20,150,100,40]))
// console.log(numPairsDivisibleBy60([439,407,197,191,291,486,30,307,11]))


// [30,20,150,100,40]
// [0.5, 0.33333333333, 2.5, 1.66666666667, 0.66666666666]
// [0.5, 0.33333333333, 0.5, 0.66666666667, 0.66666666666]
// {0.5: [3, 150], 0.33333333333: [20], 0.66666666667: [100 , 40]}
// {0.5: [3, 150], 0.33333333333: [20], 0.66666666667: [100 , 40]}



// [20,30,40,100,150]