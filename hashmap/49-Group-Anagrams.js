/**
 * @param {string[]} strs
 * @return {string[][]}
 */



// approcah 1 - assign every alphabet one number,
//  if the sum of alphabets is same and length is same, both are Anagrams

// var geroup = function(){
//     this.uniqueLetters = new Set();

// }

// ************************************************************ THIS APPROCH FAILS HERE FOR "duh","ill"


// var groupAnagrams = function (strs) {

//     let doubltDict = {};

//     let letterToNumber = {
//         a: 1,
//         b: 2,
//         c: 3,
//         d: 4,
//         e: 5,
//         f: 6,
//         g: 7,
//         h: 8,
//         i: 9,
//         j: 10,
//         k: 11,
//         l: 12,
//         m: 13,
//         n: 14,
//         o: 15,
//         p: 16,
//         q: 17,
//         r: 18,
//         S: 19,
//         t: 20,
//         u: 21,
//         v: 22,
//         w: 23,
//         x: 24,
//         y: 25,
//         z: 26,
//     };

//     for (let i = 0; i < strs.length; i++) {
//         let currLength = strs[i].length;
//         let currSum = 0;
//         for (let j = 0; j < strs[i].length; j++) {
//             currSum = currSum + letterToNumber[strs[i][j]];
//         }

//         if (currSum in doubltDict) {
//             if (currLength in doubltDict[currSum]) {
//                 doubltDict[currSum][currLength].push(strs[i]);
//             } else {
//                 doubltDict[currSum][currLength] = [strs[i]];
//             }
//         } else {
//             doubltDict[currSum] = { [currLength]: [strs[i]] };
//         }

//     }
//     // console.log(doubltDict, " doubltDict ")

//     // for(const [key, value] of Object.)
//     let result = [];
//     for (const _sumKey in doubltDict) {
//         for (const _lengthKey in doubltDict[_sumKey]) {
//             result.push(doubltDict[_sumKey][_lengthKey]);
//         }
//     }

//     // console.log(result, " result ")
//     return result;

// };


// console.log(groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]));
// console.log(groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat", "", "", "tea", "tea"]));
// console.log(groupAnagrams(["eat"]));








// ************************************************************
// approcah 2 order every string  and match ( o(nlogn) for sorting every string multiplied by number of string)
// ["eat", "tea", "tan", "ate", "nat", "bat"] will become ["eat", "eat", "tan", "eat", "tan", "bat"]


// var groupAnagrams = function (strs) {
//     let ansDict = {};
//     for (str of strs) {
//         let strArray = str.split('');
//         strArray.sort((a, b) => a.charCodeAt() - b.charCodeAt());
//         let sortedString = strArray.join('');
//         sortedString in ansDict ? ansDict[sortedString].push(str) : ansDict[sortedString] = [str];
//     }
//     // console.log(ansDict, " ansDict ")
//     return Object.values(ansDict)
// }

// console.log(groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]));








// ************************************************************
// approcah 3 ( looked at someone else's solution) order every string  and match ( o(nlogn) for sorting every string multiplied by number of string)

// var groupAnagrams = function(strs) {
//     let res = {};
//     for (let str of strs) {
//         let count = new Array(26).fill(0);
//         for (let char of str) count[char.charCodeAt()-97]++;
//         let key = count.join("#");
//         res[key] ? res[key].push(str) : res[key] = [str];
//     }
//     return Object.values(res);
// };
// Time Complexity: O(n*k) where n is the size of input array and k is the maximum length of string in input array
// Space Complexity: O(n)


// ********** tried to rewrite without looking

// var groupAnagrams = function (strs) {
//     let ansDict = {};
//     for (str of strs) {
//         let stringKey = new Array(26).fill(0);
//         for (char of str) {
//             stringKey[char.charCodeAt() - 97]++;
//         }
//         let key = stringKey.join(" ");
//         key in ansDict ? ansDict[key].push(str) : ansDict[key] = [str];
//     }
//     // console.log(ansDict, " ansDict ")
//     return Object.values(ansDict)
// }

// console.log(groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]));