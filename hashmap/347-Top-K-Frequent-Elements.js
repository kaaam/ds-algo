/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

// ****************** sort whole array
// var topKFrequent = function (nums, k) {
//     let _dict = {};
//     let dictToArray = [];

//     for (let digit of nums) {
//         digit in _dict ? _dict[digit] = _dict[digit] + 1 : _dict[digit] = 1;
//     }

//     for (let [key, value] of Object.entries(_dict)) {
//         dictToArray.push([key, value]);
//     }

//     return dictToArray.sort((a, b) => -a[1] + b[1]).slice(0, k).reduce((accu, [key, value]) => {
//         accu.push(key);
//         return accu;
//     }, []);

// };


// // *************** NOT WOKING ******************

// var swap = function (nums, index1, index2) {
//     let temp = nums[index1];
//     nums[index1] = nums[index2];
//     nums[index2] = temp;
//     return nums;

// }



// var sortKelemntsIterative = function (nums, k) {
//     let left = 0;
//     let right = nums.length - 1;

//     let currentPivotIndex;
//     let targetIndex = nums.length - k;

//     //  && (nums.length-k)!=
//     while (left < right) {
//         console.log(left, right, " left, right ")
//         let i = left;
//         let j = right;
//         currentPivotIndex = (left + right) >> 1;
//         let pivot = nums[currentPivotIndex];
//         console.log(nums, "   nums ")
//         console.log(pivot, "   pivot ")
//         console.log(currentPivotIndex, " currentPivotIndex ")

//         swap(nums, currentPivotIndex, right);
//         j--;

//         while (i < j) {
//             if (nums[i] <= pivot && nums[j] > pivot) {
//                 i++; j--;
//             } else if (nums[i] <= pivot) {
//                 i++;
//             } else if (nums[j] > pivot) {
//                 j--;
//             } else {
//                 console.log(nums, " nums before swap", i, j)
//                 swap(nums, i, j);
//             }
//         }

//         console.log(i, j, " i, j ")
//         if (i == j) {
//             if (nums[i] > pivot) {
//                 currentPivotIndex = i;
//                 swap(nums, i, right);
//             } else {
//                 currentPivotIndex = i + 1;
//                 swap(nums, i + 1, right);
//             }
//         } else {
//             currentPivotIndex = i;
//             swap(nums, i, right);
//         }
//         console.log(nums, "   nums AFTER ")


//         console.log(targetIndex == currentPivotIndex, targetIndex, currentPivotIndex)
//         if (targetIndex == currentPivotIndex) {
//             console.log(nums, " before ")
//             console.log(targetIndex, "  targetIndex ")
//             console.log(nums.slice(0, targetIndex), "   nums.slice(0, targetIndex-1) ")
//             return nums.slice(0, targetIndex).sort().concat(nums.slice(targetIndex, nums.length));
//         } else if (targetIndex > currentPivotIndex) {
//             left = currentPivotIndex + 1;
//         } else {
//             right = currentPivotIndex - 1;
//         }

//         console.log(left, right, " END ")

//     }

//     return nums;


// };


// // ****************** quicksearch and sort k elements instead of sorting whole array
// var topKFrequent = function (nums, k) {
//     let _dict = {};
//     let _reverseDict = {};
//     let dictToArray = [];

//     for (let digit of nums) {
//         digit in _dict ? _dict[digit] = _dict[digit] + 1 : _dict[digit] = 1;
//     }

//     for (let [key, value] of Object.entries(_dict)) {
//         dictToArray.push(value);
//         if (value in _reverseDict) {
//             _reverseDict[value].push(key);
//         } else {
//             _reverseDict[value] = [key];

//         }
//     }

//     console.log(_reverseDict, " _reverseDict ")

//     let sortedList = sortKelemntsIterative(dictToArray, k);
//     console.log(sortedList, " sortedList ")

//     let ans = [];

//     let visitedSet = new Set();
//     let sortedListIndex = sortedList.length - 1;
//     while (ans.length < k) {
//         console.log(sortedListIndex, sortedList[sortedListIndex], visitedSet)
//         if (!visitedSet.has(sortedListIndex)) {
//             visitedSet.add(sortedListIndex);
//             ans = [...ans, ..._reverseDict[sortedList[sortedListIndex]]];
//             console.log(ans, " ans ")
//         }
//         sortedListIndex--;
//     }
//     return ans;

//     // return dictToArray.sort((a, b) => -a[1] + b[1]).slice(0, k).reduce((accu, [key, value]) => {
//     //     accu.push(key);
//     //     return accu;
//     // }, []);

// };

// // console.log(topKFrequent([1, 1, 1, 2, 2, 3], 2));
// // console.log(topKFrequent([4,1,-1,2,-1,2,3], 2));

// console.log(topKFrequent([3,2,3,1,2,4,5,5,6,7,7,8,2,3,1,1,1,10,11,5,6,2,4,7,8,5,6], 10));


// // console.log(topKFrequent([-5, -9, 67, -10, 4, -57, 47, 13, -67, -26, -57, 63, 38, -68, 62, -45, -37, 95, 49, -91, -53, -42, -33, 80, 78, -30, -36, 22, 9, -86, 79, -1, 44, -92, 30, -68, -94, 58, -51, -26, -38, 5, -74, 25, 71, -93, 52, -12, -86, 7, -86, 53, 78, -31, -5, -87, 88, -98, -39, 9, 99, 23, 96, -90, 51, -64, 35, -73, 9, 60, -78, 70, 99, 14, 70, 71, -78, 50, 7, 46, -89, 57, -1, 87, -87, -95, 48, 49, 79, -54, 31, 28, -27, 75, 31, -76, -12, 35, 40, -90, -60, -60, -7, 67, 73, -34, -42, -35, 61, 51, 18, 95, 16, 78, -81, -91, -30, 92, 57, -79, 5, 41, 29, 72, -62, -47, 80, 29, 1, -21, -36, 5, 82, 4, -12, -52, -56, -47, -68, 95, 85, -87, -7, -12, 98, 75, -64, -93, 11, 73, -81, -9, -12, -9, 51, 17, -94, 33, -9, 57, -35, 10, -17, 87, -18, -55, -52, 30, -62, 73, 35, -74, -47, -63, 77, -72, -55, 5, 73, 21, 14, 7, -65, -51, -55, -49, 98, -20, -22, -68, 34, -20, 92, 55, 47, -20, 6, -54, -12, 3, 75, 69, 60, 15, 88, 64, 2, -27, -50, 55, 73, 46, -15, -64, 93, -47, -75, -55, -75, 21, -57, 91, -12, -99, -68, -56, -14, -4, -77, -94, 55, 93, -31, 68, -12, -23, 59, -56, -86, 43, 83, -93, -78, -11, -7, 96, -3, -87, -37, 19, -78, 67, -29, 77, -28, 91, -73, -68, -22, 18, -7, 3, 15, 77, 99, 31, -48, -86, -45, -82, 52, -39, 8, -88, -83, -58, -77, 5, 87, -61, 50, 32, -66, -36, 60, -53, 52, 70, -36, -1, 83, -56, 33, 98, -80, 28, 1, -21, -50, -60, 44, 99, 18, 83, -29, 83, -36, -55, -6, 96, -60, 61, 75, 6, -57, 2, 82, 62, -27, 36, 60, 72, 92, 61, -65, 79, -57, -34, -23, -28, -55, 53, 36, -80, 5, -76, 64, -81, -32, -43, -1, 50, -16, -72, -74, 22, 88, 28, -79, -99, 85, -13, -34, -76, 85, 6, 21, -99, 10, -46, 79, 11, -70, 17, 47, -22, -62, 0, 6, 75, -19, 57, -25, -52, -83, 90, 21, 95, 52, 68, 47, -12, 76, -9, -65, 86, 90, 16, 74, 64, 26, 84, 64, -42, 97, -72, 53, -76, 11, 89, -62, 67, 100, 15, 53, 68, -16, 24, 11, -77, 20, 59, -95, -50, -20, 27, 45, 94, 13, -93, 86, 49, 12, 19, 17, -33, -52, -28, 71, 79, -19, -73, 40, -99, 83, 77, 19, -20, 98, 86, -5, -5, 73, 18, 100, 73, -45, 33, 3, 89, 32, -53, 73, 16, -3, -26, -80, 49, -78, 67, 31, 1, -85, -44, -91, -68, 75, -74, 95, 23, 89, 99, -84, 54, -93, 68, 0, -41, 66, -15, -27, -23, -9, -68, 37, 45, -69, 57, 80, 10, -64, 35, 26, 55, -67, 31, -76, 36, -99, 21], 7))




// /**
//  * @param {number[]} nums
//  * @param {number} k
//  * @return {number[]}
//  */
// var topKFrequent = function (nums, k) {
//     let numFrequency = {};
//     let maxFrequency = Number.MIN_SAFE_INTEGER;

//     for (let _num of nums) {
//         if (numFrequency[_num]) numFrequency[_num] += 1;
//         else numFrequency[_num] = 1;

//         if (maxFrequency < numFrequency[_num]) maxFrequency = numFrequency[_num];
//     }

//     let frequencyArray = new Array(maxFrequency + 1).fill(null);
//     let ans = [];
//     // console.log(numFrequency, maxFrequency)

//     for (let [key, value] of Object.entries(numFrequency)) {
//         if (frequencyArray[value] == null) {
//             frequencyArray[value] = [key];
//         } else {
//             frequencyArray[value].push(key)
//         }
//     }
//     // console.log(frequencyArray)

//     for (let i = frequencyArray.length - 1; i >= 0; i--) {
//         if (ans.length < k) {
//             if (frequencyArray[i] != null) {
//                 ans = [...ans, ...frequencyArray[i]]
//                 // console.log(ans)
//             }
//         } else { break; }
//     }
//     return ans;
// };

// // console.log(topKFrequent([3,2,3,1,2,4,5,5,6,7,7,8,2,3,1,1,1,10,11,5,6,2,4,7,8,5,6], 3));
// // console.log(topKFrequent([5, 3, 1, 1, 1, 3, 73, 1], 2));
// console.log(topKFrequent([-1, -1], 1));
















// ************************************ MAX heap O(NLOGN)
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

var swap = function (nums, i1, i2) {
    let temp = nums[i1];
    nums[i1] = nums[i2];
    nums[i2] = temp;
}

var constructHeap = function (nums) {
    let numberOfSwaps = 0;
    while (1) {
        // console.log(nums, " numsconstructHeap ")
        for (let i = nums.length - 1; i >= 0; i--) {
            // let parentIndex = (i-1)>>1;
            let leftChildIndex = (i * 2) + 1;
            let rightChildIndex = (i * 2) + 2;

            // console.log(i, leftChildIndex, rightChildIndex, " i, leftChildIndex, rightChildIndex ")

            if (nums[leftChildIndex] != undefined) {
                // console.log(nums[i][0] < nums[leftChildIndex][0], nums[i][0], nums[leftChildIndex][0], "first if")
                if (nums[i][0] < nums[leftChildIndex][0]) {
                    swap(nums, leftChildIndex, i);
                    numberOfSwaps += 1;
                }
            }
            // console.log(nums, " numsconstructHeap ")

            if (nums[rightChildIndex] != undefined) {
                // console.log(nums[i][0] < nums[rightChildIndex][0], nums[i][0], nums[rightChildIndex][0], "seconnd if")
                if (nums[i][0] < nums[rightChildIndex][0]) {
                    swap(nums, rightChildIndex, i);
                    numberOfSwaps += 1;
                }
            }
            // console.log(nums, " numsconstructHeap ")
        }
        if (numberOfSwaps == 0) return;
        numberOfSwaps = 0;
    }
    // console.log(nums, " numsconstructHeap ")
}

var takeMax = function (nums) {
    let max = nums[0];
    nums[0] = nums[nums.length - 1];
    nums.length -= 1;

    let i = 0;
    let leftChildIndex = (i * 2) + 1;
    let rightChildIndex = (i * 2) + 2;

    // console.log(nums[i], nums[i] != undefined)
    while (nums[i] != undefined && ((nums[i][0] < (nums[leftChildIndex] != undefined ? nums[leftChildIndex][0] : -1)) || (nums[i][0] < (nums[rightChildIndex] != undefined ? nums[rightChildIndex][0] : -1)))) {
        let swapIndex = (nums[leftChildIndex] ? nums[leftChildIndex][0] : -1) > (nums[rightChildIndex] ? nums[rightChildIndex][0] : -1) ? leftChildIndex : rightChildIndex;
        swap(nums, swapIndex, i);
        i = swapIndex;
        leftChildIndex = (i * 2) + 1;
        rightChildIndex = (i * 2) + 2;
    }

    return max;

}


var topKFrequent = function (nums, k) {
    let numFrequency = new Map();
    // let reverseNumFrequency = new Map();
    let frequencyArray = [];
    let ans = [];

    for (let _num of nums) {
        if (numFrequency.has(_num)) numFrequency.set(_num, numFrequency.get(_num) + 1);
        else numFrequency.set(_num, 1);
    }

    for (const [key, value] of numFrequency) {
        frequencyArray.push([value, key]);
        // if (reverseNumFrequency.has(value)) reverseNumFrequency.set(value, [...reverseNumFrequency.get(value), key]);
        // else reverseNumFrequency.set(value, [key]);
    }

    // console.log(numFrequency, reverseNumFrequency)
    // console.log(numFrequency)

    // console.log(numFrequency, " numFrequency ")
    constructHeap(frequencyArray);
    // console.log(frequencyArray, " frequencyArray ")

    for (let i = 0; i < k; i++) {
        // ans = [...ans, ...reverseNumFrequency.get(takeMax(frequencyArray))]
        // console.log(frequencyArray, " --- ");
        ans.push(takeMax(frequencyArray)[1])
    }

    return ans;

};


// console.log(topKFrequent([3,2,3,1,2,4,5,5,6,7,7,8,2,3,1,1,1,10,11,5,6,2,4,7,8,5,6], 3));
// console.log(topKFrequent([5, 3, 1, 1, 1, 3, 73, 1], 2));
// console.log(topKFrequent([-1, -1], 1));
// console.log(topKFrequent([-1, 2, 3, 2, 3], 1));

// console.log(topKFrequent([1, 1, 1, 2, 2, 3], 2));

console.log(topKFrequent([2, 3, 4, 1, 4, 0, 4, -1, -2, -1], 2));
