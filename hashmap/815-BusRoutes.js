/**
 * @param {number[][]} routes
 * @param {number} source
 * @param {number} target
 * @return {number}
 */
var numBusesToDestination = function (routes, source, target) {
    let stationToIndex = {};
    for (let i = 0; i < routes.length; i++) {
        for (let j = 0; j < routes[i].length; j++) {
            if (routes[i][j] in stationToIndex) {
                stationToIndex[routes[i][j]].push(i);
            } else {
                stationToIndex[routes[i][j]] = [i];
            }
        }
    }

    let startIndex = stationToIndex[source];
    let endIndex = stationToIndex[target];

    let indexCovered = {};
    let indexList = []; // [0, 5]
    for(let _index of startIndex){
        indexCovered[_index] = 0;
        indexList.push(_index)
    }
    let jump = 1;

    
    let debug = 0;
    let currIndex = 0;
    while(currIndex <= indexList.length-1){
        // let currentStops = routes[currIndex];
        
        // currIndex 0 5


        // routes[currIndex] // [1,2,7] [1, 100]
        for(let _station of routes[currIndex]){
            // _station 1
            // _station 2
            // _station 7

            if(_station == source){continue;} // 1

            // stationToIndex[_station] [0, 4] for 2
            for(let _index of stationToIndex[_station]){
                if(_index in indexCovered){
                    if(indexCovered[_index] > jump){indexCovered[_index] = jump;}
                }else{
                    console.log(currIndex, _index, indexCovered, '----')
                    indexCovered[_index] = indexCovered[currIndex]+1;
                    indexList.push(_index)
                }
                console.log(indexCovered)
            }
            debug++;
            if(debug>40) return;

        }
        console.log(currIndex, indexList)
        currIndex++;
    }

    



};

console.log(numBusesToDestination([[1, 2, 7], [3, 12, 7], [9, 10, 11, 6, 3], [6, 30], [30, 2], [1, 100]], 1, 6));