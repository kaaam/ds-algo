/**
 * @param {number[]} arr
 * @param {number} target
 * @return {number}
 */

 var twosum = function(arr, target, i){
    let count = 0;
    
    let _dict = {};
    
    if(target<0) return 0;
    
    for(let j=i+1;j<arr.length;j++){
        
        if(arr[j] > target) break;

        if((target-arr[j]) in _dict){
            count += _dict[target-arr[j]];
        }

        if(arr[j] in _dict){
             _dict[arr[j]] += 1;
        }else{
            _dict[arr[j]] = 1;
        }
    }
    // console.log(_dict, target, count)
    
    // return count%1000000007;
    return count;
    
}

var threeSumMulti = function(arr, target) {
    
    arr.sort((a, b)=> a-b);
    
    let count=0;
    for(let i=0;i<arr.length;i++){
        if(arr[i] > target) break;
        // console.log(arr[i])
        count += twosum(arr, target-arr[i], i);
        count = count%1000000007;
    }
    return count;
};