/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */

 var _lowestCommonAncestor = function(root, p, q) {
    let leftReturn;
    let rightReturn;
    if(!root) return null;
    // if(root.val == p.val || root.val == q.val) {
    //     leftReturn = _lowestCommonAncestor(root.left, p, q);
    //     rightReturn = _lowestCommonAncestor(root.right, p, q);
    //     // console.log(root.val, p.val, q.val)
    //     // console.log(leftReturn, ' leftReturn ')
    //     // console.log(rightReturn, " rightReturn ")
    //     // console.log('\n')
    //     if(leftReturn || rightReturn){ 
    //         // console.log(" FIRST ")
    //         root.count_child=2;
    //         return root; 
    //     }
    //     // console.log(" RETURN ROOT ", root.val)
    //     return root;
    // }

    // if(root.val == q.val) return root;
    
    leftReturn = _lowestCommonAncestor(root.left, p, q);
    rightReturn = _lowestCommonAncestor(root.right, p, q);
    
    if(leftReturn && rightReturn) {
        // console.log(" SECOND ")
        root.count_child=2;
        return root;
    };
    
    if(root.val == p.val || root.val == q.val) {
        if(leftReturn || rightReturn){ 
            root.count_child=2;
            return root; 
        }
        return root
    }
    
    // console.log(root.val, " root.val ")
    // console.log(leftReturn, " leftReturn ")
    // console.log(rightReturn, " rightReturn ")
    if(leftReturn) return leftReturn;
    if(rightReturn) return rightReturn;
};

var lowestCommonAncestor = function(root, p, q) {
    if(!q) q={val:Math.pow(10, 9)+1}
    if(!p) p={val:Math.pow(10, 9)+1}
    // console.log(root, " root paremt !!!", root.val, p.val, q.val)
    let returnVal = _lowestCommonAncestor(root, p, q);
    // console.log(returnVal.val, " returnVal ", returnVal.count_child)
    if(returnVal.count_child == 2) return returnVal;
    else return null;
};