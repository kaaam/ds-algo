

// ************************************************* working but not good


/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */

// var findAnswer = function(listToItearate, setToLook){
//     // console.log(listToItearate, setToLook, " findAnswer ")
//     for(let i=listToItearate.length-1;i>=0;i--){
//         // console.log(setToLook, listToItearate[i])
//         if(setToLook.has(listToItearate[i])){
//             // console.log("!!!!!!! ", listToItearate[i])
//             return listToItearate[i];
//         }
//     }
    
// }

// var dfs = function(current, target, parentList, parentSet){
    
//     if(!current) return null;
    
//     if(current.val == target.val){
//         // return [parentList, parentSet];
//         return [[...parentList, current.val], new Set([...parentList, current.val])]
//     }else{
//         let rightReturn = dfs(current.right, target, [...parentList, current.val], new Set([...parentList, current.val]));
//         if(rightReturn){ return rightReturn;}
//         let leftReturn = dfs(current.left, target, [...parentList, current.val], new Set([...parentList, current.val]))
//         if(leftReturn){ return leftReturn;}
//     }
// }

// var lowestCommonAncestor = function(root, p, q) {
//     let [firstParentList, firstParentSet] = dfs(root, p, [], new Set());
//     let [secondParentList, secondParentSet] = dfs(root, q, [], new Set());
    
//     // console.log(firstParentList)
//     // console.log(secondParentList)
//     // console.log(firstParentSet)
//     // console.log(secondParentSet)
//     // return ;
    
//     let result;
//     if(firstParentList.length<secondParentList.length){
//         result =  findAnswer(firstParentList, secondParentSet);
//     }else{
//         result = findAnswer(secondParentList, firstParentSet);
//     }
//     return {val: result};
// };



// ************************************************* copied answer
function lowestCommonAncestor(root, p, q) {
    if (!root || root === p || root === q) return root;
    var resL = lowestCommonAncestor(root.left, p, q);
    var resR = lowestCommonAncestor(root.right, p, q);
    return (resL && resR) ? root : (resL || resR);
}
  
  

