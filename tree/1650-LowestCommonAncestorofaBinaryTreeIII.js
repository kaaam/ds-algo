/**
 * // Definition for a Node.
 * function Node(val) {
 *    this.val = val;
 *    this.left = null;
 *    this.right = null;
 *    this.parent = null;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */

// ************ works fine

 var traverseUp = function(node){
    if(!node) return;
    
    if(node.have_reached_before){
        return node;
    }else{
        node.have_reached_before = true;
        return traverseUp(node.parent);
    }
}

var lowestCommonAncestor = function(p, q) {
    traverseUp(p);
    // let commonNode = traverseUp(q);
    // console.log(commonNode)
    return traverseUp(q);;
};




// ************ works fine, copied solution, very smart solution

/**
 * // Definition for a Node.
 * function Node(val) {
 *    this.val = val;
 *    this.left = null;
 *    this.right = null;
 *    this.parent = null;
 * };
 */

/**
 * @param {Node} node
 * @return {Node}
 */
 var lowestCommonAncestor = function(p, q) {
    let first = p;
    let second = q;
    while(first.val != second.val){
        first = first.parent?first.parent:q;
        second = second.parent?second.parent:p;
    }
    // console.log(first.val)
    // console.log(second.val)
    return first;
};