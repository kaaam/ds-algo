/**
 * @param {number[][]} trips
 * @param {number} capacity
 * @return {boolean}
 */


// // ********************** O(N) - more space
// var carPooling = function (trips, capacity) {

//     let maxTo = Number.MIN_SAFE_INTEGER;
//     for (let _trip of trips) {
//         if (_trip[2] > maxTo) maxTo = _trip[2];
//     }

//     let journeyArray = new Array(maxTo + 1).fill(0);

//     for (let _trip of trips) {
//         // if(_trip[2]> maxTo) maxTo = _trip[2];
//         // journeyArray
//         for (let i = _trip[1]; i < _trip[2]; i++) {
//             journeyArray[i] += _trip[0];
//             if (journeyArray[i] > capacity) return false;
//         }
//     }
//     return true;

// };

// // console.log(carPooling([[2, 1, 5], [3, 3, 7]], 4));
// // console.log(carPooling([[1, 1, 5], [3, 3, 7]], 4));

// console.log(carPooling([[2, 1, 5], [3, 5, 7]], 3));




// ********************** sorting
var carPooling = function (trips, capacity) {

    let startArray = [];
    let endArray = [];

    for (let _trip of trips) {
        startArray.push([_trip[1], _trip[0]]);
        endArray.push([_trip[2], (-1 * _trip[0])]);
    }

    startArray.sort((a, b) => a[0] - b[0]);
    endArray.sort((a, b) => a[0] - b[0]);

    let currentNumberOfPeople = 0;
    // console.log(startArray)
    // console.log(endArray)

    let i = 0; let j = 0;

    while (i < startArray.length && j < startArray.length) {
        // console.log(i, j, " i, j ")
        // console.log(currentNumberOfPeople, " console.log(currentNumberOfPeople)")
        if (startArray[i][0] < endArray[j][0]) {
            currentNumberOfPeople += startArray[i][1];
            i++;
        } else if (startArray[i][0] == endArray[j][0]) {
            currentNumberOfPeople += endArray[j][1];
            currentNumberOfPeople += startArray[i][1];
            i++; j++;
        } else {
            currentNumberOfPeople += endArray[j][1];
            j++;
        }
        // console.log(currentNumberOfPeople, " console.log(currentNumberOfPeople)")

        if (currentNumberOfPeople > capacity) return false;
    }
    return true;

};

// console.log(carPooling([[2, 1, 5], [10, 3, 7]], 4));
// console.log(carPooling([[1, 1, 5], [3, 3, 7]], 4));

// console.log(carPooling([[2, 1, 5], [3, 5, 7]], 3));
console.log(carPooling([[2, 1, 5], [1, 2, 3], [3, 5, 7]], 3));