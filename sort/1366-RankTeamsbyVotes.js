/**
 * @param {string[]} votes
 * @return {string}
 */
var rankTeams = function (votes) {
    let _dict = {};
    // let scoreDict = {};
    for (let vote of votes) {
        // let score = 1;
        for (let i = vote.length - 1; i >= 0; i--) {
            // vote[i] in scoreDict ? scoreDict[vote[i]] += score : scoreDict[vote[i]] = score;
            // score = vote.length * 10;
            vote[i] in _dict ? i in _dict[vote[i]] ? _dict[vote[i]][i] += 1 : _dict[vote[i]][i] = 1 : _dict[vote[i]] = { [i]: 1 }
        }
    }
    // console.log(_dict, " _dict ")
    // console.log(scoreDict, " scoreDict ")
    // let letters = Object.keys(scoreDict);
    // letters.sort((a, b) => scoreDict[b] - scoreDict[a])
    // return letters.join('');

    let x = [...votes[0]];
    x.sort((a, b) => {
        // console.log('\n\n')
        // console.log(a, b)
        for (let i = 0; i < x.length; i++) {
            if (i in _dict[a] && i in _dict[b]) {
                if (_dict[a][i] != _dict[b][i]) {
                    // console.log('1')
                    return _dict[b][i] - _dict[a][i];
                }
            } else if (i in _dict[a]) {
                // console.log('2')
                return -1;
            } else if (i in _dict[b]) {
                // console.log('3')
                return 1;
            }
        }
        return  a.toString().charCodeAt(0) - b.toString().charCodeAt(0);
    })
    // console.log(x)
    return x.join('');
};

// console.log(rankTeams(["ABC", "ACB", "ABC", "ACB", "ACB"]));
// console.log(rankTeams(["ABC", "ACB", "ABC", "ACB", "ACB"]));
// console.log(rankTeams(["WXYZ", "XYZW"]));
// console.log(rankTeams(["ZMNAGUEDSJYLBOPHRQICWFXTVK"]));
console.log(rankTeams(["BCA","CAB","CBA","ABC","ACB","BAC"]));

