/**
 * @param {number[][]} points
 * @return {number}
 */
// var findMinArrowShots = function (points) {
//     let maxEnd = -1;
//     let minStart = Number.MIN_SAFE_INTEGER;

//     points.sort((a, b) => {
//         if (a[1] > maxEnd) maxEnd = a[1];
//         if (b[1] > maxEnd) maxEnd = b[1];
//         if (a[0] > minStart) minStart = a[0];
//         if (b[0] > minStart) minStart = b[0];
//         return a[0] == b[0] ? ((a[1] - a[0]) - (b[1] - b[0])) : a[0] - b[0]
//     });

//     console.log(points, " points ")

//     let dpArray = new Array(maxEnd+1).fill(0);

//     for(let i=minStart;i<=maxEnd;i++){
//         // push countOfBall, leftReach, rightReach, list
//     }


// };



// // ********************** sort by ending (greey) O(NlogN)
// var findMinArrowShots = function (points) {

//     points.sort((a, b) => {
//         return a[1] - b[1];
//     });

//     // console.log(points, " points ")
//     let dartsCount = 1;

//     // let dpArray = new Array(maxEnd+1).fill(0);

//     let curr = points[0];
//     for(let i=1;i<points.length;i++){
//         // push countOfBall, leftReach, rightReach, list
//         if(points[i][0]<=curr[1]){

//         }else{
//             dartsCount++;
//             curr = points[i];

//         }
//     }
//     return dartsCount;


// };

// // console.log(findMinArrowShots([[10, 16], [2, 8], [1, 6], [7, 12], [1, 2]]));
// console.log(findMinArrowShots([[0, 3], [1, 4], [1, 9], [3, 6], [3, 5], [3, 4], [3, 9], [4, 5], [5, 6], [5, 7], [6, 7], [8, 9]]));




// **************** sort by start approch
// *************************************  NOT JAVASCRIPT
// C++
// sort based on the the start of ballon

// class Solution {
// public:
//     int findMinArrowShots(vector<vector<int>>& points) {
//         if (points.empty()) return 0;
//         sort(points.begin(), points.end());
//         int start = points[0][0], end = points[0][1];
		
// 		// the reason res = 1 is that we need an arrow to destroy the last group. 
//         int res = 1;
//         for (auto point: points)
//         {
//             if (point[0] <= end)
//             {
//                 start = max(start, point[0]);
//                 end = min(end, point[1]);
//             }
//             else
//             {
//                 res++;
//                 start = point[0];
//                 end = point[1];
//             }
//         }
//         return res;
//     }
// };