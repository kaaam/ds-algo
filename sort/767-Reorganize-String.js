// /**
//  * @param {string} s
//  * @return {string}
//  */
// var reorganizeString = function (s) {
//     let frequencyDict = {}
//     for (let _s of s) {
//         _s in frequencyDict ? frequencyDict[_s] += 1 : frequencyDict[_s] = 1;
//     }

//     let uniqueLetters = Object.keys(frequencyDict);
//     // for(let key of Object.keys(frequencyDict)){

//     // }

//     uniqueLetters.sort((a,b)=>{
//         return frequencyDict[b]-frequencyDict[a];
//     })
    
//     let maxCount = frequencyDict[uniqueLetters[0]];
//     let elseCount = 0;
//     for(let i=1; i<uniqueLetters.length;i++){
//         elseCount+=frequencyDict[uniqueLetters[i]];
//     }

//     if(elseCount<maxCount-1){
//         return "";
//     }

//     let ans = ``;

//     for(let i=0;i<frequencyDict[uniqueLetters[0]];i++){
//         // for(let j=1;j<frequencyDict[uniqueLetters[0]];j++){
//         //     ans += 
//         // }

//         for(let j=1; j<uniqueLetters.length;j++){
//             for(let k=0;k<frequencyDict[uniqueLetters[j]];k++){
//                 console.log(i, j, k, uniqueLetters[0], uniqueLetters[j])
//             }
//         }
//     }



// };


// console.log(reorganizeString("aaaabbbccd"))
// // console.log(reorganizeString("aaaabcdefg"))





/**
 * @param {string} s
 * @return {string}
 */
var reorganizeString = function(s) {
    
    let ans = ``;
    let sArray = s.split('');
    let maxFreq = 0;
    // let minFreq = Number.MAX_SAFE_INTEGER;
    
    let freqDict = sArray.reduce((a, c)=>{
        // console.log(c)
        if(c in a) a[c]++;
        else a[c] = 1;
        if(a[c]>maxFreq) maxFreq = a[c];
        // if(a[c]<minFreq) minFreq = a[c];
        return a;
    }, {});
    // console.log(freqDict)
    
    let freqArray = new Array(maxFreq+1).fill(null);
    
    
    let minFreq = Number.MAX_SAFE_INTEGER;
    for(let [key, val] of Object.entries(freqDict)){
        if(val<minFreq) minFreq = val;
        if(freqArray[val] == null){
            freqArray[val] = [key];
        }else{
            freqArray[val].push(key);
        }
    }
    
    // console.log(freqArray, " freqArray ")
    
    // let maxCovered = false;
    // let maxCoveredIndex = 1;
    // let maxCoveredCurrCount = 0;
    // let maxCoveredCount = freqArray.length-1-1;
    let insertIndex=0;

    for(let i=0;i<freqArray[freqArray.length-1].length;i++){
        insertIndex=0;
        for(let j=0;j<freqArray.length-1;j++){
            if(i==0){
                ans += freqArray[freqArray.length-1][i];
            }else{
                ans = ans.slice(0, insertIndex) + freqArray[freqArray.length-1][i] + ans.slice(insertIndex);
                insertIndex += 2;
            }
            // console.log(ans)
        }
    }
    
    insertIndex=1;
    
    for(let i=freqArray.length-2;i>=minFreq;i--){
        if(freqArray[i] != null){
            for(let _s of freqArray[i]){
                // let insertIndex=1;
                for(let j=0;j<i;j++){
                    ans = ans.slice(0, insertIndex) + _s + ans.slice(insertIndex);
                    // console.log(ans)
                    // maxCoveredCount--;
                    // console.log(insertIndex)
                    insertIndex += 2;
                    // console.log(insertIndex)
                    insertIndex = insertIndex%(ans.length);
                    // console.log(i, freqArray.length-1, " !!! ")
                    if(insertIndex == 0 && i<freqArray.length-1) insertIndex = 1;
                    // console.log(insertIndex, ans.length)
                }
            }
        }
    }
    
    // console.log(ans)
    // console.log('\n\n')
    
    // TODO: instead of this check mathemcatically.
    for(let i=0;i<ans.length-1;i++){
        if(ans[i] == ans[i+1]) return "";
    }
    
    return ans;
    
    
    
};


console.log(reorganizeString("aaaaaabbbbbbcccccc"))
console.log(reorganizeString("aab"))
console.log(reorganizeString("aaaaaaabcdefghijk"))
console.log(reorganizeString("aaaaaaabcdefg"))