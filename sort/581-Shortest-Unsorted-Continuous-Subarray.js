/**
 * @param {number[]} nums
 * @return {number}
 */



// // ************ O(N*log(N))
// var findUnsortedSubarray = function (nums) {

//     // for (let i = 1; i < nums.length; i++) {

//     // }
//     let origionalNums = [...nums];
//     nums.sort((a, b) => a - b);

//     let first;
//     let last;

//     for (let i = 0; i < nums.length; i++) {
//         // console.log(nums[i], origionalNums[i])
//         if (nums[i] != origionalNums[i]) {
//             if (first==undefined) first = i;
//             last = i;
//         }
//     }
//     // console.log(first, last, " first, last ")

//     if (first!=undefined && last!=undefined) {
//         return last - first + 1;
//     }else{
//         return 0;
//     }


// };


// // console.log(findUnsortedSubarray([2, 6, 4, 8, 10, 9, 15]));
// // console.log(findUnsortedSubarray([2, 6, 4]));
// // console.log(findUnsortedSubarray([2, 6, 4]));
// // console.log(findUnsortedSubarray([2]));
// // console.log(findUnsortedSubarray([1, 2, 3, 4, 5, 6]));
// console.log(findUnsortedSubarray([1, 2, 3, 4, 5, 6, 0]));






// // ************ O(N) - first 
// var findUnsortedSubarray = function (nums) {

//     let minValue = Number.MAX_SAFE_INTEGER;
//     let minValueIndex;
//     let maxValue = Number.MIN_SAFE_INTEGER;
//     let maxValueIndex;

//     for (let i = 1; i < nums.length; i++) {
//         if (nums[i] < nums[i - 1]) {
//             if (nums[i] < minValue) {
//                 minValue = nums[i];
//                 minValueIndex = i;
//             }
//             if (nums[i-1] >= maxValue) {
//                 maxValue = nums[i-1];
//                 maxValueIndex = i-1;
//             }
//         }
//     }

//     // for (let i = nums.length - 2; i >= 0; i--) {
//     //     if (nums[i] > nums[i + 1]) {
//     //         if (nums[i] > maxValue) {
//     //             maxValue = nums[i];
//     //             maxValueIndex = i;
//     //         }
//     //     }
//     // }

//     console.log(minValueIndex, maxValueIndex)

//     let leftIndex;
//     if (minValueIndex == undefined) {
//         leftIndex = nums.length - 1;
//     } else {
//         for (let i = minValueIndex - 1; i >= 0; i--) {
//             if (minValue >= nums[i]) {
//                 break;
//             }
//             leftIndex = i;
//         }
//     }
//     // console.log(leftIndex)


//     let rightIndex;
//     if (maxValueIndex == undefined) {
//         rightIndex = 0;
//     } else {
//         for (let i = maxValueIndex + 1; i < nums.length; i++) {
//             if (maxValue <= nums[i]) {
//                 break;
//             }
//             rightIndex = i;
//         }
//     }

//     console.log(leftIndex, rightIndex)
//     // console.log('\n\n')
//     if (leftIndex < rightIndex) {
//         return rightIndex - leftIndex + 1;
//     }
//     return 0;



// };


// // console.log(findUnsortedSubarray([2, 6, 4, 8, 10, 9, 15]));
// // console.log(findUnsortedSubarray([2, 6, 4]));
// // console.log(findUnsortedSubarray([2, 6, 4]));
// // console.log(findUnsortedSubarray([2]));
// // console.log(findUnsortedSubarray([1, 2, 3, 4, 5, 6]));
// // console.log(findUnsortedSubarray([1, 2, 3, 4, 5, 6, 0]));
// // console.log(findUnsortedSubarray([1, 3, 2, 3, 3]));
// console.log(findUnsortedSubarray([3,2,3,2,4]));




// *************************** someone else's code - elegant
var findUnsortedSubarray = function (N) {
    console.log(JSON.stringify(N))
    console.log('\n')
    let len = N.length - 1, left = -1, right = -1,
        max = N[0], min = N[len]
    for (let i = 1; i <= len; i++) {
        let a = N[i], b = N[len - i]
        console.log(a, max, " a, max ")
        a < max ? right = i : max = a
        console.log(b, min, " b, min ")
        b > min ? left = i : min = b
        console.log(i, a, b, " i, a, b,  ")
        console.log(left, right, max, min, " left, right, max, min ")
        console.log(N[left], N[right], " N[left], N[right] ")
        console.log('\n')
    }
    console.log(left - len, right)
    return Math.max(0, left + right - len + 1)
};


// console.log(findUnsortedSubarray([11, 12, 13, 14, 15, 16, 17, 10, 18, 19, 111, 110, 112, 113, 114, 115, 116]));
// console.log(findUnsortedSubarray([2, 3, 4, 5, 6, 12, 13, 14, 15, 16, 1, 17, 18]));

// console.log(findUnsortedSubarray([2, 3, 4, 5, 6, 12, 13, 14, 15, 16, 100, 17, 18]));
// console.log(findUnsortedSubarray([2, 3, 4, 5, 6, 12, 15, 13, 14, 16, 100, 17, 18]));
// console.log(findUnsortedSubarray([2, 3, 4, 5, 6, 7, 8, 13, 12, 14, 15, 16, 17, 18]));

console.log(findUnsortedSubarray([2, 3, 4, 0, 5, 6, 12, 13, 14, 15, 16, 100, 17, 18]));