/**
 * @param {number[]} people
 * @param {number} limit
 * @return {number}
 */
// var numRescueBoats = function (people, limit) {
//     let weightFrequency = {};
//     let distinctWeights = new Set(people);
//     for (let _people of people) {
//         if (_people in weightFrequency) {
//             weightFrequency[_people] += 1;
//         } else {
//             weightFrequency[_people] = 1;
//         }
//     }

//     let boatsCount = 0;

//     // console.log(weightFrequency, distinctWeights, " weightFrequency, distinctWeights ");


//     // for (let _weight of [...distinctWeights].sort((a, b) => a - b)) {
//     //     if (_weight == limit) {
//     //         boatsCount += weightFrequency[_weight];
//     //     } else if (_weight * 2 == limit) {
//     //         boatsCount += weightFrequency[_weight];
//     //     } else {

//     //     }
//     // }

//     let weightsSorted = [...distinctWeights].sort((a, b) => a - b);
//     let left = 0; let right = weightsSorted.length - 1;

//     while (left <= right) {
//         // console.log(weightsSorted[right], weightsSorted[left], weightsSorted, boatsCount);
//         // if(left == right) boatsCount += weightFrequency[weightsSorted[right]];
//         if (((weightsSorted[right] + weightsSorted[left]) > limit)) {
//             boatsCount += weightFrequency[weightsSorted[right]];
//             right--;
//         } else {
//             let toAdd = left == right ? ((weightFrequency[weightsSorted[right]] >> 1) + (weightFrequency[weightsSorted[right]] % 2)) : (Math.min(weightFrequency[weightsSorted[right]], weightFrequency[weightsSorted[left]]));
//             boatsCount += toAdd;
//             weightFrequency[weightsSorted[right]] -= toAdd;
//             weightFrequency[weightsSorted[left]] -= toAdd;

//             if (weightFrequency[weightsSorted[right]] <= 0) {
//                 right--;
//             }
//             if (weightFrequency[weightsSorted[left]] <= 0) {
//                 left++;
//             }
//         }
//     }
//     return boatsCount;

// };

// console.log(numRescueBoats([3, 2, 2, 1], 3));
// console.log(numRescueBoats([4, 4, 4, 1, 7], 8));




var numRescueBoats = function (people, limit) {
    people = people.sort((a, b) => a - b);
    let count = 0;

    let left = 0; let right = people.length - 1;

    while (left < right) {
        if (((people[right] + people[left]) > limit)) {
            count++;
            right--;
        } else {
            count++;
            right--;
            left++;
        }
    }

    if (left == right) count++;

    return count;

}

console.log(numRescueBoats([4, 4, 4, 1, 7], 8));