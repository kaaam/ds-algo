/**
 * @param {string[]} words
 * @param {number} k
 * @return {string[]}
 */

// TODO: implement one with Map


// // *************************** using {} dict, should have used map because order is garenteed
//  var topKFrequent = function (words, k) {
    
//     // This works because the order is preserved in dict
//     words.sort((a, b) => a.localeCompare(b));

//     let frequencyDict = {};
//     let mostFrequent = 0;
//     let ans = [];

//     for (let _word of words) {
//         if (_word in frequencyDict) {
//             frequencyDict[_word] += 1;
//         } else {
//             frequencyDict[_word] = 1;
//         }
//         if (mostFrequent < frequencyDict[_word]) mostFrequent = frequencyDict[_word];
//     }
//     // console.log(frequencyDict, "  frequencyDict  ")
//     // console.log(mostFrequent, " mostFrequent ")

//     let frequencyArray = new Array(mostFrequent + 1).fill(0);

//     for (let [key, value] of Object.entries(frequencyDict)) {
//         // console.log(frequencyArray, value)
//         if (frequencyArray[value] == 0) {
//             frequencyArray[value] = [key];
//         } else {
//             frequencyArray[value].push(key);
//         }
//     }

//     // console.log(frequencyArray, "  frequencyArray  ")

//     for (let i = frequencyArray.length - 1; i >= 0; i--) {
//         if (ans.length >= k) break;
//         if (frequencyArray[i] != 0) {
//             // frequencyArray[i].sort((a, b) => a.localeCompare(b))
//             ans = [...ans, ...frequencyArray[i]]
//             if (ans.length > k) ans = ans.slice(0, k);
//         }
//     }

//     return ans;

// };

// // console.log(topKFrequent(["the","day","is","sunny","the","the","the","sunny","is","is"], 4))

// // console.log(topKFrequent(["i","love","leetcode","i","love","coding"], 2))

// // console.log(topKFrequent(["i", "love", "leetcode", "i", "love", "coding"], 1))


// console.log(topKFrequent(["i", "love", "leetcode", "i", "love", "coding"], 3))





