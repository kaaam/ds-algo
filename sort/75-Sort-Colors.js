// /**
//  * @param {number[]} nums
//  * @return {void} Do not return anything, modify nums in-place instead.
//  */

// var swap = function (nums, index1, index2) {
//     let temp = nums[index1];
//     nums[index1] = nums[index2];
//     nums[index2] = temp;
// }

// var quickSortRecursive = function (nums, start, end) {

//     if (start >= end) return;

//     let pivotIndex = (end + start) >> 1;
//     // console.log(nums, start, end, pivotIndex)
//     swap(nums, pivotIndex, end);

//     let leftIndex = start;
//     let rightIndex = end - 1;
//     // console.log(leftIndex, rightIndex, end, pivotIndex, ' leftIndex, rightIndex, end, pivotIndex ')

//     // console.log(nums)
//     while (leftIndex < rightIndex) {
//         // console.log(nums[end], " pivot ")
//         // console.log(leftIndex, rightIndex, nums[leftIndex], nums[rightIndex])
//         if (nums[leftIndex] > nums[end] && nums[rightIndex] <= nums[end]) {
//             swap(nums, leftIndex, rightIndex);
//             leftIndex++;
//             rightIndex--;
//         } else if (nums[leftIndex] <= nums[end]) {
//             leftIndex++;
//         } else if (nums[rightIndex] > nums[end]) {
//             rightIndex--;
//         } else if (nums[leftIndex] <= nums[end] && nums[rightIndex] > nums[end]) {
//             leftIndex++;
//             rightIndex--;
//         }
//     }
//     // console.log(leftIndex, rightIndex, end)
//     // if(leftIndex == rightIndex) swap(nums, leftIndex+1, end);
//     // else swap(nums, leftIndex, end);
//     if (nums[leftIndex] > nums[end]) {
//         swap(nums, leftIndex, end);

//         // console.log(nums, start, leftIndex - 1, " !!! a 1")
//         // console.log(nums, leftIndex + 1, end, " !!! a 2")

//         quickSortRecursive(nums, start, leftIndex - 1);
//         quickSortRecursive(nums, leftIndex + 1, end);
//     } else {
//         swap(nums, leftIndex + 1, end);

//         // console.log(nums, start, leftIndex, " !!! b 1")
//         // console.log(nums, leftIndex + 2, end, " !!! b 2")


//         quickSortRecursive(nums, start, leftIndex);
//         quickSortRecursive(nums, leftIndex + 2, end);
//     }


// }

// // **************************************** brute force sort
// var sortColors = function (nums) {
//     // let test = [...nums];
//     // test.sort((a, b) => a - b);
//     quickSortRecursive(nums, 0, nums.length - 1);
//     // return [nums, test];
//     return nums;
// };

// // console.log(sortColors([2, 0, 2, 1, 1, 0]));
// console.log(sortColors([3, 4, 4, 4, 4, 5, 3, 4, 5, 3, 5, 4, 5, 6, 6, 5, 5, 56, 5, 4, 5, 65, 4, 6, 6, 6, 5, 43, 3, 7, 8, 8, 432, 22, 3, 5, 6, 8, 9, 9, 6, 53, 2, 2, 5, 6, 6, 4, 7, 4, 3, 6, 4, 5]));





var swap = function (nums, index1, index2) {
    let temp = nums[index1];
    nums[index1] = nums[index2];
    nums[index2] = temp;
}


/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var sortColors = function (nums) {
    let zeroEndinng = -1;
    let twoStarting = nums[nums.length - 1] == 2 ? nums.length - 1 : nums.length;

    let i = 0;
    while (i < nums.length) {
        // console.log(i, nums)
        // console.log(zeroEndinng, twoStarting, " zeroEndinng, twoStarting ")
        // console.log('\n')
        if (nums[i] == 0) {
            if (zeroEndinng + 1 == i) {
                zeroEndinng = i;
                i++;
            } else {
                swap(nums, zeroEndinng + 1, i);
                zeroEndinng += 1;
            }
        } else if (nums[i] == 2) {
            if (i >= twoStarting) {
                i++;
            } else {
                swap(nums, twoStarting - 1, i);
                twoStarting -= 1;
            }
        } else {
            i++;
        }
    }

    return nums;

};

// console.log(sortColors([2, 0, 2, 1, 1, 0]));
// console.log(sortColors([1, 1, 2, 0, 0, 0, 1, 1, 1, 2, 2, 0, 0, 1, 1, 0, 1, 2]));
// console.log(sortColors([0, 0, 0, 0, 0]));
// console.log(sortColors([1, 1, 1, 1, 1]));
// console.log(sortColors([2, 2, 2, 2, 2]));
// console.log(sortColors([2, 2, 2, 2, 2, 1]));
// console.log(sortColors([2, 2, 2, 2, 2, 0]));
// console.log(sortColors([2, 2, 2, 1, 2, 2]));
// console.log(sortColors([2, 2, 2, 0, 2, 2]));
console.log(sortColors([2, 2, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0]));