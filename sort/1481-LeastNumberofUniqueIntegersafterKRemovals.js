/**
 * @param {number[]} arr
 * @param {number} k
 * @return {number}
 */


// ******************** O(N) bucket
var findLeastNumOfUniqueInts = function (arr, k) {
    let freq = {};
    let maxFreq = -1;
    for (let num of arr) {
        num in freq ? freq[num] += 1 : freq[num] = 1;
        if (freq[num] > maxFreq) maxFreq = freq[num];
    }
    // console.log(freq)
    // console.log(maxFreq)

    let allFreq = Object.values(freq);
    let freqArrayBucket = new Array(maxFreq + 1).fill(0);
    let left = allFreq.length;
    // console.log(allFreq)
    // let right = -1;
    for (let _freq of allFreq) {
        freqArrayBucket[_freq] += 1;
        if (_freq < left) left = _freq;
        // if(_freq>right) right = _freq;
    }

    let count = 0;
    let unique = allFreq.length;
    // console.log(unique, " unique ");
    // console.log(freqArrayBucket, " freqArrayBucket ");
    // console.log(left, " left ");
    while (count < k && left < freqArrayBucket.length) {
        if (freqArrayBucket[left] > 0) {


            // // count += left * freqArray[left];
            // if((left * freqArrayBucket[left]) + count > k){
            //     count += (left * freqArrayBucket[left]);
            //     break;
            // }else if((left * freqArrayBucket[left]) + count == k){
            //     unique -= freqArrayBucket[left];
            //     count += (left * freqArrayBucket[left]);
            //     break;
            // }else{
            //     unique -= freqArrayBucket[left];
            //     count += (left * freqArrayBucket[left]);
            // }

            let toSubtract = Math.min((left * freqArrayBucket[left]), (k - count));
            count += toSubtract;
            unique -= Math.floor(toSubtract / left);

        }
        left++;
    }

    return unique;


};


console.log(findLeastNumOfUniqueInts([5, 5, 5, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 8, 8], 7))