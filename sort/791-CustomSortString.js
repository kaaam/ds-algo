/**
 * @param {string} order
 * @param {string} s
 * @return {string}
 */

// // ************************************ N*(LOG N)
// var customSortString = function (order, s) {
//     let map = {};
//     for (let i = 0; i < order.length; i++) {
//         map[order[i]] = i + 1;
//     }

//     let sList = s.split("");

//     sList.sort((a, b) => {
//         return (map[a] != undefined ? map[a] : 0) - (map[b] != undefined ? map[b] : 0)
//     })
//     return sList.join('');
// };

// console.log(customSortString("cba", "abcd"));





// ************************************ O(N)
var customSortString = function (order, s) {
    let visitedCount = new Array(order.length).fill(``);
    let result = ``;
    let map = {};
    for (let i = 0; i < order.length; i++) {
        map[order[i]] = i;
    }

    for (let _s of s) {
        if (_s in map) {
            visitedCount[map[_s]] += _s;
        } else {
            result += _s;
        }
    }

    for (let i = 0; i < visitedCount.length; i++) {
        // if(visitedCount[i])
        result += visitedCount[i];
    }
    // console.log(result)
    return result;


};

console.log(customSortString("cba", "abcdczxnmjkl"));