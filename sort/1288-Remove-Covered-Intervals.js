/**
 * @param {number[][]} intervals
 * @return {number}
 */
var removeCoveredIntervals = function (intervals) {
    intervals.sort((a, b) => {
        if (a[0] == b[0]) {
            return ((b[1] - b[0]) - (a[1] - a[0]));
        } else {
            return a[0] - b[0];
        }
    })
    let ans = [];

    let i = 1;
    let prev = intervals[0]
    while (i < intervals.length) {

        if (intervals[i][1] > prev[1]) {
            ans.push(prev);
            prev = intervals[i];
        }
        i++;
    }
    ans.push(prev);

    // return ans;
    return ans.length;

};

console.log(removeCoveredIntervals(
    [
        [1, 6],
        [35, 45],
        [1, 10],
        [5, 12],
        [7, 20],
        [40, 42],
        [9, 30],
        [7, 25],
        [12, 20],
        [1, 2],
    ]
));

