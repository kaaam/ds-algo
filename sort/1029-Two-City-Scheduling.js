/**
 * @param {number[][]} costs
 * @return {number}
 */
var twoCitySchedCost = function (costs) {
    let totalCosts = 0;
    let firstCount = 0;
    let secondCount = 0;

    costs.sort((a, b) => {
        return (Math.abs(b[0] - b[1]) - Math.abs(a[0] - a[1]));
    })

    // console.log(costs, " costs ")

    for (let _cost of costs) {
        // console.log(_cost, " _cost ", _cost[0]-_cost[1])
        if (_cost[0] < _cost[1]) {
            if (firstCount < (costs.length / 2)) {
                firstCount++;
                totalCosts += _cost[0];
            } else {
                secondCount++;
                totalCosts += _cost[1];
            }
        } else {
            if (secondCount < (costs.length / 2)) {
                secondCount++;
                totalCosts += _cost[1];
            } else {
                firstCount++;
                totalCosts += _cost[0];
            }
        }
    }

    return totalCosts;

};

// console.log(twoCitySchedCost([[10, 20], [30, 200], [400, 50], [30, 20]]));
console.log(twoCitySchedCost([[259, 770], [448, 54], [926, 667], [184, 139], [840, 118], [577, 469]]));
