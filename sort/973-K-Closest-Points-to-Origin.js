/**
 * @param {number[][]} points
 * @param {number} k
 * @return {number[][]}
 */


// // ************************ N*(log(N)) quicksort
// var kClosest = function (points, k) {
//     points.sort((a, b) => {
//         return (Math.pow(a[0] - 0, 2) + Math.pow(a[1] - 0, 2)) - (Math.pow(b[0] - 0, 2) + Math.pow(b[1] - 0, 2))
//     })
//     // console.log(points, " points ")
//     return points.slice(0, k)
// };

// console.log(kClosest([[1, 0], [2, 0], [3, 0], [0, 0], [0, 1.5], [0.5, 0.5]], 3))


var swap = function (array, i, j) {
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
    return array;
}


// ************************ N*(log(N)) quick search/select
var kClosest = function (points, k) {

    for (let _point of points) {
        _point.push((Math.pow(_point[0], 2) + Math.pow(_point[1], 2)))
    }

    let startIndex = 0;
    let endIndex = points.length - 1;
    let pivotIndex;
    let resultIndex;

    let left; let right;

    while (1) {
        // add break condition
        // console.log(points, "before")
        pivotIndex = (startIndex + endIndex) >> 1;
        // console.log(pivotIndex, " console.log(pivotIndex) ")
        swap(points, pivotIndex, endIndex);
        // console.log(points, "before after swap")
        left = startIndex;
        right = endIndex - 1;

        while (left < right) {
            if (points[left][2] <= points[endIndex][2] && points[right][2] > points[endIndex][2]) {
                left++; right--;
            } else if (points[left][2] > points[endIndex][2] && points[right][2] <= points[endIndex][2]) {
                swap(points, left, right);
                left++; right--;
            } else if (points[left][2] <= points[endIndex][2]) {
                left++;
            } else if (points[right][2] > points[endIndex][2]) {
                right--;
            }
            // console.log(left, right, points[left][2], points[right][2], points[endIndex][2], " left, right, points[left][2], points[right][2], points[endIndex][2] ")
        }
        
        // console.log(points)

        if (points[left][2] < points[endIndex][2]) {
            swap(points, left + 1, endIndex);
            resultIndex = left + 1;
        } else {
            swap(points, left, endIndex);
            resultIndex = left;
        }

        if (resultIndex == (k - 1)) {
            break;
        } else if (resultIndex < (k - 1)) {
            startIndex = resultIndex + 1;
        } else {
            endIndex = resultIndex - 1;
        }
        // console.log(resultIndex, " console.log(resultIndex) ")
        // console.log(points)

    }

    let ans = [];
    for(let _point of points.slice(0, k)){
        ans.push([_point[0], _point[1]])
    }
    return ans;
    // console.log(resultIndex, " console.log(resultIndex) ")
    // console.log(points)
    
};

// console.log(kClosest([[1, 0], [2, 0], [3, 0], [0, 0], [0, 1.5], [0.5, 0.5]], 2))
console.log(kClosest([[1, 0], [2, 0], [3, 0], [0, 0], [0, -1.5], [0.5, 0.5]], 3))