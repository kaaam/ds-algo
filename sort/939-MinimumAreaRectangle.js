/**
 * @param {number[][]} points
 * @return {number}
 */




// var minAreaRect = function (points) {
//     // console.log(points)
//     // points.sort((a, b)=>{

//     //     return
//     // })

//     let minArea = Number.MAX_SAFE_INTEGER;
//     // let minAreaIndex;

//     let xDict = {}
//     let xyDict = {};

//     for (let i = 0; i < points.length; i++) {
//         let point = points[i];
//         point[0] in xDict ? xDict[point[0]].push([...point, i]) : xDict[point[0]] = [[...point, i]];

//         if (point[0] in xyDict) {
//             xyDict[point[0]][point[1]] = 1;
//         } else {
//             xyDict[point[0]] = { [point[1]]: 1 };
//         }
//     }
//     // console.log(xyDict, " xyDict ")
//     // console.log(xDict, " xDict ")
//     let allX = Object.keys(xDict);
//     allX.sort((a, b) => a - b)

//     for (let xi = 0; xi < allX.length; xi++) {
//         let currX = allX[xi];
//         // console.log(currX, " currX ", xDict[currX], xi)

//         for (let i = 0; i < xDict[currX].length; i++) {
//             for (let j = i + 1; j < xDict[currX].length; j++) {
//                 // console.log(i, j, "i, j")

//                 // console.log(xDict[currX][i], xDict[currX][j])

//                 for (let xiNew = xi + 1; xiNew < allX.length; xiNew++) {
//                     if (xyDict[allX[xiNew]][xDict[currX][j][1]] && xyDict[allX[xiNew]][xDict[currX][i][1]]) {
//                         // console.log(allX[xiNew], " currXNew ", xDict[allX[xiNew]], xiNew)
//                         // console.log(xyDict[allX[xiNew]][xDict[currX][i][1]], xyDict[allX[xiNew]][xDict[currX][j][1]], "!!!!!!")
//                         let area = Math.abs((xDict[currX][j][1] - xDict[currX][i][1]) * (allX[xiNew] - allX[xi]));
//                         // console.log(xDict[currX][j][1], xDict[currX][i][1], allX[xiNew], allX[xi]);
//                         if (area < minArea) minArea = area;
//                         // console.log(area, " area ")
//                         // console.log('\n')
//                     }

//                 }
//             }
//         }


//     }

//     return minArea == Number.MAX_SAFE_INTEGER ? 0 : minArea;


// };

// console.log(minAreaRect([[1, 1], [1, 3], [3, 1], [3, 3], [4, 1], [4, 3], [10, 20], [1, 4], [1, 5], [2, 1], [2, 3]]));



// ********** incomplete
// var minAreaRect = function (points) {
//     let maxX = -1;
//     let maxY = -1;

//     for (let point of points) {
//         if (point[0] > maxX) maxX = point[0];
//         if (point[1] > maxY) maxY = point[1];
//     }

//     // console.log(maxX, maxY, " maxX, maxY ")

//     let dpArray = [];
//     for (let i = 0; i < maxY+1; i++) {
//         dpArray.push(new Array(maxX + 1).fill(0));
//     }

//     console.log(dpArray, " dpArray ")

//     for (let point of points) {
//         // console.log(dpArray[point[1]][point[0]], " dpArray[point[1]][point[0]] ", point[0], point[1])
//         dpArray[point[1]][point[0]] = 1;
//     }
//     console.log(dpArray, " dpArray ")
//     // console.log(dpArray.toString(), " dpArray ")
// }


// // console.log(minAreaRect([[1, 2], [3, 2], [1, 3], [3, 3], [3, 0], [1, 4], [4, 2], [4, 0]]));
// console.log(minAreaRect([[1, 1], [1, 3], [3, 1], [3, 3], [4, 1], [4, 3], [10, 20], [1, 4], [1, 5], [2, 1], [2, 3]]));






// ******************************************************* optimies (incomplete)
var minAreaRect = function (points) {
    // console.log(points)
    // points.sort((a, b)=>{

    //     return
    // })

    let minArea = Number.MAX_SAFE_INTEGER;
    // let minAreaIndex;

    let xDict = {}
    let yDict = {}
    // let xyDict = {};

    for (let i = 0; i < points.length; i++) {
        let point = points[i];
        point[0] in xDict ? xDict[point[0]].push([...point, i]) : xDict[point[0]] = [[...point, i]];
        point[1] in yDict ? yDict[point[1]].push([...point, i]) : yDict[point[1]] = [[...point, i]];

        // if (point[0] in xyDict) {
        //     xyDict[point[0]][point[1]] = 1;
        // } else {
        //     xyDict[point[0]] = { [point[1]]: 1 };
        // }
    }
    // console.log(xyDict, " xyDict ")
    // console.log(xDict, " xDict ")
    let allX = Object.keys(xDict);
    let allY = Object.keys(yDict);
    allX.sort((a, b) => a - b)
    allY.sort((a, b) => a - b)

    for (let xi = 0; xi < allX.length; xi++) {
        let currX = allX[xi];
        // console.log(currX, " currX ", xDict[currX], xi)

        for (let i = 0; i < xDict[currX].length; i++) {
            for (let j = i + 1; j < xDict[currX].length; j++) {
                
                // console.log(i, j, "i, j")

                // console.log(xDict[currX][i], xDict[currX][j])

                // TODO: instead to this traversal, add pair of i and j to a dictionary, when xi moves further, check pairs in the dictionary

                // for (let xiNew = xi + 1; xiNew < allX.length; xiNew++) {
                //     if (xyDict[allX[xiNew]][xDict[currX][j][1]] && xyDict[allX[xiNew]][xDict[currX][i][1]]) {
                //         // console.log(allX[xiNew], " currXNew ", xDict[allX[xiNew]], xiNew)
                //         // console.log(xyDict[allX[xiNew]][xDict[currX][i][1]], xyDict[allX[xiNew]][xDict[currX][j][1]], "!!!!!!")
                //         let area = Math.abs((xDict[currX][j][1] - xDict[currX][i][1]) * (allX[xiNew] - allX[xi]));
                //         // console.log(xDict[currX][j][1], xDict[currX][i][1], allX[xiNew], allX[xi]);
                //         if (area < minArea) minArea = area;
                //         // console.log(area, " area ")
                //         // console.log('\n')
                //     }

                // }
            }
        }


    }

    return minArea == Number.MAX_SAFE_INTEGER ? 0 : minArea;


};

console.log(minAreaRect([[1, 1], [1, 3], [3, 1], [3, 3], [4, 1], [4, 3], [10, 20], [1, 4], [1, 5], [2, 1], [2, 3]]));