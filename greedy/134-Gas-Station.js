/**
 * @param {number[]} gas
 * @param {number[]} cost
 * @return {number}
 */

// ******************* fails for console.log(canCompleteCircuit([5,8,2,8], [6,5,6,6]));

var canCompleteCircuit = function (gas, cost) {
    let leftGas = 0;

    let maxContinuesStart = null;
    let maxContinuesEnd = null;
    let maxGasGained = 0;

    let currentContinuesStart = null;
    let currentContinuesEnd = null;
    let currentGasGained = 0;

    for (let i = 0; i < gas.length; i++) {
        let change = gas[i] - cost[i];
        currentGasGained += change;

        if (currentGasGained >= 0) {
            currentContinuesStart == null ? currentContinuesStart = i : null;
            currentContinuesEnd = i;
        } else {
            currentContinuesStart = null;
            currentContinuesEnd = null;
            currentGasGained = 0;
        }

        if (currentGasGained > maxGasGained) {
            maxContinuesStart = currentContinuesStart;
            maxContinuesEnd = currentContinuesEnd;
            maxGasGained = currentGasGained;
        }

        leftGas += change;
    }
    if (leftGas < 0) return -1;

    // if (currentGasGained > 0 && (currentContinuesEnd == gas.length - 1) && (maxContinuesStart == 0)) {
    //     maxGasGained += currentGasGained;
    //     return currentContinuesStart;

    // }

    // TODO:itearte from curr postion to max start, if still the gas is remaining then add both
    
    if (currentGasGained > 0 && currentContinuesEnd == gas.length - 1){
        // let currIndex = currentContinuesEnd;
        let currentGas = currentGasGained;
        for(let i=0; i<maxContinuesStart;i++){
            currentGas += gas[i] - cost[i];
        }
        // console.log(currentGas, " currentGas ")
        if(currentGas>=0){
            return currentContinuesStart;
        }

    }


    return maxContinuesStart;


};

// // console.log(canCompleteCircuit([1, 2, 3, 4, 5], [3, 4, 5, 1, 2]));
// // console.log(canCompleteCircuit([3, 1, 1, 10], [2, 10, 2, 1]));
// // console.log(canCompleteCircuit([10, 3, 1, 1], [1, 2, 10, 2]));
// // console.log(canCompleteCircuit([10, 1, 1, 3], [1, 10, 2, 2]));

console.log(canCompleteCircuit([5,8,2,8], [6,5,6,6]));





// **************************************************************** someone else's

// My thinking process is a bit different.

// Considering at each index if the gas > cost, means we are gaining some extra gas, otherwise we are losing some gas.

// If the starting point exists, it must start from the position where we lose the most of the gas, so that it can start to gain gas first to gather all the gas we need before we start losing.

// Take an example like (here the number refers to the value gas[i] - cost[i])

// [3,4,-12,4,-5,6]

// The minimum tank value will happen at index 4, where the value is -5, because at there our tank is at the minimum value -6, which means if the result exists, it must start to gather gas at index 5 so that we can cover all the gas loss before we reach index 4.

// The algorithm is simple, find the minimum tank value and its index, and then use the next index as the starting point.

//     public int canCompleteCircuit(int[] gas, int[] cost) {
//         int minimumIndex = 0;
//         int minimum = Integer.MAX_VALUE;
        
//         int total = 0;
        
//         for (int i = 0; i < gas.length; i++) {
//             total += (gas[i] - cost[i]);
            
//             if (total < minimum) {
//                 minimumIndex = i;
//                 minimum = total;
//             }
//         }
        
//         if (total < 0) return -1;
        
//         return (minimumIndex + 1) % gas.length;
//     }
// }