/**
 * @param {number[][]} intervals
 * @return {number}
 */

// var eraseOverlapIntervals = function(intervals) {

//     intervals.sort((a,b)=>{
//         return a[1]-b[1];
//     })
//     let count = 0;

//     // let last = intervals[0];
//     let start = intervals[0][0];
//     let end = intervals[0][1];
//     for(let i=1;i<intervals.length;i++){
//         if(intervals[i][0]>=end){
//             start = intervals[i][0];
//             end = intervals[i][1];
//         }else if(intervals[i][0]<=start && intervals[i][1]>=end){
//             count++;
//         }else if(intervals[i][0]>=start && intervals[i][1]<=end){
//             start = intervals[i][0];
//             end = intervals[i][1];
//             count++;
//         }else{
//             count++;
//         }
//     }

//     return count;

// };

// console.log(eraseOverlapIntervals([[1,2],[2,3],[3,4],[1,3]]));



// ************ improvement

// removed this as it never comes }else if(intervals[i][0]>=start && intervals[i][1]<=end){

// else if(intervals[i][0]<=start && intervals[i][1]>=end){
//     count++;
// }else{
// count++;

// both else if and else are doing same thing, combined.
// start is not needed

var eraseOverlapIntervals = function (intervals) {

    intervals.sort((a, b) => {
        return a[1] - b[1];
    })
    let count = 0;

    // let last = intervals[0];
    // let start = intervals[0][0];
    let end = intervals[0][1];
    for (let i = 1; i < intervals.length; i++) {
        if (intervals[i][0] >= end) {
            // start = intervals[i][0];
            end = intervals[i][1];
        } else {
            count++;
        }
    }

    return count;

};