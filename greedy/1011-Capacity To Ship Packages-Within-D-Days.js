
// TODO: similar questions
// Minimum Number of Days to Make m Bouquets
// Find the Smallest Divisor Given a Threshold
// Divide Chocolate
// Capacity To Ship Packages In N Days
// Koko Eating Bananas
// Minimize Max Distance to Gas Station
// Split Array Largest Sum




/**
 * @param {number[]} weights
 * @param {number} days
 * @return {number}
 */


// // ********************** approch 1 (less than ideal or more)

// var recurse = function(weights, days, currWeight, currIndex){

//     if(currIndex>=weights.length) return currWeight;

//     let last;
//     let currSum = 0;
//     let i=currIndex;

//     while(currSum<currWeight){
//         currSum+=weights[i];
//         i++;
//     }

//     recurse(weights, days, currWeight, i)

    

// }

// var shipWithinDays = function(weights, days) {

    
// };

// console.log(shipWithinDays([1,2,3,4,5,6,7,8,9,10], 5));



/**
 * @param {number[]} weights
 * @param {number} days
 * @return {number}
 */
var shipWithinDays = function(weights, days) {
    // console.log(weights, " weights ")

    // let totalSum = 0;
    // let max = Number.MIN_SAFE_INTEGER;
    // for(let w of weights){
    //     if(w>max) max = w;
    //     totalSum+=w;
    // }

    var left = Math.max(...weights);
    var right = weights.reduce((a,b) => a + b);

    // let left = Math.floor(totalSum/days);
    // let left = max;
    // let right = totalSum;
    // let lastMiddle;

    while(left <= right){
        // console.log("\n\n\n")
        // console.log(left, right, "left, right,")
        let middle = (right+left)>>1;
        // console.log(middle, " middle ")
        let count = 0;
        let currentSum = 0;
        for(let i=0;i<weights.length-1;i++){
            // if(weights[i]>middle && lastMiddle) return lastMiddle;
            currentSum += weights[i];
            // console.log(currentSum, currentSum+weights[i+1], i, count, " currentSum, currentSum+weights[i+1], i, count ")
            if(currentSum+weights[i+1] > middle){
                // console.log(currentSum, " currentSum ")
                currentSum = 0;
                count++;
            }
        }
        // console.log(count, " count----- before ")
        // currentSum += weights[weights.length-1];
        // if(currentSum > middle){
        // if(weights[weights.length-1]>middle && lastMiddle) return lastMiddle;
        count++;
        // }

        // console.log(count, " count ")

        if(count>days){
            // lastMiddle = middle;
            left = middle+1;
        }else if(count<days){
            // lastMiddle = middle;
            right = middle-1;
        }else{
            // return middle;
            right = middle-1;
        }
        // console.log(left, right, "left, right, end")
    }
    return left;
    
};

console.log(shipWithinDays([1,2,3,4,5,6,7,8,9,10], 5));
console.log(shipWithinDays([3,2,2,4,1,4], 3));
console.log(shipWithinDays([1,2,3,1,1], 4));
console.log(shipWithinDays([10,50,100,100,50,100,100,100], 5));





// // ********************** copied

// var shipWithinDays = function(weights, D) {
//     var left = Math.max(...weights);
//     var right = weights.reduce((a,b) => a + b);
//     while (left < right){
//         console.log('\n\n\n\n')
//         var mid = Math.floor((left + right) / 2);
//         var needed = 1;
//         var current = 0;
//         for(var i = 0; i < weights.length; i++){
//             console.log(i, weights[i], current, mid, " i, weights[i], current, mid ")
//             if(current + weights[i] > mid){
//                 needed ++;
//                 current = weights[i];
//             }
//             else current += weights[i];
//         }
//         if(needed > D) left = mid + 1;
//         else right = mid;
//     }
//     return left;
// }

// console.log(shipWithinDays([1,2,3,1,1], 4));