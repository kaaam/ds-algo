// TODO: remember BigInt


/**
 * @param {number} h
 * @param {number} w
 * @param {number[]} horizontalCuts
 * @param {number[]} verticalCuts
 * @return {number}
 */
 var maxArea = function(h, w, horizontalCuts, verticalCuts) {
    
    horizontalCuts.push(0);
    horizontalCuts.push(h);
    
    verticalCuts.push(0);
    verticalCuts.push(w);
    
    horizontalCuts.sort((a, b)=>a-b);
    verticalCuts.sort((a, b)=>a-b);
    
    let maxHor = Number.MIN_SAFE_INTEGER;
    for(let i=0;i<horizontalCuts.length-1;i++){
        if((horizontalCuts[i+1] - horizontalCuts[i]) > maxHor){
            maxHor = (horizontalCuts[i+1] - horizontalCuts[i]);
        }
    }
    
    
    let maxVer = Number.MIN_SAFE_INTEGER;
    for(let i=0;i<verticalCuts.length-1;i++){
        if((verticalCuts[i+1] - verticalCuts[i]) > maxVer){
            maxVer = (verticalCuts[i+1] - verticalCuts[i]);
        }
    }
    
    // console.log(maxHor, maxVer)
    return (BigInt(maxHor)*BigInt(maxVer))%(BigInt(Math.pow(10, 9)+7));  
};