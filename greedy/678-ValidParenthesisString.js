/**
 * @param {string} s
 * @return {boolean}
 */


var parse = function(s){
    // console.log(s)
    let bal = 0;
    let stars = 0;
    for(let i=0;i<s.length;i++){
        if(s[i] == '('){
            bal++;
        }else if(s[i] == ')'){
            bal--;

            if(bal<0){
                if((stars+bal)>=0){
                    stars = stars+bal;
                    bal = 0;
                }else{
                    return false;
                }
            }
            // else if(bal == 0){
            //     stars = 0;
            // }

        }else{
            stars++;
        }

    }

    if(bal == 0){
        return true;
    }

    if(bal>0){
        // if(s[s.length-1] == '(') return false;
        return stars>=bal?true:false;
    }

}

var checkValidString = function(s) {
    // console.log('\n\n\n\n\n')

    let first = parse(s);
    
    
    // s = s.split('').reverse();
    // for(let i=0;i<s.length;i++){
    //     if(s[i] == '(') s[i] = ')';
    //     else if (s[i] == ')') s[i] = '(';
    // }
    // s = s.join('');

    let temp = [];
    for(let i=s.length-1;i>=0;i--){
        if(s[i] == '(') temp.push(')');
        else if(s[i] == ')') temp.push('(');
        else temp.push('*');
    }
    s = temp.join('');
    let second = parse(s);

    // console.log(first, second, " first, second ")
    if(first && second) return true;
    return false;
    

};


console.log(checkValidString('(*))'), true);

console.log(checkValidString('(*)))'), false);

console.log(checkValidString('(())'), true);

console.log(checkValidString('((*))'), true);
console.log(checkValidString('*****((*****)****)****'), true);


console.log(checkValidString('****(***)('), false);