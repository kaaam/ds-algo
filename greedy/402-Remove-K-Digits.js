/**
 * @param {string} num
 * @param {number} k
 * @return {string}
 */


// // ************************ TIME LIMIT EXCEED
// var removeKdigits = function(num, k) {

//     let targetLength = num.length-k;

//     let ans = [];
//     let left = 0;

//     while(ans.length<targetLength){
//         let placesRemaining = targetLength-ans.length;
//         let min = Number.MAX_SAFE_INTEGER;
//         let minIndex;
//         for(let i=left;i<=num.length-placesRemaining;i++){
//             if(num[i]<min){
//                 min = num[i];
//                 minIndex = i;
//             }
//         }
//         ans.push(min);
//         left = minIndex+1;
//         min = Number.MAX_SAFE_INTEGER;
//     }
//     return ans.join('');

    
// };

// // "1432219", k = 3

// // console.log(removeKdigits("1432219", 3));

// console.log(removeKdigits("10200", 1));




// ************************ Optimised 
var removeKdigits = function(num, k) {

    let i=1;
    let currRemoved = 0;
    let ansStack = [num[0]];
    let ans = ``;
    let hasNonZeroComeFlag = false;

    while(currRemoved<k && i<num.length){
        // console.log(ansStack[ansStack.length-1], " ansStack[ansStack.length-1] ")
        if(num[i]>=(ansStack[ansStack.length-1]==undefined?-1:ansStack[ansStack.length-1])){
            ansStack.push(num[i]);
            i++;
        }else{
            // console.log(ansStack, " vefore ")
            ansStack.pop();
            // console.log(ansStack, " after ")
            currRemoved++;
        }
        // 
    }

    // console.log(currRemoved, currRemoved == k, k, i, ansStack, " currRemoved, currRemoved == k, k, i, ansStack ")
    if(currRemoved == k){
        // if(i<num.length-1){
            for(let j=i;j<num.length;j++){
                ansStack.push(num[j]);
            }
        // }
    }else{
        // console.log("!!!")
        for(let j=0;j<(k-currRemoved);j++){
            ansStack.pop();
        }
        // console.log(ansStack)
        if(ansStack.length == 0) return '0';
    }
    // console.log(ansStack.length, ansStack)
    if(ansStack.length == 0) return '0';

    // console.log(ansStack)
    for(let _num of ansStack){
        if((_num == '0' && hasNonZeroComeFlag==true) || (_num != '0')){
            if(_num != '0') hasNonZeroComeFlag = true;
            ans = ans + _num;
        }
    }

    if(ans == "") return '0';

    // console.log(currRemoved, i)

    return ans;
    
};

// "1432219", k = 3

// console.log(removeKdigits("1432219", 3));

// console.log(removeKdigits("10200", 1));
// console.log(removeKdigits("123456718121", 3));
// console.log(removeKdigits("1234567", 3));
// console.log(removeKdigits("1234567", 100));
// console.log(removeKdigits("10", 2));
// console.log(removeKdigits("100", 1));
// console.log(removeKdigits("10001", 4));
// console.log(removeKdigits("1173", 2));
// 2

console.log(removeKdigits("21", 1));



