/**
 * @param {number} num
 * @return {number}
 */


// // *************** wrong (in this i am expecting str but input is number)

// var swap = function(str, i, j){
//     let temp = str[i];
//     str[i] = str[j];
//     str[j] = temp;
//     return str;
// }

// var maximumSwap = function(num) {
//     num = num.split('');
//     let ansStack = [];

//     let currMax = Number.MIN_SAFE_INTEGER;
//     let currMaxIndex = -1;
    
//     for(let i=num.length-1;i>=0;i--){

//         if(currMax<num[i]){
//             currMax = num[i];
//             currMaxIndex = i;
//         }
//         if(num[i]<currMax){
//             ansStack.pop();
//             ansStack.push([i, currMaxIndex]);
//         }

//     }
//     let indexes = ansStack.pop();
//     // console.log(indexes)
//     if(indexes){
//         num = swap(num, indexes[0], indexes[1]);
//     }

//     // console.log(ansStack)
//     return num.join('');
// };

// console.log(maximumSwap("2736"));
// // console.log(maximumSwap("1234"));
// // console.log(maximumSwap("4321"));






// *************** 

var swap = function(str, i, j){
    let temp = str[i];
    str[i] = str[j];
    str[j] = temp;
    return str;
}

var maximumSwap = function(num) {
    num = num.toString();
    num = num.split('');
    let ansStack = [];

    let currMax = Number.MIN_SAFE_INTEGER;
    let currMaxIndex = -1;
    
    for(let i=num.length-1;i>=0;i--){

        if(currMax<num[i]){
            currMax = num[i];
            currMaxIndex = i;
        }
        if(num[i]<currMax){
            ansStack.pop();
            ansStack.push([i, currMaxIndex]);
        }

    }
    let indexes = ansStack.pop();
    // console.log(indexes)
    if(indexes){
        num = swap(num, indexes[0], indexes[1]);
    }

    // console.log(ansStack)
    return num.join('');
};

console.log(maximumSwap("2736"));
// console.log(maximumSwap("1234"));
// console.log(maximumSwap("4321"));