/**
 * @param {number} startValue
 * @param {number} target
 * @return {number}
 */


// // ************************** 

// var recurse = function (startValue, target, currVal, count, alreadyVisitedDict) {
//     if(currVal in alreadyVisitedDict){
//         if(count<alreadyVisitedDict[currVal]){
//             alreadyVisitedDict[currVal] = count;
//         }else{
//             // console.log("!!!!!! ", currVal, count)
//             // return count;
//             return Number.MAX_SAFE_INTEGER;
//         }
//     }
//     // console.log((currVal % 2) == 0 && (currVal / 2) >= target && startValue<currVal, currVal, target)
//     if((currVal % 2) == 0 && (currVal / 2) >= target && startValue<currVal) return Number.MAX_SAFE_INTEGER;
//     if (currVal <= 0) return Number.MAX_SAFE_INTEGER;
//     if (currVal == startValue) return count;

//     // if(currVal%2 == 0){
//     //     recurse(startValue, target, currVal/2, count+1);
//     // }
//     // recurse(startValue, target, currVal+1, count+1);

//     // console.log(currVal)
//     alreadyVisitedDict[currVal] = count;

//     let first = ((currVal % 2) == 0 ? recurse(startValue, target, currVal / 2, count + 1, alreadyVisitedDict) : Number.MAX_SAFE_INTEGER);
//     let second = recurse(startValue, target, currVal + 1, count + 1, alreadyVisitedDict);
//     // console.log(first, second, currVal, " first, second, currVal ")
//     return Math.min(
//         first, second
//     );
// }

// var brokenCalc = function (startValue, target) {

//     if(target<startValue) return startValue-target;

//     return recurse(startValue, target, target, 0, {});

// };

// // console.log(brokenCalc(3, 10))
// // console.log(brokenCalc(1024, 1))
// // console.log(brokenCalc(30, 1))


// console.log(brokenCalc(100, 1))





var brokenCalc = function (startValue, target) {

    let count = 0;

    if (startValue > target) return startValue - target;

    while (target != startValue) {
        // console.log(target, " target ")
        if (target % 2 == 0 && target > startValue) target = target / 2;
        else target += 1;
        count++;
    }
    return count;

};


// console.log(brokenCalc(3, 10))
// console.log(brokenCalc(1024, 1))
// console.log(brokenCalc(30, 1))


// console.log(brokenCalc(100, 1))
console.log(brokenCalc(3, 1000))