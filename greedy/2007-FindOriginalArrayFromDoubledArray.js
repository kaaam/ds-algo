
// ************** OPTIMISE MORE

/**
 * @param {number[]} changed
 * @return {number[]}
 */
 var findOriginalArray = function(changed) {
    if(changed.length%2!=0) return [];

    let half = [];
    let double =[];
    
    let freqDict = {};
    
    for(let n of changed){
        n in freqDict?freqDict[n] += 1:freqDict[n]=1;
    }
    
    // let unique = Object.keys(changed);
    changed.sort((a, b)=>a-b);
    
    // console.log(freqDict, " freqDict ")
    for(let n of changed){
        // console.log(freqDict[n], n, " freqDict[n], n ")
        if(n in freqDict && freqDict[n]>0){
            if((n*2) in freqDict && freqDict[n*2]>0){
                freqDict[n*2] -= 1;
            }
            // else if((n/2) in freqDict && freqDict[n/2]>0){
            //     freqDict[n/2] -= 1;
            // }
            else{
                return [];
            }
            half.push(n);
            freqDict[n] -= 1;
        }
    }
    
    return half;
    
};




// ************** MORE OPTIMISED

