/**
 * @param {number[][]} intervals
 * @return {number}
 */
var minMeetingRooms = function (intervals) {

    // intervals.sort((a, b)=>a-b)
    let startArray = [];
    let endArray = [];

    for (interval of intervals) {
        startArray.push(interval[0]);
        endArray.push(interval[1]);
    }

    startArray.sort((a, b) => a - b);
    endArray.sort((a, b) => a - b);

    let startIndex = 0;
    let endIndex = 0;

    let maxCount = Number.MIN_SAFE_INTEGER;

    let count = 0;

    while (startIndex < startArray.length) {
        // console.log(startArray[startIndex], endArray[endIndex], count, '1')
        if (startArray[startIndex] < endArray[endIndex]) {
            count++;
            startIndex++;
        } else if (startArray[startIndex] > endArray[endIndex]) {
            count--;
            endIndex++;
        } else {
            endIndex++;
            startIndex++;
        }
        // console.log(startArray[startIndex], endArray[endIndex], count, '2')
        count > maxCount ? maxCount = count : null;
    }
    return maxCount;

};

// console.log(minMeetingRooms([[0,30],[5,10],[15,20]]));
// console.log(minMeetingRooms([[5,10],[15,20], [0,30]]));
// console.log(minMeetingRooms([[7,10],[2,4]]));
// console.log(minMeetingRooms([[0, 2],[0,3], [0, 4], [2, 4], [2, 8]]));
console.log(minMeetingRooms([[13, 15], [1, 13], [6, 9]]));
