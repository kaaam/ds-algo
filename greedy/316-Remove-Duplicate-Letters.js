/**
 * @param {string} s
 * @return {string}
 */
var removeDuplicateLetters = function(s) {
    let sSet = new Set([...s.split('')]);
    // console.log(sSet)
    // let new Set()
    let sArray = [...sSet];
    // console.log(sArray)
    sArray.sort((a, b)=>{
        return a.localeCompare(b);
    })
    let ans = sArray.join('');
    // console.log(sArray, ans)
    return ans;
};

console.log(removeDuplicateLetters("cbacdcbc"));