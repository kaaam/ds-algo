/**
 * @param {number[]} tops
 * @param {number[]} bottoms
 * @return {number}
 */
var minDominoRotations = function (tops, bottoms) {


    let target = tops[0];
    let topCount = 1;
    let bottonCount = 0;
    let first = 0;

    for (let i = 1; i < tops.length; i++) {

        // condering top is ideal
        if (tops[i] == target) {
        } else if (bottoms[i] == target) {
            bottonCount++;
        } else {
            first = null;
            break;
        }

        // condering bottoms is ideal
        if (bottoms[i] == target) {
            // switchesBalance++;
            // currPosition = 0;
        } else if (tops[i] == target) {
            topCount++;
            // currPosition = 1;
        } else {
            // switchesBalance = null;
            first = null;
            break;
        }
    }

    if(first!=null){
        first = Math.min(topCount, bottonCount);
    }

    // console.log(bottonCount, topCount, " bottonCount, topCount ")
    // console.log(first, " first ")



    target = bottoms[0];
    topCount = 0;
    bottonCount = 1;
    let second = 0;

    for (let i = 1; i < tops.length; i++) {

        // condering bottoms is ideal
        if (bottoms[i] == target) {

        } else if (tops[i] == target) {
            topCount++;
        } else {
            second = null;
            break;
        }

        // condering top is ideal
        if (tops[i] == target) {

        } else if (bottoms[i] == target) {
            bottonCount++;
        } else {
            second = null;
            break;
        }
    }

    // console.log(bottonCount, topCount, " bottonCount, topCount ")
    // console.log(second, " second ")

    if(second!=null){
        second = Math.min(topCount, bottonCount);
    }


    if(first!=null && second!=null){
        return Math.min(first, second);
    }else if(first!=null){
        return first;
    }else if(second!=null){
        return second;
    }else{
        return -1;
    }





};


// console.log(minDominoRotations([2,1,2,4,2,2], [5,2,6,2,3,2]));

// console.log(minDominoRotations([3, 5, 1, 2, 3], [3, 6, 3, 3, 4]));
// console.log(minDominoRotations([3, 3, 3, 3, 3], [1, 2, 3, 4, 5]));
// console.log(minDominoRotations([3, 1, 3, 1, 5], [1, 3, 1, 3, 1]));
// console.log(minDominoRotations([1, 4, 4, 4, 4], [4, 1, 1, 1, 3]));

console.log(minDominoRotations([1, 1, 1, 1, 3, 1, 1], [3, 1, 1, 1, 1, 3, 5]));

