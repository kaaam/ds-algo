/**
 * Definition for knows()
 * 
 * @param {integer} person a
 * @param {integer} person b
 * @return {boolean} whether a knows b
 * knows = function(a, b) {
 *     ...
 * };
 */

/**
 * @param {function} knows()
 * @return {function}
 */

 var itera = function(updatedList, knownByArray, uniqueCelebrity, knows, knownByRemainingArray){
    // console.log(knownByRemainingArray)
    for(let i=0;i<updatedList.length;i++){
                for(let j=0;j<updatedList.length;j++){
                    if(updatedList[i] == updatedList[j]) continue;
                    // console.log(updatedList[i], updatedList[j], updatedList)
                    let ij = knows(updatedList[i], updatedList[j]);
                    let ji = knows(updatedList[j], updatedList[i]);
                    if(ij){
                        // knowsArray[updatedList[i]]?knowsArray[updatedList[i]].push(updatedList[j]):knowsArray[i]=[updatedList[j]];
                        knownByArray[updatedList[j]]?knownByArray[updatedList[j]].push(updatedList[i]):knownByArray[updatedList[j]]=[updatedList[i]];
                        
                        // console.log(knownByRemainingArray[updatedList[j]], updatedList[j], knownByRemainingArray, " knownByRemainingArray[updatedList[j]] ")
                        knownByRemainingArray[updatedList[j]].delete(updatedList[i]);
                        
                        uniqueCelebrity.delete(updatedList[i]);
                        
                    }
                    
                    if(ji){
                        // knowsArray[updatedList[j]]?knowsArray[updatedList[j]].push(updatedList[i]):knowsArray[updatedList[j]]=[updatedList[i]];

                        knownByArray[updatedList[i]]?knownByArray[updatedList[i]].push(updatedList[j]):knownByArray[updatedList[i]]=[updatedList[j]];
                        
                        // console.log(knownByRemainingArray[updatedList[i]], updatedList[i], knownByRemainingArray, " knownByRemainingArray[updatedList[j]] ")
                        knownByRemainingArray[updatedList[i]].delete(updatedList[j]);
                        
                        uniqueCelebrity.delete(updatedList[j]);
                    }
                    
                    // console.log(ij || ji)
                    if(ij || ji) return;
                    
                }
            }
}

var solution = function(knows) {
    /**
     * @param {integer} n Total people
     * @return {integer} The celebrity
     */

    return function(n) {
        // console.log(this.knows, " knows ")
        let uniqueCelebrity = new Set();
        for(let i=0;i<n;i++){
            uniqueCelebrity.add(i);
        }
        
        let knownByArray = new Array(n).fill(null);
        let knownByRemainingArray = new Array(n);
        for(let i=0;i<knownByRemainingArray.length;i++){
            // _i = [...uniqueCelebrity];
            knownByRemainingArray[i] = new Set([...uniqueCelebrity]);
            // console.log(_i)
        }
        // console.log(knownByRemainingArray)
//         uniqueCelebrity
        // let knowsArray = new Array(n).fill(null);
        
        while(1){
            let updatedList = [...uniqueCelebrity];
            if (updatedList.length == 0) break;
            itera(updatedList, knownByArray, uniqueCelebrity, knows, knownByRemainingArray);
            
            
            if (updatedList.length==uniqueCelebrity.size) break;
        }
        
        // console.log(uniqueCelebrity, " uniqueCelebrity ")
        // console.log(knownByRemainingArray, " knownByRemainingArray ")
        // console.log(knownByArray, " knownByArray ")
        // console.log(knowsArray, " knowsArray ")
        
        let updatedList = [...uniqueCelebrity];
        for(let cele of updatedList){
            if(knownByArray[cele] && knownByArray[cele].length == n-1) return cele;
        }
        
        for(let cele of updatedList){
            let knownCount = 0;
            // let knownBySet = new Set(knownByArray[cele]?knownByArray[cele]:[]);
            // console.log(knownBySet)
//             for(let i=0;i<n;i++){
//                 if(i==cele) continue;
//                 // console.log(i, cele)
//                 // if(!knownBySet.has(i)&&knows(i, cele)) knownCount++;
//                 // else{break;}
                
//                 // if(!knownBySet.has(i)){
//                     if(knows(i, cele) && !knows(cele, i)){
//                         // console.log(i, cele, " i, cele ")
//                         knownCount++;
//                     }else{
//                         break;
//                     }
//                 // }
                
                
            // }
            
            for(let i of [...knownByRemainingArray[cele]]){
                if(i==cele) continue;
                // console.log(i, cele)
                // console.log(knows(i, cele), !knows(cele, i))
                if(knows(i, cele) && !knows(cele, i)){
                        // console.log(i, cele, " i, cele ")
                        knownCount++;
                    }else{
                        break;
                    }
                
            }
            
            // console.log(knownCount, " knownCount ")
            // console.log(knownByArray[cele]?knownByArray[cele].length:0, " knownByArray[cele]?knownByArray[cele].length:0 ")

            if (knownCount+(knownByArray[cele]?knownByArray[cele].length:0) == n-1) return cele;
            
        }
        
        return -1
    };
};