/**
 * @param {number} target
 * @param {number} maxDoubles
 * @return {number}
 */
 var minMoves = function(target, maxDoubles) {
    let curr = target;
    let ans = 0;
    
    while(curr != 1){
        
        if(maxDoubles == 0) return ans+curr-1;
        
        if(curr%2 == 0 && maxDoubles>0){
           curr /= 2; 
           maxDoubles -- ;
        }else{
            curr -= 1;
        }
        ans++;
    }
    
    return ans;
};