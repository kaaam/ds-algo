/**
 * @param {number[]} nums
 * @return {number}
 */
var getMaxLen = function (nums) {
    let maxLength = 0;
    let maxLengthReverse = 0;

    let currLength = 0;
    let currSign = 1;

    let currLengthReverse = 0;
    let currSignReverse = 1;

    for (let i = 0; i < nums.length; i++) {
        let reverseI=nums.length-1-i;
        // if(nums[i]>0){
        if (nums[i] == 0) {
            currLength = 0;
            currSign = 1;
        } else {
            currSign = (currSign * nums[i]) > 0 ? 1 : -1;
            // console.log(currSign, nums[i], currLength, maxLength, " currSign, nums[i], currLength, maxLength, ")
            currLength++;
            if (currSign == 1) {
                if (currLength > maxLength) maxLength = currLength;
            }
        }

        if (nums[reverseI] == 0) {
            currLengthReverse = 0;
            currSignReverse = 1;
        } else {
            currSignReverse = (currSignReverse * nums[reverseI]) > 0 ? 1 : -1;
            currLengthReverse++;
            // console.log(currSignReverse, nums[reverseI], currLengthReverse, maxLengthReverse, " currSignReverse, nums[reverseI], currLengthReverse, maxLengthReverse")
            if (currSignReverse == 1) {
                if (currLengthReverse > maxLengthReverse) maxLengthReverse = currLengthReverse;
            }
        }

        // console.log(currSign, nums[i], currLength)
        // }


    }

    // console.log(maxLength, " maxLength ")
    // console.log(maxLengthReverse, " maxLengthReverse ")
    return Math.max(maxLength, maxLengthReverse);
    // return maxLength;

};

// console.log(getMaxLen([-1, -2, -3, 0, 1]));
// console.log(getMaxLen([-1,2]));

console.log(getMaxLen([5,-20,-20,-39,-5,0,0,0,36,-32,0,-7,-10,-7,21,20,-12,-34,26,2]));