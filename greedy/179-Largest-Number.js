/**
 * @param {number[]} nums
 * @return {string}
 */
// var largestNumber = function (nums) {
//     let dict = {};
//     let ansArray = [];

//     for (let num of nums) {
//         num = num.toString();
//         let first = num[0];
//         first in dict ? dict[first].push(num) : dict[first] = [num];
//     }

//     for (let i = 9; i >= 0; i--) {
//         if (i in dict) {
//             let val = dict[i];
//             val.sort((a, b) => {
//                 let first = '' + a + b;
//                 let second = '' + b + a;
//                 for (let j = 1; j < (a.length - 1 + b.length - 1 + 1); j++) {
//                     // console.log(first, second)
//                     // console.log(first[j], second[j])
//                     if (first[j] > second[j]) return -1;
//                     if (first[j] < second[j]) return 1;
//                     // console.log(a, b)
//                     // console.log((a[j] == undefined ? b[j-a.length] : a[j]), (b[j] == undefined ? a[j-b.length] : b[j]))
//                     // if ((a[j] == undefined ? b[j-a.length] : a[j]) > (b[j] == undefined ? a[j-b.length] : b[j])) return -1;
//                     // if ((a[j] == undefined ? b[j-a.length] : a[j]) < (b[j] == undefined ? a[j-b.length] : b[j])) return 1;
//                 }
//             })
//             ansArray = [...ansArray, ...val]

//         }
//     }
//     // console.log(dict)

//     // for (let [key, val] of Object.entries(dict).sort((a, b) => b[0] - a[0])) {

//     //     let leftPointer = 0;
//     //     let rightPointer = val.length - 1;

//     //     while (leftPointer < rightPointer) {
//     //         if (leftPointer == rightPointer) {
//     //             ans = ans + val[leftPointer];
//     //             leftPointer++;
//     //             rightPointer--
//     //         } else {
//     //             if (val[leftPointer].length == 1) {
//     //                 leftPointer++;
//     //             }
//     //             if (val[rightPointer].length == 1) {
//     //                 rightPointer--;
//     //             }

//     //             // if()
//     //         }
//     //     }
//     // }
//     // console.log(ansArray)

//     let ans = '';

//     let nonZeroCame = false;
//     for (let i = 0; i < ansArray.length - 1; i++) {
//         if (nonZeroCame == false && ansArray[i] == '0') {
//             continue;
//         } else {
//             nonZeroCame = true;
//             ans = ans + ansArray[i];
//         }
//     }
//     ans = ans + ansArray[ansArray.length - 1];
//     return ans;
// };


// console.log(largestNumber([0, 0]));







var largestNumber = function (nums) {
    let dict = {};

    // for (let num of nums) {
    //     num = num.toString();
    //     let first = num[0];
    //     first in dict ? dict[first].push(num) : dict[first] = [num];
    // }

    nums.sort((a, b) => {
        // console.log('\n')
        let first = '' + a + b;
        let second = '' + b + a;
        for (let j = 0; j < first.length; j++) {
            // console.log(first, second)
            // console.log(first[j], second[j])
            // console.log(first[j] > second[j])
            // console.log(first[j] < second[j])
            if (first[j] > second[j]) return -1;
            if (first[j] < second[j]) return 1;
            // console.log(a, b)
            // console.log((a[j] == undefined ? b[j-a.length] : a[j]), (b[j] == undefined ? a[j-b.length] : b[j]))
            // if ((a[j] == undefined ? b[j-a.length] : a[j]) > (b[j] == undefined ? a[j-b.length] : b[j])) return -1;
            // if ((a[j] == undefined ? b[j-a.length] : a[j]) < (b[j] == undefined ? a[j-b.length] : b[j])) return 1;
        }
    })
    // console.log(nums)

    // for (let i = 9; i >= 0; i--) {
    //     if (i in dict) {
    //         let val = dict[i];
    //         val.sort((a, b) => {
    //             let first = '' + a + b;
    //             let second = '' + b + a;
    //             for (let j = 1; j < (a.length - 1 + b.length - 1 + 1); j++) {
    //                 // console.log(first, second)
    //                 // console.log(first[j], second[j])
    //                 if (first[j] > second[j]) return -1;
    //                 if (first[j] < second[j]) return 1;
    //                 // console.log(a, b)
    //                 // console.log((a[j] == undefined ? b[j-a.length] : a[j]), (b[j] == undefined ? a[j-b.length] : b[j]))
    //                 // if ((a[j] == undefined ? b[j-a.length] : a[j]) > (b[j] == undefined ? a[j-b.length] : b[j])) return -1;
    //                 // if ((a[j] == undefined ? b[j-a.length] : a[j]) < (b[j] == undefined ? a[j-b.length] : b[j])) return 1;
    //             }
    //         })
    //         ansArray = [...ansArray, ...val]

    //     }
    // }
    // console.log(dict)

    // for (let [key, val] of Object.entries(dict).sort((a, b) => b[0] - a[0])) {

    //     let leftPointer = 0;
    //     let rightPointer = val.length - 1;

    //     while (leftPointer < rightPointer) {
    //         if (leftPointer == rightPointer) {
    //             ans = ans + val[leftPointer];
    //             leftPointer++;
    //             rightPointer--
    //         } else {
    //             if (val[leftPointer].length == 1) {
    //                 leftPointer++;
    //             }
    //             if (val[rightPointer].length == 1) {
    //                 rightPointer--;
    //             }

    //             // if()
    //         }
    //     }
    // }
    // console.log(ansArray)

    let ans = '';

    let nonZeroCame = false;
    for (let i = 0; i < nums.length - 1; i++) {
        if (nonZeroCame == false && nums[i] == '0') {
            continue;
        } else {
            nonZeroCame = true;
            ans = ans + nums[i];
        }
    }
    ans = ans + nums[nums.length - 1];
    return ans;
};


console.log(largestNumber([3, 30, 34, 5, 9, 90, 901, 32, 98, 981]));
// console.log(largestNumber([432,43243]));
// 43243243
// 43243432
