/**
 * @param {string} colors
 * @return {boolean}
 */
var winnerOfGame = function (colors) {
    // let AContinuesCount = [];
    // let BContinuesCount = [];

    let currACount = 0;
    let currBCount = 0;

    let ATurnsCount = 0;
    let BTurnsCount = 0;

    for (let i = 0; i < colors.length; i++) {
        if (colors[i] == 'A') {
            currACount++;
            if (currBCount >= 3) {
                // BContinuesCount.push(currBCount);
                BTurnsCount += (currBCount - 2);
            }
            currBCount = 0;
        } else {
            currBCount++;
            if (currACount >= 3) {
                // AContinuesCount.push(currACount);
                ATurnsCount += (currACount - 2);
            }
            currACount = 0;
        }
    }


    // console.log(currACount, currBCount, " currACount, currBCount ")

    if(currBCount>=3){
        BTurnsCount += (currBCount - 2);
    }

    if(currACount>=3){
        ATurnsCount += (currACount - 2);
    }

    // console.log(AContinuesCount, BContinuesCount, " AContinuesCount, BContinuesCount ")

    // let AIndex = 0;
    // let BIndex = 0;


    // for (let i = 0; i < Math.max(AContinuesCount.length, BContinuesCount.length); i++) {
    //     if (i < AContinuesCount.length) {
    //         ATurnsCount += (AContinuesCount[i] - 2);
    //     }

    //     if (i < BContinuesCount.length) {
    //         BTurnsCount += (BContinuesCount[i] - 2);
    //     }
    // }

    // console.log(ATurnsCount, BTurnsCount, " ATurnsCount, BTurnsCount ")

    return ATurnsCount > BTurnsCount ? true : false;


};


// console.log(winnerOfGame("BAAAABBBBAAAAAB"))

console.log(winnerOfGame("AAAABBBB"))
