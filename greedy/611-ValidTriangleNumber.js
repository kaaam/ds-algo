// TODO: try more optimised solution




/**
 * @param {number[][]} people
 * @return {number[][]}
 */
var triangleNumber = function (people) {
    people.sort((a, b) => a - b);
    // console.log(JSON.stringify(people), " people ")

    let ans = 0;
    for (let i = 0; i < people.length; i++) {
        if(people[i] == 0) continue;
        for (let j = i + 1; j < people.length; j++) {
            let twoSum = people[i] + people[j];

            let left = j + 1;
            let right = people.length - 1;


            // console.log('\n\n')
            // console.log(people[i], people[j], " people[i], people[j] ")
            // console.log(i, j, " i, j ")

            // let minDiff = Number.MAX_SAFE_INTEGER;
            while (left < right) {

                // let middle = (right + left) >> 1;
                // let diff = people[middle] - twoSum;
                // // console.log('\n\n')
                // // console.log(people[middle], twoSum, " people[middle] twoSum")
                // // console.log(left, right, middle, diff, minDiff, " left, right, middle, diff, minDiff ")
                // // console.log(left, right, middle, diff, " left, right, middle, diff, ")
                // // if (Math.abs(diff) <= minDiff) {
                //     // minDiff = Math.abs(diff);
                //     if (diff <= 0) {
                //         left = middle + 1;
                //     } else if (diff > 0) {
                //         right = middle;
                //     }
                // // }

                let middle = (right + left) >> 1;
                if (people[middle] >= twoSum) {
                    right = middle;
                } else {
                    left = middle + 1;
                }
                // console.log(left, right, " left, right ")
                // console.log(middle, twoSum, " middle, twoSum ")


            }

            
            // console.log(people[left - 1], people[left - 2], people[right + 1], people[right + 2], " people[left-1], people[left-2], people[right+1], people[right+2], ")
            // console.log(people[left], people[right], " people[left], people[right] ")
            // console.log(left, right, " left right ")
            // console.log(people[right] >= twoSum ? (right - (j + 1)) : Math.abs((right-1) - (j + 1)), right, j + 1, " right-j+1, right, j+1 ")
            ans += people[right] >= twoSum ? Math.abs(right - (j + 1)) : Math.abs((right+1) - (j + 1));
            // if(people[right]<people[right] && people[right]<=twoSum){
            //     right++;
            // }


            // return;
        }
    }
    return ans;

};


// console.log(triangleNumber([7, 8, 9, 12, 13, 15, 16, 32, 4, 12, 45, 7, 3, 2, 3, 65, 22, 4,2 ,4]));
// console.log(triangleNumber([7, 8, 9, 12, 13, 13, 13, 13, 13, 13, 16, 16, 16, 16, 16, 16, 32, 12, 45,65, 22, 100, 122, 234, 455, 432, 235,435]));

// console.log(triangleNumber([2, 2, 3, 4]));
// console.log(triangleNumber([4,2,3,4]));

// console.log(triangleNumber([2,2,3,3,4,4,4]));
console.log(triangleNumber([0, 0]));

