// TODO: optimise



/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */

var swap = function(array, a, b){
	let temp = array[a];
	array[a] = array[b];
	array[b] = temp;
	return array;

}

var wiggleSort = function (nums) {
	nums.sort((a, b) => a - b);
	// 1, 2, 3, 4, 5, 6
	let middle = (nums.length-1) >> 1;
	let left = 0;

	// swap(nums, middle, 0);
	// console.log(nums)
	for(let i=middle;i<nums.length;i++){
		// console.log(i)
		swap(nums, i, left);
		// console.log(nums, i, left)
		left++;
		if(i!=middle && i!=nums.length-1){
			swap(nums, i, left);
			// console.log(nums, i, left)
			left++;
		}

	}


	return nums;

};

// console.log(wiggleSort([3, 5, 2, 1, 6, 4]));
console.log(wiggleSort([1, 2, 3]));