/**
 * @param {character[][]} board
 * @param {string} word
 * @return {boolean}
 */


// *** DFS

var recurse = function(board, word, i, j, strIndex, len){
    if(board[i][j]!= word[strIndex]) return len;
    
    let temp = board[i][j];
    board[i][j] = -1;
    
    let r = Math.max(
        i+1>=board.length?len+1:recurse(board, word, i+1, j, strIndex+1, len+1),
        i-1<=-1?len+1:recurse(board, word, i-1, j, strIndex+1, len+1),
        j+1>=board[0].length?len+1:recurse(board, word, i, j+1, strIndex+1, len+1),
        j-1<=-1?len+1:recurse(board, word, i, j-1, strIndex+1, len+1)
    )
    
    board[i][j] = temp;
    return r;

}

var exist = function(board, word) {
    
    
    // let max = 0;
    // let r;
    for(let i=0; i<board.length;i++){
        for(let j=0; j<board[i].length;j++){
            if(board[i][j] == word['0']){
                // r = recurse(board, word, i, j, 0, 0);
                // if(r>max) max=r;
                // console.log(r, " r ")
                if(recurse(board, word, i, j, 0, 0) == word.length) return true;
            }
        }
    }
    
    // return max==word.length;
    return false;
    
};