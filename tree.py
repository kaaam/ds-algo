
def printInorder(root):
  
    if root:
        # print(root.value, "!!!!!!!!") 
  
        # First recur on left child 
        printInorder(root.left) 
  
        # then print the data of node 
        print(root.value, root.value1)
  
        # now recur on right child 
        printInorder(root.right) 


def sum_of_left_nodes(node):
    # in traversal we want to go both left and right
    # print(node.value, "!!!!!! start")
    if node.left is None:
        node.value1 = node.value
    else:
        node.value1 = sum_of_left_nodes(node.left) + node.value
    if node.right is None:
        return node.value1
    else:
        sum_of_left_nodes(node.right)
    # print(node.value, "!!!!!! end", node.value1)
    return node.value1


def sum_of_left_right_children_nodes_seprately(node):
    # print(node.value, "!!!!!! start")
    node.valuel = 0
    node.valuer = 0
    if node.left is None:
        node.valuel = 0
    else:
        _return_left = sum_of_left_right_children_nodes_seprately(node.left)
        node.valuel = _return_left[0] + _return_left[1] + _return_left[2]
    if node.right is None:
        node.valuer = 0
        # return node.valuel
    else:
        _return_left = sum_of_left_right_children_nodes_seprately(node.right)
        node.valuer = _return_left[0] + _return_left[1] + _return_left[2]
    # print(node.value, node.valuel, node.valuer, "!")
    return (node.value, node.valuel, node.valuer)
    
                
class node():
    def __init__(self, value, right=None, left=None):
        self.value = value
        self.right = right
        self.left = left
    def connect(right=None, left=None):
        self.right = right
        self.left = left


               #         1
               #       /  \
               #      /    \
               #     2      3
               #    / \    / \ 
               #   4   5  6   7
               #  /     \    /
               # 8       9  10



_list = []
for _x in range(11):
    _list.append(node(_x))

# for _x in _list:
#     print(_x.value)

_list[1].left = _list[2]
_list[1].right = _list[3]

_list[2].left = _list[4]
_list[2].right = _list[5]

_list[4].left = _list[8]
# _list[1].right = _list[4]

# _list[1].left = _list[3]
_list[5].right = _list[9]

_list[3].left = _list[6]
_list[3].right = _list[7]

_list[7].left = _list[10]
# print(_list[5].value, _list[5].right.value)

# sum_of_left_nodes(_list[1])
# print(printInorder(_list[1]))


class nnode():
    def __init__(self, value, right=None, left=None):
        self.value = value
        self.right = right
        self.left = left
    def connect(right=None, left=None):
        self.right = right
        self.left = left



class ListToBst():

    # def __init__(self, node_instance):
    #     self.node_instance = node_instance


    def _insert(self, node, e):
        # here we either want to go left or right
        if e < node.value:
            if node.left is None:
                # print(e, "adding to left", node.value, '\n')
                node.left = nnode(e)
            else:
                self._insert(node.left, e)
        else:
            if node.right is None:
                # print(e, "adding to right", node.value, '\n')
                node.right = nnode(e)
            else:
                self._insert(node.right, e)
        # print(node.value, "@@@", e, '\n')
        return node

    def construct_bst(self, _list):
        root_node = nnode(_list[0])
        for element in _list[1:]:
            root_node = self._insert(root_node, element)
        # print(root_node)
        # x = input()
        # _root_node = root_node
        # print(_root_node.value, "first")
        # while(x is not 'q'):
        #     if x == 'r':
        #         if _root_node.right is not None:
        #             _root_node = _root_node.right
        #             print(_root_node.value)
        #     elif x == 'l':
        #         if _root_node.left is not None:
        #             _root_node = _root_node.left
        #             print(_root_node.value)
        #     elif x == 't':
        #             _root_node = root_node
        #             print(_root_node.value)
        #     x = input()
        return root_node


def interactive_tree_traversal(root_node):
    x = input('enter q to quit , t for top, l for left, r for right')
    _root_node = root_node
    print(_root_node.value, "first")
    while(x is not 'q'):
        if x == 'r':
            if _root_node.right is not None:
                _root_node = _root_node.right
                print(_root_node.value, 'top')
        elif x == 'l':
            if _root_node.left is not None:
                _root_node = _root_node.left
                print(_root_node.value)
        elif x == 't':
                _root_node = root_node
                print(_root_node.value)
        else:
            print('enter q to quit , t for top, l for left, r for right')
        x = input()


_ListToBst = ListToBst()
new_bst_tree = _ListToBst.construct_bst([50, 5, 1, 20, 35, 10, 100, 200, 25, 11])


def bst_to_sorted_list(node):
    _list=[]
    # print(_list, 'start', node.value)
    if node.left is None:
        # _list.append(node.value)
        pass
    else:
        # https://stackoverflow.com/questions/1132941/least-astonishment-and-the-mutable-default-argument
        # ToDo: have to pass empty array, defalut param value not working !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        _list = bst_to_sorted_list(node.left)
    _list.append(node.value)
    if node.right is None:
        pass
    else:
        # print(_list, 'before')
        # https://stackoverflow.com/questions/1132941/least-astonishment-and-the-mutable-default-argument
        # ToDo: have to pass empty array, defalut param value not working !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        _list = _list + bst_to_sorted_list(node.right)
        # print(_list, 'after')
    # print(_list, 'finally', node.value)
    return _list


# https://stackoverflow.com/questions/1132941/least-astonishment-and-the-mutable-default-argument
def testtest(y, x = []):
    if y%2 is 0:
        x = [10]
    print(y, x)
    if y is 0:
        return
    else:
        testtest(y-1)
# print(testtest(10))


def sorted_list_to_min_heap(root_nodes, sorted_list):
    if len(sorted_list) is 0:
        print("!!!!sorted_list is 0", sorted_list)
        return
    if len(root_nodes) is 0:
        root_nodes = [nnode(sorted_list[0])]
        sorted_list = sorted_list[1:]
        sorted_list_to_min_heap(root_nodes, sorted_list)
        return root_nodes[0]
    else:
        _i = 0
        _root_nodes = []
        for _node in root_nodes:
            print("_______", _node.value)
            try:
                _node.left = nnode(sorted_list[_i])
                _root_nodes.append(_node.left)
                _i = _i + 1
                print(_node.value, _node.left.value, "@@@@@sorted_list_to_min_heap left")
            except IndexError as e:
                return
            try:
                _node.right = nnode(sorted_list[_i])
                _root_nodes.append(_node.right)
                _i = _i + 1
                print(_node.value, _node.right.value, "@@@@@sorted_list_to_min_heap right")
            except IndexError as e:
                pass
            # print(_node.value, _node.left.value, _node.right.value, "@@@@@sorted_list_to_min_heap")
        sorted_list = sorted_list[_i:]
        print(sorted_list)
        print('--------startlevel\n')
        for xx in _root_nodes:
            print(xx.value)
        print('--------endlevel\n')
        return sorted_list_to_min_heap(_root_nodes, sorted_list)


def sorted_list_to_balanced_bst(sorted_list):
    if len(sorted_list) == 0:
        return None
    lower_half = int((len(sorted_list)-1)/2)
    # if node is None:
    node = nnode(sorted_list[lower_half])
    node.left = sorted_list_to_balanced_bst(sorted_list[:lower_half])
    node.right = sorted_list_to_balanced_bst(sorted_list[lower_half+1:])
    try:
        print(node.value, "left", node.left.value)
    except AttributeError:
        print(node.value, "left", None)
    try:
        print(node.value, "right", node.right.value)
    except AttributeError:
        print(node.value, "right", None)
    print('\n')
    return node
        



# sum_of_left_right_children_nodes_seprately(new_bst_tree)

# sorted_list = bst_to_sorted_list(new_bst_tree)

# print(sorted_list, "@!!!!sorted_list")
# _min_heap_root_node = sorted_list_to_min_heap([], sorted_list)
# interactive_tree_traversal(_min_heap_root_node)

# sorted_list_to_balanced_bst(sorted_list)
# sorted_list_to_balanced_bst([1, 2, 3, 4, 5, 6])



# args and kwargs
def my_sum(*args):
    print(args, type(args))
    result = 0
    # Iterating over the Python args tuple
    for x in args:
        print(x)

print(my_sum([1,2,3,3,4,4], 2, 3, 4))