
class Node:
	def __init__(self, value, next=None):
		self.value = value
		self.next = next
	def connect(self, next):
		self.next = next

def list_to_linked_list(int_list):
	prev_node = Node(int_list[0])
	first_node = prev_node
	for i in range(1, len(int_list)):
		new_node = Node(int_list[i])
		prev_node.connect(new_node)
		prev_node = new_node
	return first_node

def linked_list_to_list(first_node):
	_list = []
	current_node = first_node
	while(current_node.next):
		print(current_node.value)
		_list.append(current_node.value)
		current_node = current_node.next
	print(current_node.value)
	_list.append(current_node.value)
	return _list


first_node = list_to_linked_list([1,2,3,4,5,6,7])
# print('linked_list_to_list', linked_list_to_list(first_node))

def middle_element_of_linked_list(first_node):
	_fast_node = first_node
	_slow_node = first_node
	while _fast_node.next is not None and _fast_node.next.next is not None:
		_fast_node = _fast_node.next.next
		_slow_node = _slow_node.next
	return _slow_node.value


# print('middle_element_of_linked_list', middle_element_of_linked_list(first_node))


def reverse_link_list(first_node):
	_stack = linked_list_to_list(first_node)
	_list = []
	while(1):
		try:
			_list.append(_stack.pop())
		except IndexError:
			break
	print(_list)
	return list_to_linked_list(_list)

# print('reverse_link_list', reverse_link_list(first_node))


def rotate_link_list(first_node, new_value):
	old_first_node = first_node
	
	current_node = first_node.next
	prev_node = first_node
	while current_node.value is not new_value:
		current_node = current_node.next
		prev_node = prev_node.next

	new_first_node = current_node
	prev_node.next = None

	while current_node.next:
		current_node = current_node.next
	current_node.next = old_first_node

	return new_first_node

	# while(first_node.value)
# print('rotate_link_list', linked_list_to_list(rotate_link_list(first_node, 3)))


def reverse_link_list(first_node):
	first = first_node
	second = first_node.next
	third = first_node.next.next
	first.next = None

	while second.next:
		# print(first.value, second.value, third.value)
		second.next = first

		first = second
		second = third
		third = third.next
	second.next = first
	return second

print('reverse_link_list', linked_list_to_list(reverse_link_list(first_node)))
